## 1.2.1
▪ Fix for a bug with the advanced interval settings. (#53)



## 1.2.0
▪ Stax now tracks how much time you spend with a HIT tab open and uses the information to calculate each completed HIT's hourly wage. (#38)

▪ Added four reports (1 day, 7 days, 14 days, and 30 days) to the Activity tab. Click their titles to copy a Markdown version to the clipboard. (#37)

▪ Added an explicit dropzone to the Filter tab for exported Stax and MTS filter files. (#45)

▪ Added an explicit dropzone to the Config tab for exported theme files.

▪ Fixed the default sort in the Activity tab to show the most recently completed HITs. (#48)

▪ Added an Export to CVS button to the Activity tab.

▪ The Activity tab's exported CSV will now be sorted by date, with the most recent at the top. (#49)

▪ Renamed the Stax state switch in the Config tab to Running.

▪ Added interval fields for the three background processes to the Config tab. These are advanced settings -- the defaults are optimized for Amazon's rate limiting. Choosing poor intervals may result in pages loading slowly or not at all. Modify them at your own risk. (#44)

▪ Added a donation panel to the About tab.



## 1.1.3
▪ The Blacklist tab has been renamed to Filter.

▪ Entries in the Filter tab can now be set to Reject or Accept. Accept entries will override all Reject entries, so use them with care. (#34)

▪ Added whitelist (create an Accept filter) buttons to the Queue and HITs tab.

▪ Added experimental support for filter export/import from both Stax and MTS. You can find the related controls in the Filter tab, at the bottom inside a purple box marked as Experimental. To import, simply drag the relevant file into the filter table. Use this feature at your own risk. (#12, #17)

▪ Added a penny HIT link to the captcha popup. (#35)

▪ Fixed a display bug with HIT timers in the queue. (#36)

▪ Fixed Stax accepting some HITs with pay below the minimum if it was changed recently.

▪ Stax will go straight to the Queue tab instead of a blank screen when opened.

▪ If Stax has been updated recently, it will show the About tab instead of the Queue tab.

▪ Added an Activity tab. It does not automatically sync with Amazon; you must click the Sync button at the bottom each time you want updated information, and it will take around half a minute to collect all of the data (depending on how many HITs you've done in the last month). More features will be added to the Activity tab in future updates. (#9)

▪ Added a Minimum and Maximum pay for notification alarm sounds when a HIT has been successfully accepted. Find them under Config/Behavior. (#39)

▪ Fixed the missing checkboxes on the Queue tab.

▪ Fixed a couple of minor interface inconsistencies.



## 1.0.2
▪ Fixed the missing checkboxes on the Queue tab.

## 1.0.1
▪ Fixed Stax beeping whenever your queue was changed even if Stax wasn't running.

▪ The Config tab now has an Appearance section for creating custom themes.

▪ Themes can be exported to a Stax theme file and imported by dragging them onto the Appearance section.

▪ Minor visual changes

## 1.0.0
▪ Major architectural changes to improve efficiency, particularly when Stax is a background tab.
▪ Several minor display fixes.

## 0.0.4
▪ Stax can now play a notification sound when a new HIT is added to the queue. This is disabled by default and can be turned on with the Notification Sound and Notification Volume settings in the Config tab. (#28)

▪ An alert similar to the captcha popup will now appear in the Queue tab when Amazon has triggered a logout. (#23)

▪ Stax accepts HITs whose requesters do not have TO ratings. This is enabled by default and can be turned off with the Accept Unrated setting in the Config tab. (#30)

## 0.0.3
▪ Added an About tab that lists changes made to Stax.

▪ Stax will now detect when a captcha has occurred and embed it in the Queue tab for manual completion.

▪ Stax now auto-blacklists the IDs of HITs that have been accepted. This is enabled by default and can be turned off with the Single Accept setting in the Config tab. (#16)

▪ The blacklist by HIT ID buttons will not appear in the Queue tab if Single Accept is enabled to reduce visual clutter.

▪ The blacklist buttons in the Queue tab now blacklists by requester ID and HIT ID, instead of by requester name and HIT title. (#13)

▪ You can blacklist HITs and requesters by their IDs from the Hits tab now. (#15)

▪ You can edit entries in the Blacklist tab now. (#18)

▪ Possible fix for intermittent blacklist failure bug. (#19)

▪ You can manually add a new entry to the blacklist now. (#25)

▪ Added a new type of blacklist entry: Text. Text entries will filter out HITs if the HIT title or description contains (or equals) the entry's keyword.

▪ Fixed Stax not detecting input in the Hits tab. (#20)

▪ Hovering over a HIT title in the Queue and Hits tabs will show its description. (#21)

▪ Better tables in the Queue, Hits, and Blacklist tabs.

▪ Tweaked the default config values to provide a better first-time experience.

▪ More tooltips on miscellaneous buttons.

▪ Many minor visual fixes.

▪ Want a feature not on this list, no matter how small? Send me a DM on Reddit: /u/StructByLightning. Or open a GitLab issue at https://gitlab.com/StructByLightning/stax/issues.


## 0.0.2
▪ Removed Tesseract-based captcha completion


## 0.0.1
▪ First release
