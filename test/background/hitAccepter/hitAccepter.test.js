/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import HitAccepter from "../../../src/js/background/hitAccepter/hitAccepter";
import * as Types from "../../../src/js/constants/types";
import * as Errors from "../../../src/js/constants/errors";
import * as Requests from "../../../src/js/constants/requests";

function makeHit() {
  return {
    requester_id: "rid",
    hit_set_id: "hsid",
    description: "hello description",
    title: "what a fun hit",
    amount_in_dollars: 5.17,
    timestamp: 100,
  };
}

function makeState() {
  return {
    hits: [makeHit()],
    requesters: [
      {
        requester_id: "rid",
      },
    ],
    config: {
      to1MinPay: 1.25,
      to1MinFair: 1.25,
      to1MinFast: 1.25,
      to1MinComm: 1.25,
    },
  };
}

function setupHitAccepter(whitelisted, expired, belowPay, blacklisted, requesterNew, requesterGood) {
  const backup = [
    HitAccepter.prototype.hitIsExpired,
    HitAccepter.prototype.hitIsBelowPay,
    HitAccepter.prototype.hitIsOnFilter,
    HitAccepter.prototype.getHitRequester,
    HitAccepter.prototype.requesterMeetsRequirements,
  ];

  if (whitelisted) {
    HitAccepter.prototype.hitIsOnFilter = jest.fn(() => { return Types.FILTER_ACCEPT; });
  } else if (blacklisted) {
    HitAccepter.prototype.hitIsOnFilter = jest.fn(() => { return Types.FILTER_REJECT; });
  } else {
    HitAccepter.prototype.hitIsOnFilter = jest.fn(() => { return Types.FILTER_NONE; });
  }

  HitAccepter.prototype.hitIsExpired = jest.fn(() => { return expired; });
  HitAccepter.prototype.hitIsBelowPay = jest.fn(() => { return belowPay; });

  if (requesterNew) {
    HitAccepter.prototype.getHitRequester = jest.fn(() => { return -1; });
  } else {
    HitAccepter.prototype.getHitRequester = jest.fn(() => { return 0; });
  }

  HitAccepter.prototype.requesterMeetsRequirements = jest.fn(() => { return requesterGood; });

  return backup;
}


function restoreHitAccepter(backup) {
  HitAccepter.prototype.hitIsExpired = backup[0];
  HitAccepter.prototype.hitIsBelowPay = backup[1];
  HitAccepter.prototype.hitIsOnFilter = backup[2];
  HitAccepter.prototype.getHitRequester = backup[3];
  HitAccepter.prototype.requesterMeetsRequirements = backup[4];
}

describe("class HitAccepter", () => {
  describe("HitAccepter.hitIsOnFilter", () => {
    describe("Filtering by requester id", () => {
      it("should return Types.FILTER_NOTHING if the hit requester id does not match any entry's requester id", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "gfds",
            type: Types.FILTER_TYPE_REQUESTER_ID,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_NOTHING);
      });

      it("should return Types.FILTER_REJECT if the hit requester id matches an entry's requester id with action:Types.FILTER_REJECT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "rid",
            type: Types.FILTER_TYPE_REQUESTER_ID,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_REJECT);
      });

      it("should return Types.FILTER_ACCEPT if the hit requester id matches an entry's requester id with action:Types.FILTER_ACCEPT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "rid",
            type: Types.FILTER_TYPE_REQUESTER_ID,
            action: Types.FILTER_ACCEPT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });

      it("should return Types.FILTER_ACCEPT if there are both ACCEPT and REJECT matches", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [
            {
              id: "id1",
              keyword: "rid",
              type: Types.FILTER_TYPE_REQUESTER_ID,
              action: Types.FILTER_ACCEPT,
            },
            {
              id: "id1",
              keyword: "rid",
              type: Types.FILTER_TYPE_REQUESTER_ID,
              action: Types.FILTER_REJECT,
            },
          ],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });
    });

    describe("Filtering by hit id", () => {
      it("should return Types.FILTER_NOTHING if the hit id does not match any entry's hit id", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "gfds",
            type: Types.FILTER_TYPE_HIT_ID,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_NOTHING);
      });

      it("should return Types.FILTER_REJECT if the hit id matches an entry's hit id with action:Types.FILTER_REJECT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "hsid",
            type: Types.FILTER_TYPE_HIT_ID,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_REJECT);
      });

      it("should return Types.FILTER_ACCEPT if the hit id matches an entry's hit id with action:Types.FILTER_ACCEPT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "hsid",
            type: Types.FILTER_TYPE_HIT_ID,
            action: Types.FILTER_ACCEPT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });

      it("should return Types.FILTER_ACCEPT if there are both ACCEPT and REJECT matches", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [
            {
              id: "id1",
              keyword: "hsid",
              type: Types.FILTER_TYPE_HIT_ID,
              action: Types.FILTER_ACCEPT,
            },
            {
              id: "id1",
              keyword: "hsid",
              type: Types.FILTER_TYPE_HIT_ID,
              action: Types.FILTER_REJECT,
            },
          ],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });
    });

    describe("Filtering by title text match", () => {
      it("should return Types.FILTER_NOTHING if the title does not match any entry", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "gfds",
            type: Types.FILTER_TYPE_HIT_ID,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_NOTHING);
      });

      it("should return Types.FILTER_REJECT if the title matches an entry with action:Types.FILTER_REJECT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "fun",
            type: Types.FILTER_TYPE_DESCRIPTION,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_REJECT);
      });

      it("should return Types.FILTER_ACCEPT if the title matches an entry with action:Types.FILTER_ACCEPT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "fun",
            type: Types.FILTER_TYPE_DESCRIPTION,
            action: Types.FILTER_ACCEPT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });

      it("should return Types.FILTER_ACCEPT if there are both ACCEPT and REJECT matches", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [
            {
              id: "id1",
              keyword: "fun",
              type: Types.FILTER_TYPE_DESCRIPTION,
              action: Types.FILTER_ACCEPT,
            },
            {
              id: "id1",
              keyword: "fun",
              type: Types.FILTER_TYPE_DESCRIPTION,
              action: Types.FILTER_REJECT,
            },
          ],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });
    });

    describe("Filtering by description text match", () => {
      it("should return Types.FILTER_NOTHING if the description does not match any entry", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "gfds",
            type: Types.FILTER_TYPE_HIT_ID,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_NOTHING);
      });

      it("should return Types.FILTER_REJECT if the description matches an entry with action:Types.FILTER_REJECT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "hello",
            type: Types.FILTER_TYPE_DESCRIPTION,
            action: Types.FILTER_REJECT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_REJECT);
      });

      it("should return Types.FILTER_ACCEPT if the description matches an entry with action:Types.FILTER_ACCEPT", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [{
            id: "id1",
            keyword: "hello",
            type: Types.FILTER_TYPE_DESCRIPTION,
            action: Types.FILTER_ACCEPT,
          }],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });

      it("should return Types.FILTER_ACCEPT if there are both ACCEPT and REJECT matches", () => {
        const hitAccepter = new HitAccepter();
        hitAccepter.sendMessage = jest.fn(() => {});
        hitAccepter.state = {
          filter: [
            {
              id: "id1",
              keyword: "hello",
              type: Types.FILTER_TYPE_DESCRIPTION,
              action: Types.FILTER_ACCEPT,
            },
            {
              id: "id1",
              keyword: "hello",
              type: Types.FILTER_TYPE_DESCRIPTION,
              action: Types.FILTER_REJECT,
            },
          ],
        };

        expect(hitAccepter.hitIsOnFilter(makeHit())).toEqual(Types.FILTER_ACCEPT);
      });
    });
  });

  describe("HitAccepter.hitIsBelowPay", () => {
    it("should return true if state.config.minReward >= hit.amount_in_dollars", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = {
        config: [{
          minReward: 5.17,
        }],
      };

      expect(hitAccepter.hitIsBelowPay(makeHit())).toEqual(false);
    });

    it("should return false if state.config.minReward < hit.amount_in_dollars", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = {
        config: [{
          minReward: 3.25,
        }],
      };

      expect(hitAccepter.hitIsBelowPay(makeHit())).toEqual(false);
    });
  });

  describe("HitAccepter.hitIsExpired", () => {
    it("should return true if current time - state.config.lifespan >= hit.timestamp", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = {
        config: [{
          lifespan: 5000,
        }],
      };

      const hit = makeHit();
      hit.timestamp = new Date().getTime() - 10000;

      expect(hitAccepter.hitIsBelowPay(hit)).toEqual(false);
    });

    it("should return true if current time - state.config.lifespan < hit.timestamp", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = {
        config: [{
          lifespan: 5000,
        }],
      };

      const hit = makeHit();
      hit.timestamp = new Date().getTime() - 1000;

      expect(hitAccepter.hitIsBelowPay(hit)).toEqual(false);
    });
  });

  describe("HitAccepter.getHitRequester", () => {
    it("should return -1 if no requester is found", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = {
        requesters: [{}],
      };

      const hit = makeHit();
      hit.timestamp = new Date().getTime() - 10000;

      expect(hitAccepter.getHitRequester(hit)).toEqual(-1);
    });

    it("should return the index of the found requester", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = {
        requesters: [{
          id: "rid",
        }],
      };

      const hit = makeHit();
      hit.timestamp = new Date().getTime() - 10000;

      expect(hitAccepter.getHitRequester(hit)).toEqual(0);
    });
  });

  describe("HitAccepter.validateHits", () => {
    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=true requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, true, true, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=true requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, true, true, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, true, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, true, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, true, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, true, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, true, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=true blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, true, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=true requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, false, true, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=true requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, false, true, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, false, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, false, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, false, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, false, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, true, false, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=true belowPay=false blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, true, false, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=true requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, true, true, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=true requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, true, true, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, true, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, true, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, true, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, true, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, true, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=true blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, true, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=true requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, false, true, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=true requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, false, true, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, false, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, false, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, false, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, false, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(true, false, false, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=true expired=false belowPay=false blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(true, false, false, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(false, true, false, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(false, true, false, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, true, false, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, true, false, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=true requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, true, true, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=true requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, true, true, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, true, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, true, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, true, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, true, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, true, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=true blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, true, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=true requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, false, true, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=true requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, false, true, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, false, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, false, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, false, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [makeHit().requester_id],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, false, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [makeHit().requester_id],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, false, false, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false expired=false belowPay=false blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, false, false, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=true blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, true, true, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=true blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, true, true, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=true blacklisted=false requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(false, true, true, false, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=true blacklisted=false requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(false, true, true, false, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=true blacklisted=false requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, true, true, false, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=true blacklisted=false requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, true, true, false, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=true requesterNew=true requesterGood=true", () => {
      const backup = setupHitAccepter(false, true, false, true, true, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=true requesterNew=true requesterGood=false", () => {
      const backup = setupHitAccepter(false, true, false, true, true, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=true requesterNew=false requesterGood=true", () => {
      const backup = setupHitAccepter(false, true, false, true, false, true);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });

    it("should validate a hit if whitelisted=false  expired=true belowPay=false blacklisted=true requesterNew=false requesterGood=false", () => {
      const backup = setupHitAccepter(false, true, false, true, false, false);
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      expect(hitAccepter.validateHits()).toEqual({
        hitsToBeRemoved: [makeHit().hit_set_id],
        tempRequesters: [],
      });

      restoreHitAccepter(backup);
    });
  });

  describe("HitAccepter.syncCaptcha", () => {
    it("should do nothing if there is an error", () => {
      const hitAccepter = new HitAccepter();
      const newSendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = newSendMessage;

      hitAccepter.syncCaptcha(Errors.CAPTCHA_FOUND, null);

      expect(newSendMessage.mock.calls[0]).toEqual(undefined);
    });
    it("should send a message if there is no error", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      const newSendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = newSendMessage;

      hitAccepter.syncCaptcha(Errors.NO_ERROR, "fake.url.com");

      expect(newSendMessage.mock.calls[0]).toEqual([Requests.MTC_SYNC_CAPTCHA, "fake.url.com"]);
    });
  });

  describe("HitAccepter.handleHitAcceptance", () => {
    it("should do nothing if the error is Errors.NO_ERROR", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      const newSendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = newSendMessage;

      hitAccepter.handleHitAcceptance(Errors.NO_ERROR);

      expect(newSendMessage.mock.calls[0]).toEqual(undefined);
    });
    it("should do nothing if the error is Errors.RATE_LIMIT", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      const newSendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = newSendMessage;

      hitAccepter.handleHitAcceptance(Errors.RATE_LIMIT);

      expect(newSendMessage.mock.calls[0]).toEqual(undefined);
    });
    it("should send a message if the error is Errors.CAPTCHA_FOUND", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      const newSendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = newSendMessage;

      hitAccepter.handleHitAcceptance(Errors.CAPTCHA_FOUND);

      expect(newSendMessage.mock.calls[0]).toEqual([Requests.MTC_GET_CAPTCHA_URL, null]);
    });
    it("should do nothing if the error is Errors.HIT_UNAVAILABLE", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      const newSendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = newSendMessage;

      hitAccepter.handleHitAcceptance(Errors.HIT_UNAVAILABLE);

      expect(newSendMessage.mock.calls[0]).toEqual(undefined);
    });
    it("should do nothing if the error is Errors.QUEUE_FULL", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      const newSendMessage = jest.fn(() => {});
      hitAccepter.sendMessage = newSendMessage;

      hitAccepter.handleHitAcceptance(Errors.QUEUE_FULL);

      expect(newSendMessage.mock.calls[0]).toEqual(undefined);
    });
  });

  describe("HitAccepter.requesterMeetsRequirements", () => {
    it("should return false if the requester's pay is too low", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      const requester = {
        to1: {
          pay: 0,
          fair: 5,
          fast: 5,
          comm: 5,
        },
      };

      const result = hitAccepter.requesterMeetsRequirements(requester);
      expect(result).toEqual(false);
    });
    it("should return false if the requester's fair is too low", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      const requester = {
        to1: {
          pay: 5,
          fair: 0,
          fast: 5,
          comm: 5,
        },
      };

      const result = hitAccepter.requesterMeetsRequirements(requester);
      expect(result).toEqual(false);
    });
    it("should return false if the requester's fast is too low", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      const requester = {
        to1: {
          pay: 5,
          fair: 5,
          fast: 0,
          comm: 5,
        },
      };

      const result = hitAccepter.requesterMeetsRequirements(requester);
      expect(result).toEqual(false);
    });
    it("should return false if the requester's comm is too low", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      const requester = {
        to1: {
          pay: 5,
          fair: 5,
          fast: 5,
          comm: 0,
        },
      };

      const result = hitAccepter.requesterMeetsRequirements(requester);
      expect(result).toEqual(false);
    });
    it("should return true if the requester's pay, fair, fast, and comm are high", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      const requester = {
        to1: {
          pay: 5,
          fair: 5,
          fast: 5,
          comm: 5,
        },
      };

      const result = hitAccepter.requesterMeetsRequirements(requester);
      expect(result).toEqual(true);
    });
  });

  describe("HitAccepter.updateRequesters", () => {
    it("should print out an error if there was one", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      const oldConsoleError = console.error;
      console.error = jest.fn();
      hitAccepter.updateRequesters(Errors.ALREADY_SOLVING_CAPTCHA, null);
      expect(console.error.mock.calls[0]).toEqual([Errors.ALREADY_SOLVING_CAPTCHA]);
      console.error = oldConsoleError;
    });
    it("should add to state.requesters and send a message if there was no error", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.state = makeState();
      hitAccepter.sendMessage = jest.fn(() => {});
      const oldConsoleError = console.error;
      console.error = jest.fn();

      hitAccepter.updateRequesters(Errors.NO_ERROR, ["some garbage data"]);

      expect(console.error.mock.calls[0]).toEqual(undefined);
      expect(hitAccepter.state.requesters).toEqual([
        {
          requester_id: "rid",
        },
        "some garbage data",
      ]);
      expect(hitAccepter.sendMessage.mock.calls[0]).toEqual([Requests.MTC_ADD_REQUESTERS, ["some garbage data"]]);

      console.error = oldConsoleError;
    });
  });
  describe("HitAccepter.updateState", () => {
    it("should set the state even if stax is inactive", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.getNewTo1 = jest.fn(() => {});
      hitAccepter.state = makeState();
      hitAccepter.state.config = { active: false };
      hitAccepter.state.status = { alive: true };

      const state = {
        config: { active: false },
        status: { alive: true },
        test: "junk",
      };

      hitAccepter.updateState(state);
      expect(hitAccepter.state).toEqual(state);
      // expect(hitAccepter.getNewTo1).toHaveBeenCalled();
    });
    it("should set the state even if stax is not alive", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.getNewTo1 = jest.fn(() => {});
      hitAccepter.state = makeState();
      hitAccepter.state.config = { active: true };
      hitAccepter.state.status = { alive: false };

      const state = {
        config: { active: true },
        status: { alive: false },
        test: "junk",
      };

      hitAccepter.updateState(state);
      expect(hitAccepter.state).toEqual(state);
      // expect(hitAccepter.getNewTo1).toHaveBeenCalled();
    });
    it("should call getTo1 if stax is both alive and active", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.getNewTo1 = jest.fn(() => {});
      hitAccepter.state = makeState();
      hitAccepter.state.config = { active: true };
      hitAccepter.state.status = { alive: true };

      const state = {
        config: { active: true },
        status: { alive: true },
        test: "junk",
      };

      hitAccepter.updateState(state);
      expect(hitAccepter.getNewTo1).toHaveBeenCalled();
    });
    it("should not call getTo1 if stax is not alive, and it should set working to false ", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.getNewTo1 = jest.fn(() => {});
      hitAccepter.state = makeState();
      hitAccepter.state.config = { active: true };
      hitAccepter.state.status = { alive: false };

      const state = {
        config: { active: true },
        status: { alive: false },
        test: "junk",
      };

      hitAccepter.updateState(state);
      expect(hitAccepter.getNewTo1).not.toHaveBeenCalled();
      expect(hitAccepter.working).toEqual(false);
    });
    it("should not call getTo1 if stax is not active, and it should set working to false ", () => {
      const hitAccepter = new HitAccepter();
      hitAccepter.sendMessage = jest.fn(() => {});
      hitAccepter.getNewTo1 = jest.fn(() => {});
      hitAccepter.state = makeState();
      hitAccepter.state.config = { active: false };
      hitAccepter.state.status = { alive: true };

      const state = {
        config: { active: false },
        status: { alive: true },
        test: "junk",
      };

      hitAccepter.updateState(state);
      expect(hitAccepter.getNewTo1).not.toHaveBeenCalled();
      expect(hitAccepter.working).toEqual(false);
    });
  });
});
