/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import HitScraper from "../../../src/js/background/hitScraper/hitScraper";
import * as Errors from "../../../src/js/constants/errors";
import * as Requests from "../../../src/js/constants/requests";

const makeRawHit = () => {
  return {
    hit_set_id: "hsi1",
    requester_id: "ri1",
    requester_name: "rn1",
    title: "t1",
    description: "d1",
    assignment_duration_in_seconds: "adin1",
    creation_time: "ct1",
    assignable_hits_count: "ahc1",
    latest_expiration_time: "let1",
    last_updated_time: "lut1",
    monetary_reward: {
      amount_in_dollars: "aid1",
    },
    accept_project_task_url: "aptu1",
    requester_url: "ru1",
    project_tasks_url: "ptu1",
  };
};


describe("class HitScraper", () => {
  describe("HitScraper.execute", () => {
    it("should call sendMessage", () => {
      const hitScraper = new HitScraper();
      hitScraper.sendMessage = jest.fn(() => {});

      hitScraper.execute();

      expect(hitScraper.sendMessage.mock.calls[0]).toEqual([Requests.MTC_GET_HITS, Errors.NO_ERROR, null]);
    });
  });
  describe("HitScraper.updateInterval", () => {
    it("should set this.interval to config.hitScraperInterval", () => {
      const hitScraper = new HitScraper();
      hitScraper.state = {
        config: {
          hitScraperInterval: "test",
        },
      };

      hitScraper.updateInterval();

      expect(hitScraper.interval).toEqual("test");
    });
  });
  describe("HitScraper.handleMessage", () => {
    it("should call setState if type === Requests.MTC_GET_STORE_RESPONSE", () => {
      const hitScraper = new HitScraper();
      hitScraper.setState = jest.fn(() => {});
      hitScraper.processHits = jest.fn(() => {});

      hitScraper.handleMessage(Requests.MTC_GET_STORE_RESPONSE, null, null);

      expect(hitScraper.setState).toHaveBeenCalled();
    });
    it("should call processHits if type === Requests.MTC_GET_HITS_RESPONSE", () => {
      const hitScraper = new HitScraper();
      hitScraper.setState = jest.fn(() => {});
      hitScraper.processHits = jest.fn(() => {});

      hitScraper.handleMessage(Requests.MTC_GET_HITS_RESPONSE, null, null);

      expect(hitScraper.processHits).toHaveBeenCalled();
    });
    it("should call do nothing if type isn't a valid request", () => {
      const hitScraper = new HitScraper();
      hitScraper.setState = jest.fn(() => {});
      hitScraper.processHits = jest.fn(() => {});

      hitScraper.handleMessage("adsfkjhbafdskjdsafkjs", null, null);

      expect(hitScraper.processHits).not.toHaveBeenCalled();
      expect(hitScraper.setState).not.toHaveBeenCalled();
    });
  });
  describe("HitScraper.convertRawHit", () => {
    it("should copy all of the fields except monetary_reward", () => {
      const hitScraper = new HitScraper();
      const DATE_TO_USE = new Date("2016");
      const _Date = Date;
      global.Date = jest.fn(() => { return DATE_TO_USE; });
      global.Date.UTC = _Date.UTC;
      global.Date.parse = _Date.parse;
      global.Date.now = _Date.now;

      expect(hitScraper.convertRawHit(makeRawHit())).toMatchObject({
        hit_set_id: "hsi1",
        requester_id: "ri1",
        requester_name: "rn1",
        title: "t1",
        description: "d1",
        assignment_duration_in_seconds: "adin1",
        creation_time: "ct1",
        assignable_hits_count: "ahc1",
        latest_expiration_time: "let1",
        last_updated_time: "lut1",
        accept_project_task_url: "aptu1",
        requester_url: "ru1",
        project_tasks_url: "ptu1",
      });
    });
    it("should rename monetary_reward to pay and flatten it", () => {
      const hitScraper = new HitScraper();
      const DATE_TO_USE = new Date("2016");
      const _Date = Date;
      global.Date = jest.fn(() => { return DATE_TO_USE; });
      global.Date.UTC = _Date.UTC;
      global.Date.parse = _Date.parse;
      global.Date.now = _Date.now;

      expect(hitScraper.convertRawHit(makeRawHit())).toMatchObject({
        pay: "aid1",
      });
    });
    it("should add some extra fields", () => {
      const hitScraper = new HitScraper();
      const DATE_TO_USE = new Date("2016");
      const _Date = Date;
      global.Date = jest.fn(() => { return DATE_TO_USE; });
      global.Date.UTC = _Date.UTC;
      global.Date.parse = _Date.parse;
      global.Date.now = _Date.now;

      expect(hitScraper.convertRawHit(makeRawHit())).toMatchObject({
        timestamp: DATE_TO_USE.getTime(),
        lastChecked: 0,
        validated: false,
      });
    });
  });
  describe("HitScraper.processHits", () => {
    it("should do nothing if there is an error", () => {
      const hitScraper = new HitScraper();
      hitScraper.convertRawHit = jest.fn(() => { return "nice hit"; });
      hitScraper.sendMessage = jest.fn(() => {});

      hitScraper.processHits(Errors.CAPTCHA_FOUND, [makeRawHit()]);

      expect(hitScraper.sendMessage).not.toHaveBeenCalled();
      expect(hitScraper.convertRawHit).not.toHaveBeenCalled();
    });
    it("should do nothing for hits already in the state", () => {
      const hitScraper = new HitScraper();
      hitScraper.convertRawHit = jest.fn(() => { return "nice hit"; });
      hitScraper.sendMessage = jest.fn(() => {});
      hitScraper.state = {
        hits: [makeRawHit()],
      };

      hitScraper.processHits(Errors.NO_ERROR, [makeRawHit()]);

      expect(hitScraper.sendMessage.mock.calls[0]).toEqual([Requests.MTC_ADD_HITS, Errors.NO_ERROR, []]);
      expect(hitScraper.convertRawHit).not.toHaveBeenCalled();
    });
    it("should convert new hits and send them in a message", () => {
      const hitScraper = new HitScraper();
      hitScraper.convertRawHit = jest.fn(() => { return "nice hit"; });
      hitScraper.sendMessage = jest.fn(() => {});
      const otherHit = makeRawHit();
      otherHit.hit_set_id = "hsi2";
      hitScraper.state = {
        hits: [otherHit],
      };

      hitScraper.processHits(Errors.NO_ERROR, [makeRawHit()]);

      expect(hitScraper.sendMessage.mock.calls[0]).toEqual([Requests.MTC_ADD_HITS, Errors.NO_ERROR, ["nice hit"]]);
      expect(hitScraper.convertRawHit.mock.calls[0]).toEqual([makeRawHit()]);
    });
  });
});
