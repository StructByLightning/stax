/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QueueScraper from "../../../src/js/background/queueScraper/queueScraper";
import * as Errors from "../../../src/js/constants/errors";
import * as Requests from "../../../src/js/constants/requests";

const makeRawQueueEntry = () => {
  return {
    accepted_at: "aa",
    task_id: "ti",
    assignment_id: "ai",
    deadline: "d",
    time_to_deadline_in_seconds: "ttdis",
    expired_task_action_url: "etau",
    task_url: "tu",
    deleteThis: "dt",
    project: {
      hit_set_id: "hsi",
      requester_id: "ri",
      requester_name: "rn",
      title: "t",
      description: "d",
      assignment_duration_in_seconds: "adis",
      creation_time: "ct",
      assignable_hits_count: "ahc",
      latest_expiration_time: "let",
      last_updated_time: "lut",
      requester_url: "ru",
      monetary_reward: {
        amount_in_dollars: "aid",
      },
    },
  };
};

describe("class QueueScraper", () => {
  describe("QueueScraper.execute", () => {
    it("should call sendMessage", () => {
      const queueScraper = new QueueScraper();
      queueScraper.sendMessage = jest.fn(() => {});

      queueScraper.execute();

      expect(queueScraper.sendMessage.mock.calls[0]).toEqual([Requests.MTC_GET_QUEUE, Errors.NO_ERROR, null]);
    });
  });
  describe("QueueScraper.updateInterval", () => {
    it("should set this.interval to config.queueScraperInterval", () => {
      const queueScraper = new QueueScraper();
      queueScraper.state = {
        config: {
          queueScraperInterval: "test",
        },
      };

      queueScraper.updateInterval();

      expect(queueScraper.interval).toEqual("test");
    });
  });
  describe("QueueScraper.handleMessage", () => {
    it("should call setState if type === Requests.MTC_GET_STORE_RESPONSE", () => {
      const queueScraper = new QueueScraper();
      queueScraper.setState = jest.fn(() => {});
      queueScraper.processQueue = jest.fn(() => {});

      queueScraper.handleMessage(Requests.MTC_GET_STORE_RESPONSE, null, null);

      expect(queueScraper.setState).toHaveBeenCalled();
    });
    it("should call processQueue if type === Requests.MTC_GET_QUEUE_RESPONSE", () => {
      const queueScraper = new QueueScraper();
      queueScraper.setState = jest.fn(() => {});
      queueScraper.processQueue = jest.fn(() => {});

      queueScraper.handleMessage(Requests.MTC_GET_QUEUE_RESPONSE, null, null);

      expect(queueScraper.processQueue).toHaveBeenCalled();
    });
    it("should call do nothing if type isn't a valid request", () => {
      const queueScraper = new QueueScraper();
      queueScraper.setState = jest.fn(() => {});
      queueScraper.processQueue = jest.fn(() => {});

      queueScraper.handleMessage("adsfkjhbafdskjdsafkjs", null, null);

      expect(queueScraper.processQueue).not.toHaveBeenCalled();
      expect(queueScraper.setState).not.toHaveBeenCalled();
    });
  });
  describe("QueueScraper.convertRawQueueEntry", () => {
    it("should copy some fields", () => {
      const queueScraper = new QueueScraper();
      queueScraper.setState = jest.fn(() => {});
      queueScraper.processQueue = jest.fn(() => {});

      expect(queueScraper.convertRawQueueEntry(makeRawQueueEntry())).toMatchObject({
        accepted_at: "aa",
        task_id: "ti",
        assignment_id: "ai",
        deadline: "d",
        time_to_deadline_in_seconds: "ttdis",
        expired_task_action_url: "etau",
        task_url: "tu",
        deleteThis: "dt",
      });
    });
    it("should flatten and copy some fields", () => {
      const queueScraper = new QueueScraper();
      queueScraper.setState = jest.fn(() => {});
      queueScraper.processQueue = jest.fn(() => {});

      expect(queueScraper.convertRawQueueEntry(makeRawQueueEntry())).toMatchObject({
        hit_set_id: "hsi",
        requester_id: "ri",
        requester_name: "rn",
        title: "t",
        description: "d",
        assignment_duration_in_seconds: "adis",
        creation_time: "ct",
        assignable_hits_count: "ahc",
        latest_expiration_time: "let",
        last_updated_time: "lut",
        requester_url: "ru",
      });
    });
    it("should flatten and rename the monetary_reward field", () => {
      const queueScraper = new QueueScraper();
      queueScraper.setState = jest.fn(() => {});
      queueScraper.processQueue = jest.fn(() => {});

      expect(queueScraper.convertRawQueueEntry(makeRawQueueEntry())).toMatchObject({
        pay: "aid",
      });
    });
  });
  describe("QueueScraper.processQueue", () => {
    it("should do nothing if there is an error", () => {
      const queueScraper = new QueueScraper();
      queueScraper.sendMessage = jest.fn(() => {});
      queueScraper.convertRawQueueEntry = jest.fn(() => { return "nice queue"; });

      queueScraper.processQueue(Errors.RATE_LIMIT, null);

      expect(queueScraper.sendMessage).not.toHaveBeenCalled();
      expect(queueScraper.convertRawQueueEntry).not.toHaveBeenCalled();
    });
    it("should convert everything in data and send it to the parent process", () => {
      const queueScraper = new QueueScraper();
      queueScraper.sendMessage = jest.fn(() => {});
      queueScraper.convertRawQueueEntry = jest.fn(() => { return "nice queue"; });

      queueScraper.processQueue(Errors.NO_ERROR, [makeRawQueueEntry()]);

      expect(queueScraper.sendMessage.mock.calls[0]).toEqual([Requests.MTC_CLEAR_AND_ADD_QUEUE, Errors.NO_ERROR, ["nice queue"]]);
      expect(queueScraper.convertRawQueueEntry.mock.calls[0]).toEqual([makeRawQueueEntry()]);
    });
  });
});
