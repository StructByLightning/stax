/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import config from "../../../src/js/store/reducers/config";
import * as ConfigActions from "../../../src/js/store/actions/configActions";

const makeState = () => {
  return {
    minReward: 5,
    hitSort: "asdgfasdf",
    pageSize: 64,
    qualified: false,
    masters: true,
    to1MinComm: 80,
    to1MinPay: 39,
    to1MinFair: 39,
    to1MinFast: 42,
    lifespan: 90,
    active: true,
    singleAccept: false,
    captchaUrl: "example.com",
    notificationAudioFilepath: "asdfadsf",
    notificationAudioVolume: 254,
    acceptUnratedRequesters: false,
    lastUpdate: "1.0.2",
    notificationMaxPay: 99,
    notificationMinPay: 0,
  };
};

describe("Config reducer", () => {
  describe("ConfigActions.SET", () => {
    it("should set the keys in the dict ", () => {
      const targets = {
        minReward: 25,
        hitSort: "vcadvc",
        pageSize: 640,
        qualified: true,
        masters: false,
        to1MinComm: 820,
        to1MinPay: 339,
        to1MinFair: 349,
        to1MinFast: 452,
        lifespan: 950,
        active: false,
        singleAccept: true,
        captchaUrl: "examssple.com",
        notificationAudioFilepath: "vdcs",
        notificationAudioVolume: 9832,
        acceptUnratedRequesters: true,
        lastUpdate: "1.0.2",
        notificationMaxPay: 99,
        notificationMinPay: 0,
      };

      expect(config(makeState(), ConfigActions.set(targets))).toEqual({
        minReward: 25,
        hitSort: "vcadvc",
        pageSize: 640,
        qualified: true,
        masters: false,
        to1MinComm: 820,
        to1MinPay: 339,
        to1MinFair: 349,
        to1MinFast: 452,
        lifespan: 950,
        active: false,
        singleAccept: true,
        captchaUrl: "examssple.com",
        notificationAudioFilepath: "vdcs",
        notificationAudioVolume: 9832,
        acceptUnratedRequesters: true,
        lastUpdate: "1.0.2",
        notificationMaxPay: 99,
        notificationMinPay: 0,
      });
    });

    it("should not set keys if they do not already exist", () => {
      const targets = {
        asdfjkhadsfhk: "asdfasdf",
      };

      expect(config(makeState(), ConfigActions.set(targets))).toEqual({
        minReward: 5,
        hitSort: "asdgfasdf",
        pageSize: 64,
        qualified: false,
        masters: true,
        to1MinComm: 80,
        to1MinPay: 39,
        to1MinFair: 39,
        to1MinFast: 42,
        lifespan: 90,
        active: true,
        singleAccept: false,
        captchaUrl: "example.com",
        notificationAudioFilepath: "asdfadsf",
        notificationAudioVolume: 254,
        acceptUnratedRequesters: false,
        lastUpdate: "1.0.2",
        notificationMaxPay: 99,
        notificationMinPay: 0,
      });
    });
  });

  describe("ConfigActions.TOGGLE", () => {
    it("should toggle the keys in the array", () => {
      const targets = [
        "qualified",
        "masters",
        "active",
        "singleAccept",
        "acceptUnratedRequesters",
      ];

      expect(config(makeState(), ConfigActions.toggle(targets))).toEqual({
        minReward: 5,
        hitSort: "asdgfasdf",
        pageSize: 64,
        qualified: true,
        masters: false,
        to1MinComm: 80,
        to1MinPay: 39,
        to1MinFair: 39,
        to1MinFast: 42,
        lifespan: 90,
        active: false,
        singleAccept: true,
        captchaUrl: "example.com",
        notificationAudioFilepath: "asdfadsf",
        notificationAudioVolume: 254,
        acceptUnratedRequesters: true,
        lastUpdate: "1.0.2",
        notificationMaxPay: 99,
        notificationMinPay: 0,
      });
    });
    it("should do nothing for keys that are not booleans", () => {
      const targets = [
        "adsfasdf",
        "minReward",
      ];

      expect(config(makeState(), ConfigActions.toggle(targets))).toEqual({
        minReward: 5,
        hitSort: "asdgfasdf",
        pageSize: 64,
        qualified: false,
        masters: true,
        to1MinComm: 80,
        to1MinPay: 39,
        to1MinFair: 39,
        to1MinFast: 42,
        lifespan: 90,
        active: true,
        singleAccept: false,
        captchaUrl: "example.com",
        notificationAudioFilepath: "asdfadsf",
        notificationAudioVolume: 254,
        acceptUnratedRequesters: false,
        lastUpdate: "1.0.2",
        notificationMaxPay: 99,
        notificationMinPay: 0,
      });
    });
  });

  describe("ConfigActions.CLEAR", () => {
    it("should return the initial state", () => {
      expect(config(makeState(), ConfigActions.clear())).toEqual({
        minReward: 0.5,
        hitSort: "updated_desc",
        pageSize: 100,
        qualified: true,
        masters: false,
        to1MinComm: 0,
        to1MinPay: 3,
        to1MinFair: 3,
        to1MinFast: 0,
        lifespan: 1800000,
        active: false,
        singleAccept: true,
        captchaUrl: "https://worker.mturk.com/projects/3NIW7VZHBKXLVNM0IL5KCER81JYGOG/tasks/accept_random?ref=w_pl_prvw",
        notificationAudioFilepath: "",
        notificationAudioVolume: 0.5,
        acceptUnratedRequesters: true,
        lastUpdate: "1.0.2",
        notificationMaxPay: 99,
        notificationMinPay: 0,
      });
    });
  });

  describe("undefined", () => {
    it("should return the initial state", () => {
      expect(config(undefined, {})).toEqual({
        minReward: 0.5,
        hitSort: "updated_desc",
        pageSize: 100,
        qualified: true,
        masters: false,
        to1MinComm: 0,
        to1MinPay: 3,
        to1MinFair: 3,
        to1MinFast: 0,
        lifespan: 1800000,
        active: false,
        singleAccept: true,
        captchaUrl: "https://worker.mturk.com/projects/3NIW7VZHBKXLVNM0IL5KCER81JYGOG/tasks/accept_random?ref=w_pl_prvw",
        notificationAudioFilepath: "",
        notificationAudioVolume: 0.5,
        acceptUnratedRequesters: true,
        lastUpdate: "1.0.2",
        notificationMaxPay: 99,
        notificationMinPay: 0,
      });
    });
  });
});
