/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import status from "../../../src/js/store/reducers/status";
import * as StatusActions from "../../../src/js/store/actions/statusActions";

const makeState = () => {
  return {
    loggedOut: false,
    alive: false,
    workerId: "",
  };
};

describe("Status reducer", () => {
  describe("StatusActions.SET", () => {
    it("should set multiple entries' keys", () => {
      const targets = {
        loggedOut: true,
      };

      expect(status(makeState(), StatusActions.set(targets))).toEqual({
        loggedOut: true,
        alive: false,
      });
    });
    it("should not set keys that don't already exist", () => {
      const targets = {
        adsfasdf: true,
      };

      expect(status(makeState(), StatusActions.set(targets))).toEqual({
        loggedOut: false,
        alive: false,
      });
    });
  });
  describe("undefined", () => {
    it("should return the initial state", () => {
      expect(status(undefined, {})).toEqual({
        loggedOut: false,
        alive: false,
      });
    });
  });
});
