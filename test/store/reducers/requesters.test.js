/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import requesters from "../../../src/js/store/reducers/requesters";
import * as RequestersActions from "../../../src/js/store/actions/requestersActions";

const makeState = () => {
  return [
    {
      name: "Amazon Requester Inc.- Unspun",
      id: "A1N9E8602PJQIV",
      action: "NONE",
      to1Comm: 1.45,
      to1Pay: 2.58,
      to1Fair: 4.41,
      to1Fast: 3.72,
    },
  ];
};

describe("Requesters reducer", () => {
  describe("RequestersActions.ADD", () => {
    it("should should add an array of dicts to the state ", () => {
      const additional = [
        {
          name: "asdfasdfsadf",
          id: "asdfasdfsadf",
          action: "NvcbvcvcxbONE",
          to1Comm: 1.345,
          to1Pay: 2.558,
          to1Fair: 42.41,
          to1Fast: 33.72,
        },
        {
          name: "asdgf afdgdgfa",
          id: "shgfddsgfhhfdsg",
          action: "asdfadsgfafgds",
          to1Comm: 1.443565,
          to1Pay: 2.525468,
          to1Fair: 4.414562,
          to1Fast: 312534.72,
        },
      ];

      expect(requesters(makeState(), RequestersActions.add(additional))).toEqual([
        {
          name: "Amazon Requester Inc.- Unspun",
          id: "A1N9E8602PJQIV",
          action: "NONE",
          to1Comm: 1.45,
          to1Pay: 2.58,
          to1Fair: 4.41,
          to1Fast: 3.72,
        },
        {
          name: "asdfasdfsadf",
          id: "asdfasdfsadf",
          action: "NvcbvcvcxbONE",
          to1Comm: 1.345,
          to1Pay: 2.558,
          to1Fair: 42.41,
          to1Fast: 33.72,
        },
        {
          name: "asdgf afdgdgfa",
          id: "shgfddsgfhhfdsg",
          action: "asdfadsgfafgds",
          to1Comm: 1.443565,
          to1Pay: 2.525468,
          to1Fair: 4.414562,
          to1Fast: 312534.72,
        },
      ]);
    });
    it("should not add entries with the same id", () => {
      const additional = [
        {
          name: "asdfasdfsadf",
          id: "asdfasdfsadf",
          action: "NvcbvcvcxbONE",
          to1Comm: 1.345,
          to1Pay: 2.558,
          to1Fair: 42.41,
          to1Fast: 33.72,
        },
        {
          name: "asdgf afdgdgfa",
          id: "A1N9E8602PJQIV",
          action: "asdfadsgfafgds",
          to1Comm: 1.443565,
          to1Pay: 2.525468,
          to1Fair: 4.414562,
          to1Fast: 312534.72,
        },
      ];

      expect(requesters(makeState(), RequestersActions.add(additional))).toEqual([
        {
          name: "Amazon Requester Inc.- Unspun",
          id: "A1N9E8602PJQIV",
          action: "NONE",
          to1Comm: 1.45,
          to1Pay: 2.58,
          to1Fair: 4.41,
          to1Fast: 3.72,
        },
        {
          name: "asdfasdfsadf",
          id: "asdfasdfsadf",
          action: "NvcbvcvcxbONE",
          to1Comm: 1.345,
          to1Pay: 2.558,
          to1Fair: 42.41,
          to1Fast: 33.72,
        },
      ]);
    });
  });
  describe("RequestersActions.SET", () => {
    it("should set multiple entries' keys", () => {
      const targets = [
        { id: "A1N9E8602PJQIV", action: "entryNew1" },
      ];

      expect(requesters(makeState(), RequestersActions.set(targets))).toEqual([
        {
          name: "Amazon Requester Inc.- Unspun",
          id: "A1N9E8602PJQIV",
          action: "entryNew1",
          to1Comm: 1.45,
          to1Pay: 2.58,
          to1Fair: 4.41,
          to1Fast: 3.72,
        },
      ]);
    });
    it("should not set keys that don't already exist if the id does not exist", () => {
      const targets = [
        { id: "id1", asddfjhsadf: "asddfjhsadf" },
      ];

      expect(requesters(makeState(), RequestersActions.set(targets))).toEqual([
        {
          name: "Amazon Requester Inc.- Unspun",
          id: "A1N9E8602PJQIV",
          action: "NONE",
          to1Comm: 1.45,
          to1Pay: 2.58,
          to1Fair: 4.41,
          to1Fast: 3.72,
        },
      ]);
    });
    it("should not set keys that don't already exist if the id does exist", () => {
      const targets = [
        { id: "A1N9E8602PJQIV", asddfjhsadf: "asddfjhsadf" },
      ];

      expect(requesters(makeState(), RequestersActions.set(targets))).toEqual([
        {
          name: "Amazon Requester Inc.- Unspun",
          id: "A1N9E8602PJQIV",
          action: "NONE",
          to1Comm: 1.45,
          to1Pay: 2.58,
          to1Fair: 4.41,
          to1Fast: 3.72,
        },
      ]);
    });
  });
  describe("RequestersActions.CLEAR", () => {
    it("should return the initial state", () => {
      expect(requesters(makeState(), RequestersActions.clear())).toEqual([]);
    });
  });
  describe("undefined", () => {
    it("should return the initial state", () => {
      expect(requesters(undefined, {})).toEqual([]);
    });
  });
});
