/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import captcha from "../../../src/js/store/reducers/captcha";
import * as CaptchaActions from "../../../src/js/store/actions/captchaActions";

const makeState = () => {
  return {
    imageUrl: "https://example.com",
    hitUrl: "worker.mturk.com",
    authTokenSubmit: "asdfasdfasdf",
    authTokenCaptcha: "fdgdafgadfg",
    waitingForResync: true,
  };
};

describe("Captcha reducer", () => {
  describe("CaptchaActions.SET", () => {
    it("should set the keys in the dict ", () => {
      const targets = {
        imageUrl: "asdfasdf",
        hitUrl: "asbfdadfgadfg",
        authTokenSubmit: "adsfkjsajhlks",
        authTokenCaptcha: "uoiaw joiadsf",
        waitingForResync: false,
      };

      expect(captcha(makeState(), CaptchaActions.set(targets))).toEqual({
        imageUrl: "asdfasdf",
        hitUrl: "asbfdadfgadfg",
        authTokenSubmit: "adsfkjsajhlks",
        authTokenCaptcha: "uoiaw joiadsf",
        waitingForResync: false,
      });
    });
    it("should not add keys if they do not already exist ", () => {
      const targets = {
        adsfkjhadsfjhksa: "asdfkjhasdfkj",
      };

      expect(captcha(makeState(), CaptchaActions.set(targets))).toEqual({
        imageUrl: "https://example.com",
        hitUrl: "worker.mturk.com",
        authTokenSubmit: "asdfasdfasdf",
        authTokenCaptcha: "fdgdafgadfg",
        waitingForResync: true,
      });
    });
  });

  describe("CaptchaActions.CLEAR", () => {
    it("should return the initial state", () => {
      expect(captcha(makeState(), CaptchaActions.clear())).toEqual({
        imageUrl: "",
        hitUrl: "",
        authTokenSubmit: "",
        authTokenCaptcha: "",
        waitingForResync: false,
      });
    });
  });
  describe("undefined", () => {
    it("should return the initial state", () => {
      expect(captcha(undefined, {})).toEqual({
        imageUrl: "",
        hitUrl: "",
        authTokenSubmit: "",
        authTokenCaptcha: "",
        waitingForResync: false,
      });
    });
  });
});
