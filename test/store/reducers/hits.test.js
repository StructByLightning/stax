/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import hits from "../../../src/js/store/reducers/hits";
import * as HitsActions from "../../../src/js/store/actions/hitsActions";

const makeState = () => {
  return [
    {
      hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
      requester_id: "A12M8Y27IW05FA",
      requester_name: "20bn",
      title: "[NEW TASK] Record 40 short videos clips of hand gestures in front of your webcam.",
      description: "We want to teach a robot to recognize hand gestures. Please help us by performing 40 hand gestures while standing at a given position in front of your webcam.",
      assignment_duration_in_seconds: 21600,
      creation_time: "2018-09-14T21:50:05.000Z",
      assignable_hits_count: 16,
      latest_expiration_time: "2018-09-15T00:50:05.000Z",
      last_updated_time: "2018-09-14T21:50:05.000Z",
      pay: 2,
      accept_project_task_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks/accept_random.json?ref=w_pl_prvw",
      requester_url: "/requesters/A12M8Y27IW05FA/projects.json?ref=w_pl_prvw",
      project_tasks_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks.json?ref=w_pl_prvw",
      timestamp: 1536961891986,
      lastChecked: 0,
      validated: true,
    },
  ];
};

describe("Hits reducer", () => {
  describe("HitsActions.ADD", () => {
    it("should should add an array of dicts to the state ", () => {
      const additional = [
        {
          hit_set_id: "asfdasdf",
          requester_id: "asdfasdf",
          requester_name: "sadfasfd",
          title: "asfdgadsfgadfsg",
          description: "asdgfasgdf",
          assignment_duration_in_seconds: 4235,
          creation_time: "asdfasdfadsf",
          assignable_hits_count: 125436,
          latest_expiration_time: "adsfasdf",
          last_updated_time: "adsfsadf",
          pay: 23645,
          accept_project_task_url: "asdfasdf",
          requester_url: "cxbzzdbfdafgh",
          project_tasks_url: "asgzxbgzdfsg",
          timestamp: 145145,
          lastChecked: 151350,
          validated: false,
        },
      ];

      expect(hits(makeState(), HitsActions.add(additional))).toEqual([
        {
          hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
          requester_id: "A12M8Y27IW05FA",
          requester_name: "20bn",
          title: "[NEW TASK] Record 40 short videos clips of hand gestures in front of your webcam.",
          description: "We want to teach a robot to recognize hand gestures. Please help us by performing 40 hand gestures while standing at a given position in front of your webcam.",
          assignment_duration_in_seconds: 21600,
          creation_time: "2018-09-14T21:50:05.000Z",
          assignable_hits_count: 16,
          latest_expiration_time: "2018-09-15T00:50:05.000Z",
          last_updated_time: "2018-09-14T21:50:05.000Z",
          pay: 2,
          accept_project_task_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks/accept_random.json?ref=w_pl_prvw",
          requester_url: "/requesters/A12M8Y27IW05FA/projects.json?ref=w_pl_prvw",
          project_tasks_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks.json?ref=w_pl_prvw",
          timestamp: 1536961891986,
          lastChecked: 0,
          validated: true,
        },
        {
          hit_set_id: "asfdasdf",
          requester_id: "asdfasdf",
          requester_name: "sadfasfd",
          title: "asfdgadsfgadfsg",
          description: "asdgfasgdf",
          assignment_duration_in_seconds: 4235,
          creation_time: "asdfasdfadsf",
          assignable_hits_count: 125436,
          latest_expiration_time: "adsfasdf",
          last_updated_time: "adsfsadf",
          pay: 23645,
          accept_project_task_url: "asdfasdf",
          requester_url: "cxbzzdbfdafgh",
          project_tasks_url: "asgzxbgzdfsg",
          timestamp: 145145,
          lastChecked: 151350,
          validated: false,
        },
      ]);
    });
  });
  describe("HitsActions.SET", () => {
    it("should set multiple entries' keys", () => {
      const targets = [
        {
          hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
          title: "test",
          requester_id: "adsfasdf",
        },
      ];

      expect(hits(makeState(), HitsActions.set(targets))).toEqual([
        {
          hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
          requester_id: "adsfasdf",
          requester_name: "20bn",
          title: "test",
          description: "We want to teach a robot to recognize hand gestures. Please help us by performing 40 hand gestures while standing at a given position in front of your webcam.",
          assignment_duration_in_seconds: 21600,
          creation_time: "2018-09-14T21:50:05.000Z",
          assignable_hits_count: 16,
          latest_expiration_time: "2018-09-15T00:50:05.000Z",
          last_updated_time: "2018-09-14T21:50:05.000Z",
          pay: 2,
          accept_project_task_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks/accept_random.json?ref=w_pl_prvw",
          requester_url: "/requesters/A12M8Y27IW05FA/projects.json?ref=w_pl_prvw",
          project_tasks_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks.json?ref=w_pl_prvw",
          timestamp: 1536961891986,
          lastChecked: 0,
          validated: true,
        },
      ]);
    });
    it("should not set keys that don't already exist if the id does not match", () => {
      const targets = [
        { hit_set_id: "id1", asddfjhsadf: "asddfjhsadf" },
      ];

      expect(hits(makeState(), HitsActions.set(targets))).toEqual([
        {
          hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
          requester_id: "A12M8Y27IW05FA",
          requester_name: "20bn",
          title: "[NEW TASK] Record 40 short videos clips of hand gestures in front of your webcam.",
          description: "We want to teach a robot to recognize hand gestures. Please help us by performing 40 hand gestures while standing at a given position in front of your webcam.",
          assignment_duration_in_seconds: 21600,
          creation_time: "2018-09-14T21:50:05.000Z",
          assignable_hits_count: 16,
          latest_expiration_time: "2018-09-15T00:50:05.000Z",
          last_updated_time: "2018-09-14T21:50:05.000Z",
          pay: 2,
          accept_project_task_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks/accept_random.json?ref=w_pl_prvw",
          requester_url: "/requesters/A12M8Y27IW05FA/projects.json?ref=w_pl_prvw",
          project_tasks_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks.json?ref=w_pl_prvw",
          timestamp: 1536961891986,
          lastChecked: 0,
          validated: true,
        },
      ]);

    });

    it("should not set keys that don't already exist even if the id does match", () => {
      const targets = [
        { hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5", asddfjhsadf: "asddfjhsadf" },
      ];

      expect(hits(makeState(), HitsActions.set(targets))).toEqual([
        {
          hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
          requester_id: "A12M8Y27IW05FA",
          requester_name: "20bn",
          title: "[NEW TASK] Record 40 short videos clips of hand gestures in front of your webcam.",
          description: "We want to teach a robot to recognize hand gestures. Please help us by performing 40 hand gestures while standing at a given position in front of your webcam.",
          assignment_duration_in_seconds: 21600,
          creation_time: "2018-09-14T21:50:05.000Z",
          assignable_hits_count: 16,
          latest_expiration_time: "2018-09-15T00:50:05.000Z",
          last_updated_time: "2018-09-14T21:50:05.000Z",
          pay: 2,
          accept_project_task_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks/accept_random.json?ref=w_pl_prvw",
          requester_url: "/requesters/A12M8Y27IW05FA/projects.json?ref=w_pl_prvw",
          project_tasks_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks.json?ref=w_pl_prvw",
          timestamp: 1536961891986,
          lastChecked: 0,
          validated: true,
        },
      ]);

    });
  });
  describe("HitsActions.REMOVE", () => {
    it("should remove multiple entries based on ID", () => {
      const targets = [
        "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
      ];

      expect(hits(makeState(), HitsActions.remove(targets))).toEqual([]);
    });
    it("should do nothing with entries that don't exist", () => {
      const targets = [
        "sdfa",
      ];

      expect(hits(makeState(), HitsActions.remove(targets))).toEqual([
        {
          hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
          requester_id: "A12M8Y27IW05FA",
          requester_name: "20bn",
          title: "[NEW TASK] Record 40 short videos clips of hand gestures in front of your webcam.",
          description: "We want to teach a robot to recognize hand gestures. Please help us by performing 40 hand gestures while standing at a given position in front of your webcam.",
          assignment_duration_in_seconds: 21600,
          creation_time: "2018-09-14T21:50:05.000Z",
          assignable_hits_count: 16,
          latest_expiration_time: "2018-09-15T00:50:05.000Z",
          last_updated_time: "2018-09-14T21:50:05.000Z",
          pay: 2,
          accept_project_task_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks/accept_random.json?ref=w_pl_prvw",
          requester_url: "/requesters/A12M8Y27IW05FA/projects.json?ref=w_pl_prvw",
          project_tasks_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks.json?ref=w_pl_prvw",
          timestamp: 1536961891986,
          lastChecked: 0,
          validated: true,
        },
      ]);
    });
  });
  describe("undefined", () => {
    it("should return the initial state", () => {
      expect(hits(undefined, {})).toEqual([]);
    });
  });
});
