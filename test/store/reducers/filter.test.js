/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import filter from "../../../src/js/store/reducers/filter";
import * as FilterActions from "../../../src/js/store/actions/filterActions";

const makeState = () => {
  return [
    {
      id: "id1",
      keyword: "entry1",
      type: "entry1",
    },
    {
      id: "id2",
      keyword: "entry2",
      type: "entry2",
    },
    {
      id: "id3",
      keyword: "entry3",
      type: "entry3",
    },
  ];
};

describe("Blacklist reducer", () => {
  describe("FilterActions.ADD", () => {
    it("should should add an array of dicts to the state ", () => {
      const additional = [
        { id: "id4", keyword: "entry1", type: "entry1" },
        { id: "id5", keyword: "entry2", type: "entry2" },
      ];

      expect(filter(makeState(), FilterActions.add(additional))).toEqual([
        { id: "id1", keyword: "entry1", type: "entry1" },
        { id: "id2", keyword: "entry2", type: "entry2" },
        { id: "id3", keyword: "entry3", type: "entry3" },
        { id: "id4", keyword: "entry1", type: "entry1" },
        { id: "id5", keyword: "entry2", type: "entry2" },
      ]);
    });
  });
  describe("FilterActions.SET", () => {
    it("should set multiple entries' keys", () => {
      const targets = [
        { id: "id1", keyword: "entryNew1" },
        { id: "id2", keyword: "entryNew2" },
      ];

      expect(filter(makeState(), FilterActions.set(targets))).toEqual([
        { id: "id1", keyword: "entryNew1", type: "entry1" },
        { id: "id2", keyword: "entryNew2", type: "entry2" },
        { id: "id3", keyword: "entry3", type: "entry3" },
      ]);
    });
    it("should not set keys that don't already exist", () => {
      const targets = [
        { id: "id1", asddfjhsadf: "asddfjhsadf" },
      ];

      expect(filter(makeState(), FilterActions.set(targets))).toEqual([
        { id: "id1", keyword: "entry1", type: "entry1" },
        { id: "id2", keyword: "entry2", type: "entry2" },
        { id: "id3", keyword: "entry3", type: "entry3" },
      ]);
    });
  });
  describe("FilterActions.REMOVE", () => {
    it("should remove multiple entries based on ID", () => {
      const targets = [
        "id1",
        "id2",
      ];

      expect(filter(makeState(), FilterActions.remove(targets))).toEqual([
        { id: "id3", keyword: "entry3", type: "entry3" },
      ]);
    });
  });
  describe("FilterActions.CLEAR", () => {
    it("should return the initial state", () => {
      expect(filter(makeState(), FilterActions.clear())).toEqual([]);
    });
  });
  describe("undefined", () => {
    it("should return the initial state", () => {
      expect(filter(undefined, {})).toEqual([]);
    });
  });
});
