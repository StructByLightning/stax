/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// takes an array of dicts
export const ADD = "REQUESTERS_ACTIONS_ADD";
export const add = (payload) => { return { type: ADD, payload }; };

// takes an array of dicts
export const SET = "REQUESTERS_ACTIONS_SET";
export const set = (payload) => { return { type: SET, payload }; };

export const CLEAR = "REQUESTERS_ACTIONS_CLEAR";
export const clear = () => { return { type: CLEAR }; };
