/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as ThemeActions from "../actions/themeActions";

const initialState = [
  {
    variable: "--alert-icon-bg",
    name: "Alert Icon",
    color: "#c11515",
  },
  {
    variable: "--block-container-bg",
    name: "Block",
    color: "#010101",
  },
  {
    variable: "--block-container-border-bg",
    name: "Block Border",
    color: "#732300",
  },

  {
    variable: "--block-header-bg",
    name: "Block Header",
    color: "#060606",
  },

  {
    variable: "--block-header-border-bg",
    name: "Block Header Border",
    color: "#3c1f0f",
  },
  {
    variable: "--body-bg",
    name: "Body",
    color: "#020202",
  },
  {
    variable: "--cancel-button-fg",
    name: "Cancel Button",
    color: "#c61717",
  },
  {
    variable: "--cancel-button-hover-fg",
    name: "Cancel Button (hover)",
    color: "#ff0000",
  },
  {
    variable: "--checkbox-border-bg",
    name: "Checkbox Border",
    color: "#af7e0f",
  },

  {
    variable: "--checkbox-border-hover-bg",
    name: "Checkbox Border (hover)",
    color: "#ffb714",
  },

  {
    variable: "--checkbox-mark-fill-bg",
    name: "Checkbox Mark",
    color: "#f78610",
  },

  {
    variable: "--checkbox-bg",
    name: "Checkbox",
    color: "#0c0704",
  },
  {
    variable: "--danger-button-fg",
    name: "Danger Button",
    color: "#000000",
  },
  {
    variable: "--danger-button-color",
    name: "Danger Button Text",
    color: "#ff2929",
  },
  {
    variable: "--divider-color",
    name: "Divider",
    color: "#ffb541",
  },
  {
    variable: "--drop-block-border",
    name: "Drop Block Border",
    color: "#12a0a8",
  },
  {
    variable: "--header-color",
    name: "Header",
    color: "#fab600",
  },
  {
    variable: "--input-bg",
    name: "Input",
    color: "#141414",
  },
  {
    variable: "--input-border-bg",
    name: "Input Border",
    color: "#353535",
  },
  {
    variable: "--input-border-focus-bg",
    name: "Input Border (focus)",
    color: "#b63702",
  },
  {
    variable: "--input-border-hover-bg",
    name: "Input Border (hover)",
    color: "#c64607",
  },
  {
    variable: "--input-error-color",
    name: "Input Error",
    color: "#ff0000",
  },
  {
    variable: "--link-color",
    name: "Link",
    color: "#e9872c",
  },
  {
    variable: "--link-hover-color",
    name: "Link (hover)",
    color: "#000000",
  },
  {
    variable: "--link-visited",
    name: "Link (visited)",
    color: "#ce6631",
  },
  {
    variable: "--link-hover-bg",
    name: "Link Highlight (hover)",
    color: "#e9872c",
  },
  {
    variable: "--nav-menu-item-bg",
    name: "Nav Menu",
    color: "#000000",
  },

  {
    variable: "--nav-menu-item-hover-bg",
    name: "Nav Menu (hover)",
    color: "#ffffff",
  },

  {
    variable: "--nav-menu-item-hover-font-color",
    name: "Nav Menu Text (hover)",
    color: "#000000",
  },
  {
    variable: "--primary-font-color",
    name: "Primary Text Color",
    color: "#eeedff",
  },
  {
    variable: "--raised-button-fg",
    name: "Raised Button",
    color: "#000000",
  },
  {
    variable: "--raised-button-color",
    name: "Raised Button Text",
    color: "#ffb541",
  },
  {
    variable: "--segmented-control-border-bg",
    name: "Segmented Control Border",
    color: "#d58400",
  },
  {
    variable: "--segmented-control-bg",
    name: "Segmented Control",
    color: "#381900",
  },
  {
    variable: "--segmented-control-active-bg",
    name: "Segmented Control Active",
    color: "#ce8d50",
  },
  {
    variable: "--segmented-control-hover-bg",
    name: "Segmented Control (hover)",
    color: "#853900",
  },
  {
    variable: "--segmented-control-active-color",
    name: "Segmented Control Text (active)",
    color: "#030302",
  },
  {
    variable: "--spinner-color",
    name: "Spinner",
    color: "#efb500",
  },
  {
    variable: "--table-row-bg",
    name: "Table Row",
    color: "#0a0a0a",
  },
  {
    variable: "--table-row-border-bg",
    name: "Table Row Border",
    color: "#281004",
  },
];

export default function theme(state = initialState, action) {
  switch (action.type) {
    case ThemeActions.SET:
      {
        const newState = [];
        // for each element add it to newState if it's unchanged
        // otherwise copy it, mutate the copy, and add that to newState
        for (let i = 0; i < state.length; i++) {
          let index = -1;
          for (let j = 0; j < action.payload.length; j++) {
            if (state[i].variable === action.payload[j].variable) {
              index = j;
            }
          }

          if (index !== -1) {
            const entry = JSON.parse(JSON.stringify(state[i]));
            const keys = Object.keys(action.payload[index]);
            for (let j = 0; j < keys.length; j++) {
              if (entry[keys[j]] !== undefined) {
                entry[keys[j]] = action.payload[index][keys[j]];
              }
            }
            newState.push(entry);
          } else {
            newState.push(state[i]);
          }
        }
        return newState;
      }
    case ThemeActions.CLEAR:
      {
        return initialState;
      }
    default:
      return state;
  }
}
