/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as StatusActions from "../actions/statusActions";

const initialState = {
  loggedOut: false,
  alive: false,
  workerId: "",
};

export default function status(state = initialState, action) {
  switch (action.type) {
    case StatusActions.SET:
      {
        const newState = { ...state };
        const keys = Object.keys(action.payload);
        for (let i = 0; i < keys.length; i++) {
          if (newState[keys[i]] !== undefined) {
            newState[keys[i]] = action.payload[keys[i]];
          }
        }

        return newState;
      }
    default:
      {
        return state;
      }
  }
}
