/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as ConfigActions from "../actions/configActions";

const initialState = {
  minReward: 0.5,
  hitSort: "updated_desc",
  pageSize: 100,
  qualified: true,
  masters: false,
  to1MinComm: 0,
  to1MinPay: 3,
  to1MinFair: 3,
  to1MinFast: 0,
  lifespan: 1800000,
  active: false,
  singleAccept: true,
  captchaUrl: "https://worker.mturk.com/projects/3NIW7VZHBKXLVNM0IL5KCER81JYGOG/tasks/accept_random?ref=w_pl_prvw",
  notificationAudioFilepath: "",
  notificationAudioVolume: 0.5,
  acceptUnratedRequesters: true,
  lastUpdate: "1.0.2",
  notificationMinPay: 0,
  notificationMaxPay: 99,
  hitScraperInterval: 1500,
  hitAccepterInterval: 1500,
  queueScraperInterval: 5000,
};

export default function config(state = initialState, action) {
  switch (action.type) {
    case ConfigActions.TOGGLE:
      {
        const newState = { ...state };
        for (let i = 0; i < action.payload.length; i++) {
          if (newState[action.payload[i]] === true) {
            newState[action.payload[i]] = false;
          } else if (newState[action.payload[i]] === false) {
            newState[action.payload[i]] = true;
          }
        }
        return newState;
      }
    case ConfigActions.SET:
      {
        let newState = state;
        const keys = Object.keys(action.payload);
        for (let i = 0; i < keys.length; i++) {
          if (newState[keys[i]] !== undefined) {
            newState = { ...newState, [keys[i]]: action.payload[keys[i]] };
          }
        }
        return newState;
      }
    case ConfigActions.CLEAR:
      {
        return initialState;
      }
    default:
      return state;
  }
}
