/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as RequestersActions from "../actions/requestersActions";

const initialState = [];

/*
[
  {
    "name": "Amazon Requester Inc.- Unspun",
    "id": "A1N9E8602PJQIV",
    "action": "NONE",
    "to1Comm": 1.45,
    "to1Pay": 2.58,
    "to1Fair": 4.41,
    "to1Fast": 3.72
  }
}
*/

export default function requesters(state = initialState, action) {
  switch (action.type) {
    case RequestersActions.ADD:
      {
        const newState = [...state];
        for (let i = 0; i < action.payload.length; i++) {
          let found = false;
          for (let j = 0; j < newState.length; j++) {
            if (action.payload[i].id === newState[j].id) {
              found = true;
            }
          }

          if (!found) {
            newState.push(action.payload[i]);
          }
        }
        return newState;
      }
    case RequestersActions.SET:
      {
        const newState = [];
        // for each element add it to newState if it's unchanged
        // otherwise copy it, mutate the copy, and add that to newState
        for (let i = 0; i < state.length; i++) {
          let index = -1;
          for (let j = 0; j < action.payload.length; j++) {
            if (state[i].id === action.payload[j].id) {
              index = j;
            }
          }

          if (index !== -1) {
            const entry = JSON.parse(JSON.stringify(state[i]));
            const keys = Object.keys(action.payload[index]);
            for (let j = 0; j < keys.length; j++) {
              if (entry[keys[j]] !== undefined) {
                entry[keys[j]] = action.payload[index][keys[j]];
              }
            }
            newState.push(entry);
          } else {
            newState.push(state[i]);
          }
        }
        return newState;
      }
    case RequestersActions.CLEAR:
      {
        return initialState;
      }
    default:
      return state;
  }
}
