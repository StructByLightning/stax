/* Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

import * as FilterActions from "../actions/filterActions";

const initialState = [];

/*
[
  {
    id: "ef1029f0-48c6-4f52-971c-c3b293840625",
    keyword: "39PLFW4P3MXZYLZY4U7JNNIJQXWH47",
    name: "Android and iOS Users - Survey - takes about 5 min",
    type: "HIT ID",
    action: "Reject"
  }
]
 */

export default function filter(state = initialState, action) {
  switch (action.type) {
    case FilterActions.ADD:
      {
        return [...state, ...action.payload];
      }
    case FilterActions.REMOVE:
      {
        return state.filter((entry) => {
          let found = false;
          for (let i = 0; i < action.payload.length; i++) {
            if (entry.id === action.payload[i]) {
              found = true;
            }
          }
          return !found;
        });
      }
    case FilterActions.SET:
      {
        const newState = [];
        // for each element add it to newState if it's unchanged
        // otherwise copy it, mutate the copy, and add that to newState
        for (let i = 0; i < state.length; i++) {
          let index = -1;
          for (let j = 0; j < action.payload.length; j++) {
            if (state[i].id === action.payload[j].id) {
              index = j;
            }
          }

          if (index !== -1) {
            const entry = JSON.parse(JSON.stringify(state[i]));
            const keys = Object.keys(action.payload[index]);
            for (let j = 0; j < keys.length; j++) {
              if (entry[keys[j]] !== undefined) {
                entry[keys[j]] = action.payload[index][keys[j]];
              }
            }
            newState.push(entry);
          } else {
            newState.push(state[i]);
          }
        }
        return newState;
      }
    case FilterActions.CLEAR:
      {
        return initialState;
      }
    default:
      return state;
  }
}
