/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as ActivityActions from "../actions/activityActions";

const initialState = [];


/*
[
  {
    date: "10-3-2018",
    requesterName: "Requester namee",
    requesterId: "rid",
    assignmentId: "30 alphanumeric digits",
    title: "Fun hit! $0.01/30m",
    state: "Submitted",
    pay: 0.01,
  },
];
 */

export default function activity(state = initialState, action) {
  switch (action.type) {
    case ActivityActions.MERGE:
      {
        const newState = JSON.parse(JSON.stringify(state));
        for (let i = 0; i < action.payload.length; i++) {
          let index = -1;
          for (let j = 0; j < newState.length; j++) {
            if (action.payload[i].assignmentId === newState[j].assignmentId) {
              index = j;
            }
          }

          if (index === -1) {
            newState.push(action.payload[i]);
          } else {
            newState[index].state = action.payload[i].state; // this is the only key that can change
          }
        }


        return newState;
      }
    default:
      return state;
  }
}
