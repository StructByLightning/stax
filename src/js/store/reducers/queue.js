/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as QueueActions from "../actions/queueActions";

const initialState = [];
/*
{
  "task_id": "3HA5ODM5KAT9MDZVM818GCVT9OWSV8",
  "assignment_id": "3DBQWDE4Y6ZXUQQ5YN7B24N95DI5NS",
  "accepted_at": "2018-09-14T20:58:42.000Z",
  "deadline": "2018-09-14T21:58:42.000Z",
  "time_to_deadline_in_seconds": 172,
  "hit_set_id": "3MX1CQXD04AH4DEUYXLQIXDWBTODWF",
  "requester_id": "A2DPU6JE37X0YV",
  "requester_name": "Speechfeedback",
  "title": "Rate the quality of computer-generated speech - Korean@South Korea native only [#6d78]",
  "description": "Listen to a recording of artificial speech and rate it on how correct and natural it sounds to you.",
  "assignment_duration_in_seconds": 3600,
  "creation_time": "2018-09-13T12:06:43.000Z",
  "assignable_hits_count": 6,
  "latest_expiration_time": "2018-09-20T22:00:01.000Z",
  "last_updated_time": "2018-09-13T12:06:43.000Z",
  "pay": 1,
  "requester_url": "/requesters/A2DPU6JE37X0YV/projects.json?ref=w_pl_prvw"
  "expired_task_action_url": "/projects/3MX1CQXD04AH4DEUYXLQIXDWBTODWF/tasks.json?ref=w_pl_prvw",
  "task_url": "/projects/3MX1CQXD04AH4DEUYXLQIXDWBTODWF/tasks/3HA5ODM5KAT9MDZVM818GCVT9OWSV8.json?assignment_id=3DBQWDE4Y6ZXUQQ5YN7B24N95DI5NS&ref=w_pl_prvw",
  "deleteThis": false
}
*/

export default function queue(state = initialState, action) {
  switch (action.type) {
    case QueueActions.CLEAR_AND_ADD:
      {
        return action.payload;
      }
    case QueueActions.REMOVE:
      {
        return state.filter((entry) => {
          let found = false;
          for (let i = 0; i < action.payload.length; i++) {
            if (entry.task_id === action.payload[i]) {
              found = true;
            }
          }
          return !found;
        });
      }
    default:
      return state;
  }
}
