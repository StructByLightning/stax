/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { combineReducers } from "redux";
import { routerReducer as router } from "react-router-redux";
import requesters from "./requesters";
import hits from "./hits";
import config from "./config";
import queue from "./queue";
import filter from "./filter";
import captcha from "./captcha";
import status from "./status";
import theme from "./theme";
import activity from "./activity";
import tabs from "./tabs";

const rootReducer = combineReducers({
  activity,
  captcha,
  config,
  filter,
  hits,
  queue,
  requesters,
  status,
  tabs,
  theme,
  router,
});

export default rootReducer;
