/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as HitsActions from "../actions/hitsActions";

const initialState = [];


/*
[
  {
    hit_set_id: "3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5",
    requester_id: "A12M8Y27IW05FA",
    requester_name: "20bn",
    title: "[NEW TASK] Record 40 short videos clips of hand gestures in front of your webcam.",
    description: "We want to teach a robot to recognize hand gestures. Please help us by performing 40 hand gestures while standing at a given position in front of your webcam.",
    assignment_duration_in_seconds: 21600,
    creation_time: "2018-09-14T21:50:05.000Z",
    assignable_hits_count: 16,
    latest_expiration_time: "2018-09-15T00:50:05.000Z",
    last_updated_time: "2018-09-14T21:50:05.000Z",
    pay: 2,
    accept_project_task_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks/accept_random.json?ref=w_pl_prvw",
    requester_url: "/requesters/A12M8Y27IW05FA/projects.json?ref=w_pl_prvw",
    project_tasks_url: "/projects/3K0T7FOBIJ9Q9YHLWCSHZN012MK1M5/tasks.json?ref=w_pl_prvw",
    timestamp: 1536961891986,
    lastChecked: 0,
    validated: true,
  },
];
 */

export default function hits(state = initialState, action) {
  switch (action.type) {
    case HitsActions.ADD:
      {
        return [...state, ...action.payload];
      }
    case HitsActions.REMOVE:
      {
        return state.filter((entry) => {
          let found = false;
          for (let i = 0; i < action.payload.length; i++) {
            if (entry.hit_set_id === action.payload[i]) {
              found = true;
            }
          }
          return !found;
        });
      }
    case HitsActions.SET:
      {
        const newState = [];
        // for each element add it to newState if it's unchanged
        // otherwise copy it, mutate the copy, and add that to newState
        for (let i = 0; i < state.length; i++) {
          let index = -1;
          for (let j = 0; j < action.payload.length; j++) {
            if (state[i].hit_set_id === action.payload[j].hit_set_id) {
              index = j;
            }
          }
          if (index !== -1) {
            const entry = JSON.parse(JSON.stringify(state[i]));
            const keys = Object.keys(action.payload[index]);
            for (let j = 0; j < keys.length; j++) {
              if (entry[keys[j]] !== undefined) {
                entry[keys[j]] = action.payload[index][keys[j]];
              }
            }
            newState.push(entry);
          } else {
            newState.push(state[i]);
          }
        }
        return newState;
      }
    default:
      return state;
  }
}
