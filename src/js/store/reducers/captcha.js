/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as CaptchaActions from "../actions/captchaActions";

const initialState = {
  imageUrl: "",
  hitUrl: "",
  authTokenSubmit: "",
  authTokenCaptcha: "",
  waitingForResync: false,
};

export default function captcha(state = initialState, action) {
  switch (action.type) {
    case CaptchaActions.SET:
      {
        let newState = state;
        const keys = Object.keys(action.payload);
        for (let i = 0; i < keys.length; i++) {
          if (newState[keys[i]] !== undefined) {
            newState = { ...newState, [keys[i]]: action.payload[keys[i]] };
          }
        }
        return newState;
      }
    case CaptchaActions.CLEAR:
      {
        return initialState;
      }
    default:
      return state;
  }
}
