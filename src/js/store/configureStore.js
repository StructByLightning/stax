/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { createStore, applyMiddleware, compose } from "redux";
import { createMemoryHistory } from "history";
import { routerMiddleware } from "react-router-redux";
import { createLogger } from "redux-logger";
import { persistStore, persistReducer, createMigrate } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web and AsyncStorage for react-native
import rootReducer from "./reducers";

const history = createMemoryHistory();

const migrations = {
  0: (state) => {
    return {
      ...state,
      config: {
        ...state.config,
        singleAccept: true,
      },
    };
  },
  4: (state) => {
    return {
      ...state,
      config: {
        ...state.config,
        notificationAudioFilepath: "",
        notificationAudioVolume: 0.5,
        acceptUnratedRequesters: true,
      },
    };
  },
  5: (state) => {
    return {
      blacklist: [...state.blacklist],
      captcha: {
        authTokenCaptcha: "",
        authTokenSubmit: "",
        hitUrl: "",
        imageUrl: "",
        waitingForResync: false,
      },
      config: {
        acceptUnratedRequesters: state.config.acceptUnratedRequesters,
        active: state.config.active,
        hitSort: state.config.scraper.hitSort,
        lifespan: state.config.accepter.lifespan,
        masters: state.config.scraper.masters,
        minReward: state.config.scraper.minReward,
        notificationAudioFilepath: state.config.notificationAudioFilepath,
        notificationAudioVolume: state.config.notificationAudioVolume,
        pageSize: state.config.scraper.pageSize,
        qualified: state.config.scraper.qualified,
        singleAccept: state.config.singleAccept,
        to1MinComm: state.config.accepter.to1MinComm,
        to1MinFair: state.config.accepter.to1MinFair,
        to1MinFast: state.config.accepter.to1MinFast,
        to1MinPay: state.config.accepter.to1MinPay,
      },
      hits: [],
      queue: [],
      requesters: [],
      status: {
        loggedOut: false,
        alive: false,
      },
      router: {
        locationBeforeTransitions: null,
      },
    };
  },
  6: (state) => {
    return {
      ...state,
      config: {
        ...state.config,
        lastUpdate: "1.0.2",
        notificationMinPay: 0,
        notificationMaxPay: 99,
      },
      filter: [...state.blacklist.map((item) => {
        item.action = "REJECT";
        return item;
      })],
    };
  },
  7: (state) => {
    return {
      ...state,
      config: {
        ...state.config,
        hitScraperInterval: 1500,
        hitAccepterInterval: 1500,
        queueScraperInterval: 5000,
      },
    };
  },
  8: (state) => {
    return {
      ...state,
      status: {
        ...state.status,
        workerId: "",
      },
    };
  },
};


// prepare redux-persist
const persistConfig = {
  key: "root-test2",
  storage,
  blacklist: [
    "hits",
    "queue",
    "captcha",
    "status",
  ],
  version: 8,
  migrate: createMigrate(migrations, { debug: true }),
};


const persistedReducer = persistReducer(persistConfig, rootReducer);

const configureStore = () => {
  // Redux Configuration
  const middleware = [];
  const enhancers = [];


  // Logging Middleware
  createLogger({
    level: "info",
    collapsed: true,
  });

  // Skip redux logs in console during the tests
  if (process.env.NODE_ENV !== "test") {
    //  middleware.push(logger);
  }

  // Router Middleware
  const router = routerMiddleware(history);
  middleware.push(router);

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = compose;
  /* eslint-enable no-underscore-dangle */

  // Apply Middleware & Compose Enhancers
  enhancers.push(applyMiddleware(...middleware));
  const enhancer = composeEnhancers(...enhancers);

  // Create Store
  const store = createStore(persistedReducer, {}, enhancer);

  if (module.hot) {
    module.hot.accept(
      "./reducers",
      () => { return store.replaceReducer(require("./reducers")); }, // eslint-disable-line global-require
    );
  }
  return store;
};

export const store = configureStore();
export const persistor = persistStore(store, null);
