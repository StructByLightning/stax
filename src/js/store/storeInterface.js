/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/* global chrome */
import * as ActivityActions from "./actions/activityActions";
import * as FilterActions from "./actions/filterActions";
import * as CaptchaActions from "./actions/captchaActions";
import * as ConfigActions from "./actions/configActions";
import * as HitsActions from "./actions/hitsActions";
import * as QueueActions from "./actions/queueActions";
import * as RequestersActions from "./actions/requestersActions";
import * as StatusActions from "./actions/statusActions";
import * as ThemeActions from "./actions/themeActions";
import { store } from "./configureStore";
import { mtc } from "../background/mturk/mturkController";
import * as Errors from "../constants/errors";

export default class StoreInterface {
  constructor() {
    this.numActivityRequests = 0;
  }

  // this method should be called by the background script
  listen() {
    chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
      switch (request.type) {
        case "GET_STORE":
          {
            sendResponse({ type: "GET_STORE_RESPONSE", error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case FilterActions.ADD:
          {
            store.dispatch(FilterActions.add(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case FilterActions.REMOVE:
          {
            store.dispatch(FilterActions.remove(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case FilterActions.SET:
          {
            store.dispatch(FilterActions.set(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case FilterActions.CLEAR:
          {
            store.dispatch(FilterActions.clear());
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case CaptchaActions.SET:
          {
            store.dispatch(CaptchaActions.set(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case CaptchaActions.CLEAR:
          {
            store.dispatch(CaptchaActions.clear());
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case ConfigActions.TOGGLE:
          {
            store.dispatch(ConfigActions.toggle(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case ConfigActions.SET:
          {
            store.dispatch(ConfigActions.set(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case ConfigActions.CLEAR:
          {
            store.dispatch(ConfigActions.clear());
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case HitsActions.ADD:
          {
            store.dispatch(HitsActions.add(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case HitsActions.REMOVE:
          {
            store.dispatch(HitsActions.remove(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case HitsActions.SET:
          {
            store.dispatch(HitsActions.set(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case QueueActions.CLEAR_AND_ADD:
          {
            store.dispatch(QueueActions.clearAndAdd(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case QueueActions.REMOVE:
          {
            store.dispatch(QueueActions.remove(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case RequestersActions.ADD:
          {
            store.dispatch(RequestersActions.add(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case RequestersActions.SET:
          {
            store.dispatch(RequestersActions.set(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case RequestersActions.CLEAR:
          {
            store.dispatch(RequestersActions.clear());
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case StatusActions.SET:
          {
            store.dispatch(StatusActions.set(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case ThemeActions.SET:
          {
            store.dispatch(ThemeActions.set(request.payload));
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case ThemeActions.CLEAR:
          {
            store.dispatch(ThemeActions.clear());
            sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
            break;
          }
        case "RETURN_HIT":
          {
            mtc.getMApi().returnHit(request.payload, (error, payload) => {
              sendResponse({ error, payload });
            });
            break;
          }
        case "SUBMIT_CAPTCHA":
          {
            mtc.getMApi().submitCaptcha(request.payload, (error, payload) => {
              sendResponse({ error, payload });
            });
            break;
          }
        case "SYNC_ACTIVITY":
          {
            const today = new Date();

            this.numActivityRequests += 100;
            for (let i = 0; i < 30; i++) {
              const priorDate = new Date();
              priorDate.setDate(today.getDate() - i);
              let dateString = priorDate.getFullYear() + "-";
              if (priorDate.getMonth() + 1 < 10) {
                dateString += "0" + (priorDate.getMonth() + 1);
              } else {
                dateString += (priorDate.getMonth() + 1);
              }
              dateString += "-";
              if (priorDate.getDate() < 10) {
                dateString += "0" + priorDate.getDate();
              } else {
                dateString += priorDate.getDate();
              }

              this.numActivityRequests += 1;
              this._getActivityPage(dateString, 1, sendResponse);
            }
            this.numActivityRequests -= 100;

            break;
          }
        default:
          {
            console.warn("StoreInterface received unknown request of type", request.type);
          }
      }
      return true;
    });
  }

  _getActivityPage(date, counter, sendResponse) {
    mtc.getMApi().getActivityPage("https://worker.mturk.com/status_details/" + date + "?page_number=" + counter + "&format=json", (error, payload) => {
      if (payload !== null && payload.length > 0) {
        const dated = [];
        for (let i = 0; i < payload.length; i++) {
          dated.push({ ...payload[i], date });
        }
        store.dispatch(ActivityActions.merge(dated));
        this._getActivityPage(date, counter + 1, sendResponse);
      } else {
        this.numActivityRequests -= 1;
        if (this.numActivityRequests === 0) {
          sendResponse({ error: Errors.NO_ERROR, payload: store.getState() });
        }
      }
    });
  }

  getStore(callback) {
    chrome.runtime.sendMessage({ type: "GET_STORE" }, (response) => {
      callback(response.error, response.payload);
    });
  }

  returnHit(payload, callback) {
    chrome.runtime.sendMessage({ type: "RETURN_HIT", payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  submitCaptcha(payload, callback) {
    chrome.runtime.sendMessage({ type: "SUBMIT_CAPTCHA", payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  syncActivity(callback) {
    chrome.runtime.sendMessage({ type: "SYNC_ACTIVITY" }, (response) => {
      if (response !== undefined) {
        callback(response.error, response.payload);
      } else {
        callback(Errors.UNKNOWN, null);
      }
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // Filter
  // ///////////////////////////////////////////////////////////////////////////
  addFilter(payload, callback) {
    chrome.runtime.sendMessage({ type: FilterActions.ADD, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  removeFilter(payload, callback) {
    chrome.runtime.sendMessage({ type: FilterActions.REMOVE, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  setFilter(payload, callback) {
    chrome.runtime.sendMessage({ type: FilterActions.SET, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  clearFilter(callback) {
    chrome.runtime.sendMessage({ type: FilterActions.CLEAR }, (response) => {
      callback(response.error, response.payload);
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // captcha
  // ///////////////////////////////////////////////////////////////////////////
  setCaptcha(payload, callback) {
    chrome.runtime.sendMessage({ type: CaptchaActions.SET, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  clearCaptcha(callback) {
    chrome.runtime.sendMessage({ type: CaptchaActions.CLEAR }, (response) => {
      callback(response.error, response.payload);
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // config
  // ///////////////////////////////////////////////////////////////////////////
  toggleConfig(payload, callback) {
    chrome.runtime.sendMessage({ type: ConfigActions.TOGGLE, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  setConfig(payload, callback) {
    chrome.runtime.sendMessage({ type: ConfigActions.SET, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  clearConfig(callback) {
    chrome.runtime.sendMessage({ type: ConfigActions.CLEAR }, (response) => {
      callback(response.error, response.payload);
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // Hits
  // ///////////////////////////////////////////////////////////////////////////
  addHits(payload, callback) {
    chrome.runtime.sendMessage({ type: HitsActions.ADD, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  removeHits(payload, callback) {
    chrome.runtime.sendMessage({ type: HitsActions.REMOVE, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  setHits(payload, callback) {
    chrome.runtime.sendMessage({ type: HitsActions.SET, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // queue
  // ///////////////////////////////////////////////////////////////////////////
  clearAndAddQueue(payload, callback) {
    chrome.runtime.sendMessage({ type: QueueActions.CLEAR_AND_ADD, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  removeQueue(payload, callback) {
    chrome.runtime.sendMessage({ type: QueueActions.REMOVE, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // requesters
  // ///////////////////////////////////////////////////////////////////////////
  addRequesters(payload, callback) {
    chrome.runtime.sendMessage({ type: RequestersActions.ADD, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  setRequesters(payload, callback) {
    chrome.runtime.sendMessage({ type: RequestersActions.SET, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  clearRequesters(callback) {
    chrome.runtime.sendMessage({ type: RequestersActions.CLEAR }, (response) => {
      callback(response.error, response.payload);
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // status
  // ///////////////////////////////////////////////////////////////////////////
  setStatus(payload, callback) {
    chrome.runtime.sendMessage({ type: StatusActions.SET, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  // ///////////////////////////////////////////////////////////////////////////
  // theme
  // ///////////////////////////////////////////////////////////////////////////
  setTheme(payload, callback) {
    chrome.runtime.sendMessage({ type: ThemeActions.SET, payload }, (response) => {
      callback(response.error, response.payload);
    });
  }

  clearTheme(callback) {
    chrome.runtime.sendMessage({ type: ThemeActions.CLEAR }, (response) => {
      callback(response.error, response.payload);
    });
  }

}
