/* global chrome */
import "../img/stax_16.png";
import "../img/stax_32.png";
import "../img/stax_48.png";
import "../img/stax_128.png";
import "../img/stax_chrome.png";

import { store } from "./store/configureStore";
import { mtc } from "./background/mturk/mturkController";
import StoreInterface from "./store/storeInterface";
import * as CaptchaActions from "./store/actions/captchaActions";
import * as ConfigActions from "./store/actions/configActions";
import * as Requests from "./constants/types";
import * as TabsActions from "./store/actions/tabsActions";
import TabWatcher from "./background/tabWatcher/tabWatcher";

const CURRENT_VERSION = "1.1.3";

// listen for messages from other parts of Stax
const si = new StoreInterface();
si.listen();

// start up the worker threads to scrape/accept/monitor mturk
mtc.run();

// monitor chrome tabs for important changes
const tw = new TabWatcher();
tw.run();


setInterval(() => { console.info(store.getState()); }, 1000);

if (chrome.runtime.onMessage !== undefined) { // allow npm run start to work
  chrome.runtime.onMessage.addListener((request) => {
    if (request.type === Requests.CONTENT_SET_CAPTCHA) {
      store.dispatch(CaptchaActions.set({
        imageUrl: request.imageUrl,
        hitUrl: request.hitUrl,
        authTokenCaptcha: request.authTokenCaptcha,
        authTokenSubmit: request.authTokenSubmit,
        waitingForResync: false,
      }));
    }
  });
}

// open options.html when the stax extension icon is clicked
chrome.browserAction.onClicked.addListener(() => {
  if (store.getState().config.lastUpdate === CURRENT_VERSION) {
    chrome.tabs.create({ url: chrome.runtime.getURL("options.html#/queue") });
  } else {
    store.dispatch(ConfigActions.set({ lastUpdate: CURRENT_VERSION }));
    chrome.tabs.create({ url: chrome.runtime.getURL("options.html#/changelog") });
  }
});

// tab watcher
setInterval(() => {
  chrome.tabs.query({}, (tabs) => {
    const tabsToAdd = [];
    const tabsToSet = [];
    for (let i = 0; i < tabs.length; i++) {
      // don't track non mturk tabs
      const assignmentId = decodeURIComponent(tabs[i].url.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent("assignment_id").replace(/[.+*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
      if (assignmentId.length > 0) {
        // check if this tab is already recorded in the store
        let exists = false;
        const currentTabs = store.getState().tabs;
        for (let j = 0; j < currentTabs.length; j++) {
          if (tabs[i].id === currentTabs[j].tabId) { // same tab
            if (assignmentId === currentTabs[j].assignmentId) { // same hit too
              exists = true;
            }
          }
        }

        // if it's a new tab make a new entry otherwise just update the existing entry
        if (!exists) {
          tabsToAdd.push({
            opened: new Date().getTime(),
            closed: new Date().getTime(),
            assignmentId,
            id: "" + tabs[i].id + assignmentId,
            tabId: tabs[i].id,
          });
        } else {
          tabsToSet.push({
            id: tabs[i].id + assignmentId,
            closed: new Date().getTime(),
          });
        }
      }
    }

    if (tabsToAdd.length > 0) {
      store.dispatch(TabsActions.add(tabsToAdd));
    }

    if (tabsToSet.length > 0) {
      store.dispatch(TabsActions.set(tabsToSet));
    }
  });
}, 1000);

// // paste mturk id from the context menu
// chrome.contextMenus.removeAll(() => {
//   chrome.contextMenus.create({
//     title: "Paste Mturk ID",
//     contexts: ["all"],
//     onclick: (info, tab) => {
//       function inputWorkerId(text) {
//         const activeElement = document.activeElement;
//
//         if (activeElement.nodeName === "INPUT") {
//           const nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
//           nativeInputValueSetter.call(activeElement, text);
//
//           const ev2 = new Event("input", { bubbles: true });
//           activeElement.dispatchEvent(ev2);
//         }
//       }
//
//       chrome.tabs.executeScript(tab.id, { code: "(" + inputWorkerId + ")(\"" + store.getState().status.workerId + "\")", allFrames: true }, () => {});
//     },
//   });
// });
