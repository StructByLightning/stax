/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import StoreInterface from "./store/storeInterface";

window.onload = () => {
  const results = document.getElementsByClassName("captcha-image");
  if (results.length > 0) {
    const captchaImage = results[0];
    const authTokenCaptchaInput = document.getElementsByName("classic_image_captcha[captcha_token]")[0];
    const authTokenSubmitInput = document.getElementById("submitAcceptForm").firstChild.firstChild;

    const url = captchaImage.src; // hosted on aws servers
    const authTokenCaptcha = authTokenCaptchaInput.value;
    const authTokenSubmit = authTokenSubmitInput.value;

    const si = new StoreInterface();
    si.setCaptcha({
      imageUrl: url,
      hitUrl: window.location.href,
      authTokenCaptcha,
      authTokenSubmit,
      waitingForResync: false,
    }, () => {});

    window.location.href = "https://example.com"; // changing the url signals that we're done and this tab can be cleaned up by the main script
  } else {
    window.location.href = "https://example.com"; // changing the url signals that we're done and this tab can be cleaned up by the main script
  }
};
