/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import React from "react";
import ReactDOM from "react-dom";
import App from "./options/app";
import "./options/styles/base.scss";
import * as Functions from "./options/functions";
import StoreInterface from "./store/storeInterface";

ReactDOM.render(<App />, document.getElementById("root"));

const si = new StoreInterface();
si.getStore((error, payload) => {
  for (let i = 0; i < payload.theme.length; i++) {
    Functions.applyThemeVariable(payload.theme[i].variable, payload.theme[i].color);
  }
});
