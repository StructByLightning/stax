/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* global self */
import * as Requests from "../../constants/requests";
import * as Errors from "../../constants/errors";

// abstract class for all web worker classes
// must define execute in the child class
export default class Worker {
  constructor() {
    this.interval = 1500;
    this.state = null;

    if (this.execute === undefined) {
      throw new TypeError("Must define execute in child class");
    }
    if (this.handleMessage === undefined) {
      throw new TypeError("Must define handleMessage in child class");
    }
    if (this.updateInterval === undefined) {
      throw new TypeError("Must define updateInterval in child class");
    }
  }

  run() {
    // do this here to make sure the "loop" is always running even if an iteration crashes
    setTimeout(() => { this.run(); }, this.interval);

    this.sendMessage(Requests.MTC_GET_STORE, Errors.NO_ERROR, null);

    // this is needed for the first loop, when state is still null
    if (this.state !== null) {
      this.updateInterval();
    }

    // do whatever the child class needs, assuming stax is running and the options page is open
    if (this.state !== null && this.state.config.active && this.state.status.alive) {
      this.execute();
    }
  }

  setState(state) {
    this.state = state;
  }

  /*
    Description: Send a message to the parent worker
    Arguments:
      type: a constant defined in constants/types
      payload: any kind of serializable data
    Returns: N/A
   */
  sendMessage(type, error, payload) {
    self.postMessage({ type, error, payload });
  }
}
