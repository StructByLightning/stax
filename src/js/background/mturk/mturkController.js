/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import HitScraper from "../hitScraper/hitScraper.worker";
import QueueScraper from "../queueScraper/queueScraper.worker";
import HitAccepter from "../hitAccepter/hitAccepter.worker";
import { store } from "../../store/configureStore";
import MturkApi from "./mturkApi";
import * as HitsActions from "../../store/actions/hitsActions";
import * as QueueActions from "../../store/actions/queueActions";
import * as RequestersActions from "../../store/actions/requestersActions";
import * as FilterActions from "../../store/actions/filterActions";
import * as StatusActions from "../../store/actions/statusActions";
import * as Requests from "../../constants/requests";
import * as Errors from "../../constants/errors";
import * as Types from "../../constants/types";

const uuidv4 = require("uuid/v4");

export default class MturkController {
  constructor() {
    this.mApi = new MturkApi();
  }

  getMApi() {
    return this.mApi;
  }

  run() {
    this.mApi.run(); // allow api to accept requests

    this.startHitScraper();
    this.startQueueScraper();
    this._startHitAccepter();

    this.startSyncRequesters();
  }

  startHitScraper() {
    const mApi = this.mApi; // bypass this bug
    const hitScraperWorker = new HitScraper();
    hitScraperWorker.onmessage = (event) => {
      if (event.data.type === Requests.MTC_GET_STORE) {
        hitScraperWorker.postMessage({ type: Requests.MTC_GET_STORE_RESPONSE, payload: store.getState() });
      } else if (event.data.type === Requests.MTC_GET_HITS) {
        mApi.getHits((error, payload) => {
          hitScraperWorker.postMessage({ type: Requests.MTC_GET_HITS_RESPONSE, error, payload });
        });
      } else if (event.data.type === Requests.MTC_ADD_HITS) {
        store.dispatch(HitsActions.add(event.data.payload));
      }
    };
  }

  startQueueScraper() {
    const mApi = this.mApi; // bypass this bug
    const queueWorker = new QueueScraper();
    queueWorker.onmessage = (event) => {
      if (event.data.type === Requests.MTC_GET_STORE) {
        queueWorker.postMessage({ type: Requests.MTC_GET_STORE_RESPONSE, payload: store.getState() });
      } else if (event.data.type === Requests.MTC_CLEAR_AND_ADD_QUEUE) {
        // check to see if the new queue contains a new hit
        const currentQueue = store.getState().queue;
        const newQueue = event.data.payload;
        const minPay = store.getState().config.notificationMinPay;
        const maxPay = store.getState().config.notificationMaxPay;
        let playAudio = false;
        for (let i = 0; i < newQueue.length; i++) {
          let found = false;
          for (let j = 0; j < currentQueue.length; j++) {
            if (newQueue[i].task_id === currentQueue[j].task_id) {
              found = true;
            }
          }
          if (!found) {
            if (newQueue[i].pay >= minPay && newQueue[i].pay <= maxPay) {
              playAudio = true;
            }
          }
        }

        // if it does, play the defined sound notification
        const state = store.getState();
        if (playAudio && state.status.alive && state.config.active) {
          const filepath = "./" + store.getState().config.notificationAudioFilepath;
          if (filepath.length > 2) {
            const audio = new Audio(filepath);
            audio.volume = store.getState().config.notificationAudioVolume;
            audio.play();
          }

        }

        store.dispatch(QueueActions.clearAndAdd(event.data.payload));
      } else if (event.data.type === Requests.MTC_GET_QUEUE) {
        mApi.getQueue((error, payload) => {
          if (error === Errors.LOGGED_OUT) {
            store.dispatch(StatusActions.set({ loggedOut: true }));
          } else if (error === Errors.NO_ERROR) {
            store.dispatch(StatusActions.set({ loggedOut: false }));
          }
          queueWorker.postMessage({ type: Requests.MTC_GET_QUEUE_RESPONSE, error, payload });
        });
      }
    };
  }

  // every 60 seconds make TO1 requests to keep the local requester data synced
  startSyncRequesters() {
    const requesters = store.getState().requesters;

    this._syncRequesters(requesters);
  }

  _syncRequesters(requesters) {
    let url = "https://turkopticon.ucsd.edu/api/multi-attrs.php?ids=";
    for (let i = 0; i < requesters.length && i < 100; i++) {
      url += requesters[i].id;
      if (i < requesters.length - 1 && i < 99) {
        url += ",";
      }
    }

    this.mApi.getTo1(url, (error, payload) => {
      if (error === Errors.NO_ERROR) {
        store.dispatch(RequestersActions.set(payload));
      }
    });

    if (requesters.length > 100) {
      setTimeout(() => {
        this._syncRequesters(requesters.slice(100));
      }, 500);
    } else {
      setTimeout(() => {
        this._syncRequesters(store.getState().requesters);
      }, 60000);
    }
  }

  _startHitAccepter() {
    const mApi = this.mApi; // bypass this bug
    const hitAccepterWorker = new HitAccepter();
    hitAccepterWorker.onmessage = (event) => {
      // if (store.getState().config.active) {
      if (event.data.type === Requests.MTC_GET_STORE) {
        hitAccepterWorker.postMessage({ type: Requests.MTC_GET_STORE_RESPONSE, payload: store.getState() });
      } else if (event.data.type === Requests.MTC_GET_TO1) {
        mApi.getTo1(event.data.payload, (error, payload) => {
          hitAccepterWorker.postMessage({ type: Requests.MTC_GET_TO1_RESPONSE, error, payload });
        });
      } else if (event.data.type === Requests.MTC_ACCEPT_HIT) {
        mApi.acceptHit(event.data.payload.url, (error, payload) => {
          if (store.getState().config.singleAccept) {
            if (error === Errors.NO_ERROR) {
              const entry = {
                keyword: event.data.payload.hitSetId,
                type: FilterActions.FILTER_TYPE_HIT_ID,
                name: event.data.payload.title,
                id: uuidv4(),
                action: Types.FILTER_REJECT,
              };
              store.dispatch(FilterActions.add([entry]));
            }
          }

          hitAccepterWorker.postMessage({ type: Requests.MTC_ACCEPT_HIT_RESPONSE, error, payload });
        });
      } else if (event.data.type === Requests.MTC_SET_HITS) {
        store.dispatch(HitsActions.set(event.data.payload));
      } else if (event.data.type === Requests.MTC_ADD_REQUESTERS) {
        store.dispatch(RequestersActions.add(event.data.payload));
      } else if (event.data.type === Requests.MTC_REMOVE_HITS) {
        store.dispatch(HitsActions.remove(event.data.payload));
      } else if (event.data.type === Requests.MTC_GET_CAPTCHA_URL) {
        mApi.getCaptchaUrl((error, payload) => {
          hitAccepterWorker.postMessage({ type: Requests.MTC_GET_CAPTCHA_URL_RESPONSE, error, payload });
        });
      } else if (event.data.type === Requests.MTC_SYNC_CAPTCHA) {
        mApi.syncCaptcha(event.data.payload, () => {
          // no need to do anything
        });
      }
      // }
    };
  }
}

export const mtc = new MturkController();
