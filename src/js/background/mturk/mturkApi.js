/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* global chrome */

import { store } from "../../store/configureStore";
import * as Errors from "../../constants/errors";

const REQUEST_QUEUE = "REQUEST_QUEUE";
const REQUEST_HITS = "REQUEST_HITS";
const REQUEST_ACCEPT_HIT = "REQUEST_ACCEPT_HIT";
const REQUEST_GET_CAPTCHA_URL = "REQUEST_GET_CAPTCHA_URL";
const REQUEST_RETURN_HIT = "REQUEST_RETURN_HIT";
const REQUEST_SYNC_CAPTCHA = "REQUEST_SYNC_CAPTCHA";
const REQUEST_SUBMIT_CAPTCHA = "REQUEST_SUBMIT_CAPTCHA";
const REQUEST_GET_ACTIVITY_PAGE = "REQUEST_GET_ACTIVITY_PAGE";

export default class MturkApi {
  constructor() {
    this.interval = 600; // frequency in ms to make an MTurk call

    this.requestQueue = [];

    this.authToken = null;
    this.queueRequests = 0;
    this.hitsRequests = 0;
    this.acceptRequests = 0;
  }

  enqueueRequest(type, callback, data, priority, unique) {
    let alreadyExists = false;
    for (let i = 0; i < this.requestQueue.length; i++) {
      if (type === this.requestQueue[i].type && unique) {
        alreadyExists = true;
      }
    }
    if (!alreadyExists) {
      const req = {
        type,
        callback,
        data,
        priority,
        unique,
      };
      this.requestQueue.push(req);
    } else {
      callback(Errors.RATE_LIMIT, null);
    }
  }

  run() {
    setInterval(() => {
      this._process();
    }, this.interval);
  }

  _process() {
    const index = this._getNextRequest();
    if (index !== -1) {
      console.info("Executing MturkApi request:", this.requestQueue[index]);
      //  console.log("Queue:", this.requestQueue, " Executing:", this.requestQueue[index].type);
      if (this.requestQueue[index].type === REQUEST_QUEUE) {
        this._getQueue(this.requestQueue[index].callback);
      } else if (this.requestQueue[index].type === REQUEST_HITS) {
        this._getHits(this.requestQueue[index].callback);
      } else if (this.requestQueue[index].type === REQUEST_ACCEPT_HIT) {
        this._acceptHit(this.requestQueue[index].data.url, this.requestQueue[index].callback);
      } else if (this.requestQueue[index].type === REQUEST_GET_CAPTCHA_URL) {
        this._getCaptchaUrl(this.requestQueue[index].callback);
      } else if (this.requestQueue[index].type === REQUEST_RETURN_HIT) {
        this._returnHit(this.requestQueue[index].data.url, this.requestQueue[index].callback);
      } else if (this.requestQueue[index].type === REQUEST_SYNC_CAPTCHA) {
        this._syncCaptcha(this.requestQueue[index].data.url, this.requestQueue[index].callback);
      } else if (this.requestQueue[index].type === REQUEST_SUBMIT_CAPTCHA) {
        this._submitCaptcha(this.requestQueue[index].data.value, this.requestQueue[index].callback);
      } else if (this.requestQueue[index].type === REQUEST_GET_ACTIVITY_PAGE) {
        this._getActivityPage(this.requestQueue[index].data.url, this.requestQueue[index].callback);
      }
      this.requestQueue.splice(index, 1); // remove the completed request
    }
  }

  _getNextRequest() {
    if (this.requestQueue.length > 0) {
      let highestPriority = 0;

      // increment priorities
      for (let i = 0; i < this.requestQueue.length; i++) {
        this.requestQueue[i].priority++;
      }

      for (let i = 0; i < this.requestQueue.length; i++) {
        if (this.requestQueue[highestPriority].priority <= this.requestQueue[i].priority) {
          highestPriority = i;
        }
      }

      return highestPriority;
    }

    return -1;
  }

  // description: adds a getQueue request to the queue
  // returns: nothing
  // arguments:
  //   callback: called with (error, payload) when the request is processed
  //     error can be one of:
  //       ERROR_NO_ERROR: operation successful
  //       ERROR_RATE_LIMIT: mturk PRE rate limit
  getQueue(callback) {
    this.enqueueRequest(REQUEST_QUEUE, callback, null, 3, true);
  }

  getHits(callback) {
    this.enqueueRequest(REQUEST_HITS, callback, null, 2, true);
  }

  acceptHit(url, callback) {
    // don't add requests if we're waiting for the user to fix a captcha
    if (!store.getState().captcha.exists) {
      this.enqueueRequest(REQUEST_ACCEPT_HIT, callback, { url }, 2, true);
    } else {
      callback(Errors.WAITING_FOR_CAPTCHA, null);
    }
  }

  getCaptchaUrl(callback) {
    this.enqueueRequest(REQUEST_GET_CAPTCHA_URL, callback, null, 3, true);
  }

  syncCaptcha(url, callback) {
    this.enqueueRequest(REQUEST_SYNC_CAPTCHA, callback, { url }, 3, true);
  }

  getActivityPage(url, callback) {
    this.enqueueRequest(REQUEST_GET_ACTIVITY_PAGE, callback, { url }, 999, false);
  }

  submitCaptcha(value, callback) {
    this.enqueueRequest(REQUEST_SUBMIT_CAPTCHA, callback, { value }, 999);
  }

  returnHit(url, callback) {
    this.enqueueRequest(REQUEST_RETURN_HIT, callback, { url }, 999, false);
  }

  _getQueue(callback) {
    const request = new XMLHttpRequest();
    const url = "https://worker.mturk.com/tasks?format=json";

    request.open("GET", url, true);
    request.onload = () => {
      this._getQueueOnload(callback, request);
    };
    request.send(null);
  }

  _getQueueOnload(callback, request) {
    if (request.status !== 429) { // 429 means the server is rate limiting and didn't return valid data
      if (this.isJson(request.responseText)) {
        const results = JSON.parse(request.responseText).tasks;
        for (let i = 0; i < results.length; i++) {
          results[i].deleteThis = false;
        }
        callback(Errors.NO_ERROR, results);
      } else {
        callback(Errors.LOGGED_OUT, null);
      }
    } else {
      callback(Errors.RATE_LIMIT, null);
    }
  }

  _getActivityPage(url, callback) {
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onload = () => {
      this._getActivityPageOnload(callback, request);
    };
    request.send(null);
  }

  _getActivityPageOnload(callback, request) {
    if (request.status !== 429) { // 429 means the server is rate limiting and didn't return valid data
      if (this.isJson(request.responseText)) {
        const processed = [];
        const results = JSON.parse(request.responseText).results;

        for (let i = 0; i < results.length; i++) {
          processed.push({
            requesterName: results[i].requester_name,
            requesterId: results[i].requester_id,
            assignmentId: results[i].assignment_id,
            title: results[i].title,
            state: results[i].state,
            pay: results[i].reward.amount_in_dollars,
          });
        }
        callback(Errors.NO_ERROR, processed);
      } else {
        callback(Errors.LOGGED_OUT, null);
      }
    } else {
      callback(Errors.RATE_LIMIT, null);
    }
  }

  _getHits(callback) {
    const request = new XMLHttpRequest();
    const state = store.getState();
    const url = "https://worker.mturk.com/projects?" +
      "page_size=" + state.config.pageSize +
      "&filters%5Bqualified%5D=" + state.config.qualified +
      "&filters%5Bmasters%5D=" + state.config.masters +
      "&sort=" + state.config.hitSort +
      "&filters%5Bmin_reward%5D=" + state.config.minReward +
      "&page_number=1&format=json";

    request.open("GET", url, true);
    request.onload = () => {
      this._getHitsOnload(callback, request);
    };

    // http codes won't trigger this
    request.onerror = () => {
      callback(Errors.UNKNOWN, null);
    };
    request.send(null);
  }

  _getHitsOnload(callback, request) {
    if (request.status !== 429) { // 429 means the server is rate limiting and didn't return valid data
      if (this.isJson(request.responseText)) {
        callback(Errors.NO_ERROR, JSON.parse(request.responseText).results);
      } else {
        callback(Errors.LOGGED_OUT, null);
      }
    } else {
      callback(Errors.RATE_LIMIT, null);
    }
  }

  _acceptHit(url, callback) {
    const request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.onload = () => {
      this._acceptHitOnload(callback, request);
    };

    // http codes won't trigger this
    request.onerror = () => {
      callback(Errors.UNKNOWN, null);
    };

    request.send(null);
  }

  _acceptHitOnload(callback, request) {
    if (this.isJson(request.responseText)) {
      const data = JSON.parse(request.responseText);

      // standard "you didn't get this hit" message, no action required
      if (data.message.toLowerCase().includes("there are no more of these hits available")) {
        callback(Errors.HIT_UNAVAILABLE, null);
        // console.log("Hit was unavailable"));
      } else if (data.message.toLowerCase().includes("accepted the maximum number")) { // user has 25 hits already in their queue
        callback(Errors.QUEUE_FULL, null);
        // console.log("Too many hits in queue");
      }
    } else if (request.responseText.includes("captcha")) { // if a hit was available and captcha appeared
      callback(Errors.CAPTCHA_FOUND, null);
    } else if (request.responseText.includes("There are no more of these HITs available")) { // red box appeared, only shows if captcha and no hits left
      callback(Errors.CAPTCHA_FOUND, null);
    } else if (request.responseText.includes("Return")) { // this button only appears if the hit was successfully accepted
      callback(Errors.NO_ERROR, null);
    } else { // unknown
      callback(Errors.UNKNOWN, null);
    }
  }

  isJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  // this api call is direct because it doesn't need to be throttled
  getTo1(url, callback) {
    const request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.onload = () => {
      this._getTo1Onload(callback, request);
    };

    // http codes won't trigger this
    request.onerror = () => {
      callback(Errors.UNKNOWN, null);
    };

    request.send(null);
  }

  _getTo1Onload(callback, request) {
    let data = [];
    if (this.isJson(request.responseText)) {
      data = JSON.parse(request.responseText);
    }

    const requesters = [];
    for (const [key, value] of Object.entries(data)) {
      if (value.attrs !== undefined) {
        requesters.push({
          name: value.name,
          id: key,
          action: "NONE",
          to1: {
            comm: parseFloat(value.attrs.comm),
            pay: parseFloat(value.attrs.pay),
            fair: parseFloat(value.attrs.fair),
            fast: parseFloat(value.attrs.fast),
          },
        });
      } else if (!store.getState().config.acceptUnratedRequesters) { // unrated requesters are to be considered bad
        requesters.push({
          name: "This requester is not in the TO1 database.",
          id: key,
          action: "NONE",
          to1: {
            comm: -100,
            pay: -100,
            fair: -100,
            fast: -100,
          },
        });
      } else { // unrated requesters are to be considered good
        requesters.push({
          name: "This requester is not in the TO1 database.",
          id: key,
          action: "NONE",
          to1: {
            comm: 100,
            pay: 100,
            fair: 100,
            fast: 100,
          },
        });
      }
    }

    callback(Errors.NO_ERROR, requesters);
  }

  _getCaptchaUrl(callback) {
    const request = new XMLHttpRequest();
    request.open("GET", "https://worker.mturk.com/projects/?filters%5Bmasters%5D=false&filters%5Bmin_reward%5D=0.01&filters%5Bqualified%5D=true&page_size=100&sort=num_hits_desc&page_number=1&format=json", true);
    request.onload = () => {
      this._getCaptchaUrlOnload(callback, request);
    };

    // http codes won't trigger this
    request.onerror = () => {
      callback(Errors.UNKNOWN, null);
    };

    request.send(null);
  }

  _getCaptchaUrlOnload(callback, request) {
    const data = JSON.parse(request.responseText).results;
    console.log(data);
    let url = "https://worker.mturk.com" + data[0].accept_project_task_url.slice(0, data[0].accept_project_task_url.indexOf("/accept_random"));
    url += "?stax=true";

    callback(Errors.NO_ERROR, url);
  }

  _syncCaptcha(url, callback) {
    // don't get a new captcha if there's already one in the store waiting to be solved
    if (!store.getState().captcha.imageUrl.length > 0 && !this.syncingCaptcha) {
      this.syncingCaptcha = true;
      let captchaTab = null;
      chrome.tabs.create({ url, active: false, pinned: true }, (tab) => {
        captchaTab = tab;
      });

      chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
        if (captchaTab !== null) { // otherwise this will try to close a tab that's already been closed
          if (captchaTab.id === tabId) {
            if (tab.url !== url) { // ignore the first load where it goes from no url to the captcha url
              chrome.tabs.remove(tab.id);
              this.syncingCaptcha = false;
              captchaTab = null;
            }
          }
        }
      });

      // user might close the tab while it's working
      chrome.tabs.onRemoved.addListener((tabId) => {
        if (captchaTab !== null) { // already been handled normally
          if (captchaTab.id === tabId) {
            this.syncingCaptcha = false;
          }
        }
      });

      // failsafe to prevent being unable to ever retrieve another captcha, just in case the user closes the tab or something
      // setTimeout(() => {
      //   that.syncingCaptcha = false;
      // }, parseInt(120000, 10));

      callback(Errors.NO_ERROR, null);
    } else {
      callback(Errors.ALREADY_SOLVING_CAPTCHA, null);
    }
  }

  _submitCaptcha(value, callback) {
    const state = store.getState();

    let url = state.captcha.hitUrl;

    url = url.slice(0, url.indexOf("/tasks") + 6);
    url += "/ASDHKASDKASDASDKASDASDASDASDKA"; // this would be a hit id, but a random string works too
    url += "/accept";

    // now make xhr post requests with the token
    const request = new XMLHttpRequest();

    request.open("POST", url, true);
    const urlEncodedData = (encodeURIComponent("authenticity_token") + "=" + encodeURIComponent(state.captcha.authTokenSubmit) + "&" + encodeURIComponent("classic_image_captcha[customer_solution]") + "=" + encodeURIComponent(value) + "&" + encodeURIComponent("classic_image_captcha[captcha_token]") + "=" + encodeURIComponent(state.captcha.authTokenCaptcha)).replace(/%20/g, "+");

    // Define what happens on successful data submission
    const that = this;
    request.addEventListener("load", () => {
      if (request.status === 302 || request.status === 500) { // success, 500 = internal error, which is to be expected (garbage id was used)
        that.syncingCaptcha = false;
        callback(Errors.NO_ERROR, null);
      } else if (request.status === 429) { // rate limit
        that.syncingCaptcha = false;
        callback(Errors.RATE_LIMIT, null);
      } else if (request.status === 200) {
        that.syncingCaptcha = false;
        callback(Errors.INCORRECT_CAPTCHA_SUBMISSION, null);
      } else { // something else happened
        that.syncingCaptcha = false;
        callback(Errors.UNKNOWN, null);
      }
    });

    // http codes won't trigger this
    request.onerror = () => {
      callback(Errors.UNKNOWN, null);
    };

    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(urlEncodedData);
  }

  // if this is the first time or the auth token has expired, generate a new one
  _returnHit(url, callback) {
    if (this.authToken === null) {
      const request = new XMLHttpRequest();
      const requestUrl = "https://worker.mturk.com/tasks";
      const that = this;
      request.open("GET", requestUrl, true);
      request.onload = (event) => {
        if (event.currentTarget.status === 200) { // success
          let authToken = "";
          let index = request.responseText.indexOf("<meta name=\"csrf-token\" content=\"") + 33;
          while (request.responseText.charAt(index) !== "\"") {
            authToken += request.responseText.charAt(index);
            index++;
          }
          that.authToken = authToken;
          that._returnHitWithAuthToken(url, callback);
        } else if (event.currentTarget.status === 429) { // rate limit
          callback(Errors.RATE_LIMIT, null);
        }
      };

      // http codes won't trigger this
      request.onerror = () => {
        callback(Errors.UNKNOWN, null);
      };

      request.send(null);
    } else {
      this._returnHitWithAuthToken(url, callback);
    }
  }

  _returnHitWithAuthToken(url, callback) {
    // now make xhr post requests with the token
    const request = new XMLHttpRequest();
    const requestUrl = url;

    request.open("POST", requestUrl, true);
    const urlEncodedData = (encodeURIComponent("_method") + "=" + encodeURIComponent("delete") + "&" + encodeURIComponent("authenticity_token") + "=" + encodeURIComponent(this.authToken)).replace(/%20/g, "+");

    // Define what happens on successful data submission
    const that = this;
    request.addEventListener("load", (event) => {
      if (event.currentTarget.status === 302) { // success
        callback(Errors.NO_ERROR, null);
      } else if (event.currentTarget.status === 429) { // rate limit
        callback(Errors.RATE_LIMIT, null);
      } else if (event.currentTarget.status === 200) { // this usually happens if we tried to return a nonexistent hit
        callback(Errors.NO_ERROR, null);
      } else { // something else happened, probably the auth token is out of date so start over
        that.authToken = null;
        that.returnHit(url, callback);
      }
    });

    // http codes won't trigger this
    request.onerror = () => {
      callback(Errors.UNKNOWN, null);
    };

    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(urlEncodedData);
  }
}
