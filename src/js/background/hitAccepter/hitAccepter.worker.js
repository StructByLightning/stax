/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import HitAccepter from "./hitAccepter";
import * as Requests from "../../constants/requests";

const hitAccepter = new HitAccepter();
hitAccepter.run();

self.onmessage = (event) => {
  if (event.data.type === Requests.MTC_GET_STORE_RESPONSE) {
    hitAccepter.updateState(event.data.payload);
  } else if (event.data.type === Requests.MTC_GET_TO1_RESPONSE) {
    hitAccepter.updateRequesters(event.data.error, event.data.payload);
  } else if (event.data.type === Requests.MTC_ACCEPT_HIT_RESPONSE) {
    hitAccepter.handleHitAcceptance(event.data.error);
  } else if (event.data.type === Requests.MTC_GET_CAPTCHA_URL_RESPONSE) {
    hitAccepter.syncCaptcha(event.data.error, event.data.payload);
  }
};
