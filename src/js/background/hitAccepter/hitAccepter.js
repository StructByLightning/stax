/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* global self */
import * as Requests from "../../constants/requests";
import * as Errors from "../../constants/errors";
import * as Types from "../../constants/types";

export default class HitAccepter {
  constructor() {
    this.mturkUrl = "https://worker.mturk.com";
    this.to1Url = "https://turkopticon.ucsd.edu/api/multi-attrs.php?ids=";
    this.state = null;
    this.interval = 1500;

    this.working = false;
    this.timeSinceLastFlagFlip = 0;
  }

  /*
    Description: Check whether a hit has been watched long enough
    Arguments:
      hit: a hit object with a timestamp field
    Returns: boolean
  */
  hitIsExpired(hit) {
    let valid = true;
    const date = new Date().getTime();
    if (date - this.state.config.lifespan > hit.timestamp) {
      valid = false;
    }

    return !valid;
  }

  /*
    Description: Check whether a hit's pay is high enough
    Arguments:
      hit: a hit object with a pay field
    Returns: boolean
  */
  hitIsBelowPay(hit) {
    let valid = true;
    if (parseFloat(this.state.config.minReward) >= hit.pay) {
      valid = false;
    }
    return !valid;
  }

  /*
    Description: Find the matching filter entry's action for the given hit
    Arguments:
      hit: a hit object with the following fields:
        requester_id
        hit_set_id
        title
    Returns: the action associated with the matched entry, or Types.FILTER_NOTHING if no entry was found
  */
  hitIsOnFilter(hit) {
    let result = Types.FILTER_NOTHING;

    for (let j = 0; j < this.state.filter.length && result !== Types.FILTER_ACCEPT; j++) {
      if (this.state.filter[j].keyword.length > 0) { // assume that the user does not want to filter anything if the keyword is empty
        if (this.state.filter[j].type === Types.FILTER_TYPE_REQUESTER_ID) {
          if (hit.requester_id === this.state.filter[j].keyword) {
            result = this.state.filter[j].action;
          }
        } else if (this.state.filter[j].type === Types.FILTER_TYPE_HIT_ID) {
          if (hit.hit_set_id === this.state.filter[j].keyword) {
            result = this.state.filter[j].action;
          }
        } else if (this.state.filter[j].type === Types.FILTER_TYPE_DESCRIPTION) {
          if (hit.description.toLowerCase().includes(this.state.filter[j].keyword.toLowerCase())) {
            result = this.state.filter[j].action;
          }
          if (hit.title.toLowerCase().includes(this.state.filter[j].keyword.toLowerCase())) {
            result = this.state.filter[j].action;
          }
        }
      }
    }

    return result;
  }

  /*
    Description: Find the requester index associated with a given hit
    Arguments:
      hit: a hit object with the following fields:
        requester_id
    Returns: the index if the found requester
  */
  getHitRequester(hit) {
    let index = -1;
    for (let i = 0; i < this.state.requesters.length; i++) {
      if (hit.requester_id === this.state.requesters[i].id) {
        index = i;
      }
    }
    return index;
  }

  /*
    Description: Figure out which hits should be rejected and what requesters should be looked up.
    Arguments: N/A
    Returns: a dictionary with the following fields:
      hitsToBeRemoved: a list of hit objects that should be rejected
      tempRequesters: a list of new requesters that aren't in the database
  */
  validateHits() {
    const hitsToBeRemoved = [];
    const tempRequesters = [];
    const hitsToBeValidated = [];
    for (let i = 0; i < this.state.hits.length; i++) {
      const filterResult = this.hitIsOnFilter(this.state.hits[i]);
      const requesterIndex = this.getHitRequester(this.state.hits[i]);

      if (filterResult === Types.FILTER_ACCEPT) { // check if on whitelist
        hitsToBeValidated.push(this.state.hits[i].hit_set_id);
        this.state.hits[i].validated = true;
      } else if (this.hitIsExpired(this.state.hits[i])) { // check if blacklisted
        this.state.hits[i].validated = false;
        hitsToBeRemoved.push(this.state.hits[i].hit_set_id);
      } else if (this.hitIsBelowPay(this.state.hits[i])) { // check if below pay
        this.state.hits[i].validated = false;
        hitsToBeRemoved.push(this.state.hits[i].hit_set_id);
      } else if (filterResult === Types.FILTER_REJECT) { // check if blacklisted
        this.state.hits[i].validated = false;
        hitsToBeRemoved.push(this.state.hits[i].hit_set_id);
      } else if (requesterIndex === -1) { // requester is new so suspend judgement for now
        tempRequesters.push(this.state.hits[i].requester_id); // record this for later usage
      } else if (!this.requesterMeetsRequirements(this.state.requesters[requesterIndex], this.state.config)) {
        this.state.hits[i].validated = false;
        hitsToBeRemoved.push(this.state.hits[i].hit_set_id);
      } else {
        hitsToBeValidated.push(this.state.hits[i].hit_set_id);
        this.state.hits[i].validated = true;
      }
    }

    this.sendValidatedHitInfo(hitsToBeValidated);

    return { hitsToBeRemoved, tempRequesters };
  }

  sendValidatedHitInfo(hits) {
    const payload = [];
    for (let i = 0; i < hits.length; i++) {
      const entry = {};
      entry.hit_set_id = hits[i];
      entry.validated = true;
      payload.push(entry);
    }
    this.sendMessage(Requests.MTC_SET_HITS, payload);
  }


  /*
    Description: Send a message to the parent worker
    Arguments:
      type: a constant defined in constants/types
      payload: any kind of serializable data
    Returns: N/A
   */
  sendMessage(type, payload) {
    self.postMessage({ type, payload });
  }

  run() {
    this.execute();
  }

  execute() {
    // do this here to make sure the "loop" is always running even if an iteration crashes
    setTimeout(() => { this.execute(); }, this.interval);

    self.postMessage({ type: Requests.MTC_GET_STORE });

    // this is needed for the first loop, when state is still null
    if (this.state !== null) {
      this.interval = this.state.config.hitAccepterInterval;
    }

    // don't do anything if the user has turned Stax off or if the Stax options page is closed
    if (this.state !== null && this.state.config.active && this.state.status.alive) {
      if (!this.working) {
        this.timeSinceLastFlagFlip = 0;
        this.working = true;
        postMessage({ type: Requests.MTC_GET_STORE }); // triggers updateState
      }
      this.timeSinceLastFlagFlip += this.interval;
      if (this.timeSinceLastFlagFlip > this.interval * 60) {
        this.working = false;
      }
    }
  }

  /*
    Description: set this.state to value and call getNewTo1 if stax is active and alive
    Arguments:
      value: the new state
    Returns: N/A
   */
  updateState(value) {
    this.state = value;

    // don't do anything if the user has turned Stax off or if the Stax options page is closed
    // but do set the flag so the next iteration won't get confused
    if (this.state.config.active && this.state.status.alive) {
      this.getNewTo1();
    } else {
      this.working = false;
    }
  }


  getNewTo1() {
    const data = this.validateHits();
    const tempRequesters = data.tempRequesters;
    const hitsToBeRemoved = data.hitsToBeRemoved;
    postMessage({ type: Requests.MTC_REMOVE_HITS, payload: hitsToBeRemoved });

    // remove those hits from our local state too. partially fixes bug #19
    for (let i = this.state.hits.length - 1; i >= 0; i--) {
      let shouldBeRemoved = false;
      for (let j = 0; j < hitsToBeRemoved.length; j++) {
        if (this.state.hits[i].hit_set_id === hitsToBeRemoved[j].hit_set_id) {
          shouldBeRemoved = true;
        }
      }

      if (shouldBeRemoved) {
        this.state.hits[i].splice(i, 1);
      }
    }

    // make an async call to TO1 and when it responds, add all the new requesters to the store
    if (tempRequesters.length > 0) {
      let url = this.to1Url;
      for (let i = 0; i < tempRequesters.length; i++) {
        url += tempRequesters[i];
        if (i < tempRequesters.length - 1) {
          url += ",";
        }
      }
      postMessage({ type: Requests.MTC_GET_TO1, payload: url }); // triggers updateRequesters
    }
    this.acceptOldestHit(); // don't wait for TO to respond in order to minimize time between scraping a fresh hit and trying to accept it
  }

  /*
    Description: If there was an error print it, otherwise add to our local requesters and send a message to the parent worker
    Arguments:
      error: a constant defined in constants/errors
      value: a list of requesters
    Returns: N/A
   */
  updateRequesters(error, value) {
    if (error === Errors.NO_ERROR) {
      this.state.requesters = [...this.state.requesters, ...value];
      this.sendMessage(Requests.MTC_ADD_REQUESTERS, [...value]);
    } else {
      console.error(error);
    }
  }

  acceptOldestHit() {
    if (this.state.hits.length > 0) {
      let oldest = 0;
      let found = false;
      for (let i = 0; i < this.state.hits.length; i++) {
        if (this.state.hits[i].validated) { // unvalidated hits don't have a requester on our list so just ignore them until they do
          if (this.state.hits[i].lastChecked <= this.state.hits[oldest].lastChecked) {
            found = true;
            oldest = i;
          }
        }
      }
      if (found) {
        const url = "https://worker.mturk.com" + this.state.hits[oldest].accept_project_task_url + "?format=json";
        const hitSetId = this.state.hits[oldest].hit_set_id;
        const title = this.state.hits[oldest].title;
        postMessage({ type: Requests.MTC_ACCEPT_HIT, payload: { url, title, hitSetId } });
        const id = this.state.hits[oldest].hit_set_id;
        postMessage({ type: Requests.MTC_SET_HITS, payload: [{ hit_set_id: id, lastChecked: new Date().getTime() }] });
      }
    }
    this.working = false; // moved here so this thread will only send one request at a time
  }

  /*
    Description: If there was a captcha then send a message to the parent worker
    Arguments:
      error: a constant defined in constants/errors
    Returns: N/A
   */
  handleHitAcceptance(error) {
    if (error === Errors.NO_ERROR) {
      // don't do anything
    } else if (error === Errors.RATE_LIMIT) {
      // also don't do anything
    } else if (error === Errors.CAPTCHA_FOUND) {
      this.sendMessage(Requests.MTC_GET_CAPTCHA_URL, null);
    } else if (error === Errors.HIT_UNAVAILABLE) {
      // also don't do anything
    } else if (error === Errors.QUEUE_FULL) {
      // also don't do anything
    }
  }

  /*
    Description: If there was no error then send a message to the parent worker
    Arguments:
      error: a constant defined in constants/errors
      url: a string
    Returns: N/A
   */
  syncCaptcha(error, url) {
    if (error === Errors.NO_ERROR) {
      this.sendMessage(Requests.MTC_SYNC_CAPTCHA, url);
    }
  }

  /*
    Description: Compare the requester's to1 object with state.config settings and return false if the requester doesn't meet minimum standards
    Arguments:
      requester: an object containing a to1 object
    Returns: N/A
   */
  requesterMeetsRequirements(requester) {
    if ((parseFloat(requester.to1.pay) >= parseFloat(this.state.config.to1MinPay)) &&
      (parseFloat(requester.to1.fair) >= parseFloat(this.state.config.to1MinFair)) &&
      (parseFloat(requester.to1.fast) >= parseFloat(this.state.config.to1MinFast)) &&
      (parseFloat(requester.to1.comm) >= parseFloat(this.state.config.to1MinComm))) {
      return true;
    }
    return false;
  }
}
