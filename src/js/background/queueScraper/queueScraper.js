/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* global self */
import * as Requests from "../../constants/requests";
import * as Errors from "../../constants/errors";
import Worker from "../worker/worker";

// this class will repeatedly populate the store's queue array with the current queue
export default class QueueScraper extends Worker {
  /*
    Description: Request a new queue. Receiving the queue will trigger processQueue(). Called repeatedly by the Worker parent.
    Arguments: N/A
    Returns: N/A
  */
  execute() {
    this.sendMessage(Requests.MTC_GET_QUEUE, Errors.NO_ERROR, null);
  }

  /*
    Description: Set interval, which is used for the Worker parent class
    Arguments: N/A
    Returns: N/A
  */
  updateInterval() {
    this.interval = this.state.config.queueScraperInterval;
  }

  /*
    Description: Handle incoming messages from the parent process
    Arguments:
      type: a constant defined in constants/requests
      error: a constant defined in constants/errors
      payload: any data
    Returns: N/A
  */
  handleMessage(type, error, payload) {
    if (type === Requests.MTC_GET_STORE_RESPONSE) {
      this.setState(payload);
    } else if (type === Requests.MTC_GET_QUEUE_RESPONSE) {
      this.processQueue(error, payload);
    }
  }

  /*
    Description: Convert the raw queue data into something easier to use and send a message back to the parent process
    Arguments:
      error: a constant defined in constants/errors
      data: raw queue data, left in the same format as received from mturk
    Returns: N/A
  */
  processQueue(error, data) {
    if (error === Errors.NO_ERROR) {
      const payload = [];
      for (let i = 0; i < data.length; i++) {
        payload.push(this.convertRawQueueEntry(data[i]));
      }
      this.sendMessage(Requests.MTC_CLEAR_AND_ADD_QUEUE, Errors.NO_ERROR, payload);
    }
  }

  /*
    Description: Flatten and rename the raw queue data
    Arguments:
      rawEntry: raw queue entry data, left in the same format as received from mturk
    Returns: a stax-formatted queue entry
  */
  convertRawQueueEntry(rawEntry) {
    const entry = {};
    entry.accepted_at = rawEntry.accepted_at;
    entry.task_id = rawEntry.task_id;
    entry.assignment_id = rawEntry.assignment_id;
    entry.deadline = rawEntry.deadline;
    entry.time_to_deadline_in_seconds = rawEntry.time_to_deadline_in_seconds;
    entry.expired_task_action_url = rawEntry.expired_task_action_url;
    entry.task_url = rawEntry.task_url;
    entry.deleteThis = rawEntry.deleteThis;

    entry.hit_set_id = rawEntry.project.hit_set_id;
    entry.requester_id = rawEntry.project.requester_id;
    entry.requester_name = rawEntry.project.requester_name;
    entry.title = rawEntry.project.title;
    entry.description = rawEntry.project.description;
    entry.assignment_duration_in_seconds = rawEntry.project.assignment_duration_in_seconds;
    entry.creation_time = rawEntry.project.creation_time;
    entry.assignable_hits_count = rawEntry.project.assignable_hits_count;
    entry.latest_expiration_time = rawEntry.project.latest_expiration_time;
    entry.last_updated_time = rawEntry.project.last_updated_time;
    entry.requester_url = rawEntry.project.requester_url;

    entry.pay = rawEntry.project.monetary_reward.amount_in_dollars;

    return entry;
  }
}
