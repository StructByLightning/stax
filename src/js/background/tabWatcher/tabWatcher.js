/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* global chrome */
import { store } from "../../store/configureStore";
import * as StatusActions from "../../store/actions/statusActions";

const STAX_OPTIONS_PAGE_TITLE = "Stax | Mturk Autoscraper";


export default class TabWatcher {
  run() {
    this._watchForStaxTab();
  }

  _watchForStaxTab() {
    setInterval(() => {
      chrome.tabs.query({}, (tabs) => {
        let found = false;
        for (let i = 0; i < tabs.length; i++) {
          if (tabs[i].title === STAX_OPTIONS_PAGE_TITLE) {
            found = true;
          }
        }

        store.dispatch(StatusActions.set({ alive: found }));
      });
    }, 1000);
  }
}
