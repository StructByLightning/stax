/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* global self */
import * as Requests from "../../constants/requests";
import * as Errors from "../../constants/errors";
import Worker from "../worker/worker";


// this class will repeatedly populate the store's hits array with fresh hits
// it does not process the data (except to add a timestamp and a few empty fields)
export default class HitScraper extends Worker {
  /*
    Description: Request a new set of hits. Receiving the hits will trigger processHits(). Called repeatedly by the Worker parent.
    Arguments: N/A
    Returns: N/A
  */
  execute() {
    this.sendMessage(Requests.MTC_GET_HITS, Errors.NO_ERROR, null);
  }

  /*
    Description: Set interval, which is used for the Worker parent class
    Arguments: N/A
    Returns: N/A
  */
  updateInterval() {
    this.interval = this.state.config.hitScraperInterval;
  }


  /*
    Description: Handle incoming messages from the parent process
    Arguments:
      type: a constant defined in constants/requests
      error: a constant defined in constants/errors
      payload: any data
    Returns: N/A
  */
  handleMessage(type, error, payload) {
    if (type === Requests.MTC_GET_STORE_RESPONSE) {
      this.setState(payload);
    } else if (type === Requests.MTC_GET_HITS_RESPONSE) {
      this.processHits(error, payload);
    }
  }

  /*
    Description: Convert the raw hit data into something easier to use
    Arguments:
      error: a constant defined in constants/errors
      newHits: raw hit data, left in the same format as received from mturk
    Returns: N/A
  */
  processHits(error, newHits) {
    if (error === Errors.NO_ERROR) {
      // merge any new hits
      const hitsToAdd = [];
      for (let i = 0; i < newHits.length; i++) {
        // check if the hit is already in the store
        let alreadyExists = false;
        for (let j = 0; j < this.state.hits.length; j++) {
          if (newHits[i].hit_set_id === this.state.hits[j].hit_set_id) {
            alreadyExists = true;
          }
        }

        if (!alreadyExists) {
          hitsToAdd.push(this.convertRawHit(newHits[i]));
        }
      }
      this.sendMessage(Requests.MTC_ADD_HITS, Errors.NO_ERROR, hitsToAdd);
    }
  }

  /*
    Description: Convert the raw hit data into something easier to use. Also adds a couple fields
    Arguments:
      rawHit: raw hit data, left in the same format as received from mturk
    Returns: a stax-formatted hit
  */
  convertRawHit(rawHit) {
    const newHit = {};
    newHit.hit_set_id = rawHit.hit_set_id;
    newHit.requester_id = rawHit.requester_id;
    newHit.requester_name = rawHit.requester_name;
    newHit.title = rawHit.title;
    newHit.description = rawHit.description;
    newHit.assignment_duration_in_seconds = rawHit.assignment_duration_in_seconds;
    newHit.creation_time = rawHit.creation_time;
    newHit.assignable_hits_count = rawHit.assignable_hits_count;
    newHit.latest_expiration_time = rawHit.latest_expiration_time;
    newHit.last_updated_time = rawHit.last_updated_time;
    newHit.pay = rawHit.monetary_reward.amount_in_dollars;
    newHit.accept_project_task_url = rawHit.accept_project_task_url;
    newHit.requester_url = rawHit.requester_url;
    newHit.project_tasks_url = rawHit.project_tasks_url;
    newHit.timestamp = rawHit.timestamp;
    newHit.lastChecked = rawHit.lastChecked;
    newHit.validated = rawHit.validated;
    newHit.timestamp = new Date().getTime();
    newHit.lastChecked = 0; // this gives the hit max priority for later
    newHit.validated = false;
    return newHit;
  }
}
