/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import { Switch, Route, HashRouter } from "react-router-dom";

import Filter from "./components/filterView";
import ConfigView from "./components/configView";
import HitsView from "./components/hitsView";
import ActivityView from "./components/activityView";
import NavMenu from "./components/navMenu/navMenu";
import QueueView from "./components/queueView";
import ChangelogView from "./components/changelogView";

class App extends Component {
  render() {
    return (
      <HashRouter>
        <div className="stax-parent">
          <NavMenu />
          <div className="stax-content-parent">
            <Switch>
              <Route exact path="/activity" component={ActivityView} />
              <Route exact path="/changelog" component={ChangelogView} />
              <Route exact path="/config" component={ConfigView} />
              <Route exact path="/filter" component={Filter} />
              <Route exact path="/hits" component={HitsView} />
              <Route exact path="/queue" component={QueueView} />
            </Switch>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;
