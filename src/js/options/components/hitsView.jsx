/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import ReactTable from "react-table";
import FlexView from "react-flexview";
import CancelButton from "./widgets/cancelButton";
import ProtectButton from "./widgets/protectButton";
import StoreInterface from "../../store/storeInterface";
import * as Types from "../../constants/types";

const uuidv4 = require("uuid/v4");

export default class HitsView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sorted: [],
      page: 0,
      pageSize: 17,
      expanded: {},
      resized: [],
      filtered: [],
      hits: null,
      filter: null,
    };

    this.mounted = false;

    this.si = new StoreInterface();
    this.si.getStore((error, payload) => {
      this.setState({ hits: payload.hits, filter: payload.filter });
    });

    this.intervalHandle = setInterval(() => {
      this.si.getStore((error, payload) => {
        console.log(payload.hits)
        this.setState({ hits: payload.hits, filter: payload.filter });
      });
    }, 1000);


    this.renderRequesterCell = this.renderRequesterCell.bind(this);
    this.renderTitleCell = this.renderTitleCell.bind(this);
  }

  componentWillUnmount() {
    clearInterval(this.intervalHandle);
  }

  getNumValidHits() {
    let counter = 0;
    for (let i = 0; i < this.state.hits.length; i++) {
      if (this.state.hits[i].validated) {
        counter++;
      }
    }
    return counter;
  }

  getColumnWidth(rows, accessor, headerText) {
    const maxWidth = 400;
    const magicSpacing = 9;

    let cellLength = headerText.length;
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      if (row.validated) {
        const len = ("" + rows[i][accessor]).length;

        if (len > cellLength) {
          cellLength = len;
        }
      }
    }
    let num = Math.min(maxWidth, cellLength * magicSpacing);

    // add spacing for the buttons
    if (accessor === "requester_name") {
      num += 3 * magicSpacing;
    } else if (accessor === "timestamp") {
      num += 3 * magicSpacing;
    }

    return num;
  }

  getNumPages() {
    let numPages = 0;
    if (this.state.hits.length > this.state.pageSize) {
      let numValidHits = 0;
      for (let i = 0; i < this.state.hits.length; i++) {
        if (this.state.hits[i].validated) {
          numValidHits++;
        }
      }
      numPages = Math.ceil(numValidHits / this.state.pageSize) - 1;
    }

    return numPages;
  }

  renderRequesterCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" marginTop="0.25em" marginBottom="0.25em" className="normal-text">
        <CancelButton
          disabled={() => {
            let isDisabled = false;
            const list = this.state.filter;
            for (let i = 0; i < list.length; i++) {
              if ((this.state.hits[cellInfo.index].requester_id === list[i].keyword) && (Types.FILTER_TYPE_REQUESTER_ID === list[i].type)) {
                if (Types.FILTER_REJECT === list[i].action) {
                  isDisabled = true;
                }
              }
            }
            return isDisabled;
          }}
          title="Blacklist requester ID"
          onClick={() => {
            this.si.addFilter([{
              keyword: this.state.hits[cellInfo.index].requester_id,
              type: Types.FILTER_TYPE_REQUESTER_ID,
              name: this.state.hits[cellInfo.index].requester_name,
              id: uuidv4(),
              action: Types.FILTER_REJECT,
            }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
        />
        <ProtectButton
          disabled={() => {
            let isDisabled = false;
            const list = this.state.filter;
            for (let i = 0; i < list.length; i++) {
              if ((this.state.hits[cellInfo.index].requester_id === list[i].keyword) && (Types.FILTER_TYPE_REQUESTER_ID === list[i].type)) {
                if (Types.FILTER_ACCEPT === list[i].action) {
                  isDisabled = true;
                }
              }
            }
            return isDisabled;
          }}
          title="Whitelist requester ID"
          onClick={() => {
            this.si.addFilter([{
              keyword: this.state.hits[cellInfo.index].requester_id,
              type: Types.FILTER_TYPE_REQUESTER_ID,
              name: this.state.hits[cellInfo.index].requester_name,
              id: uuidv4(),
              action: Types.FILTER_ACCEPT,
            }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
        />
        <div className="normal-text hits-table-cell">{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderTitleCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" marginTop="0.25em" marginRight="4px" marginBottom="0.25em" className="normal-text">
        <CancelButton
          disabled={() => {
            let isDisabled = false;
            const list = this.state.filter;
            for (let i = 0; i < list.length; i++) {
              if ((this.state.hits[cellInfo.index].hit_set_id === list[i].keyword) && (Types.FILTER_TYPE_HIT_ID === list[i].type)) {
                if (Types.FILTER_REJECT === list[i].action) {
                  isDisabled = true;
                }
              }
            }
            return isDisabled;
          }}
          onClick={() => {
            const name = this.state.hits[cellInfo.index].hit_set_id;
            this.si.addFilter([{
              keyword: name,
              type: Types.FILTER_TYPE_HIT_ID,
              name: this.state.hits[cellInfo.index].title,
              id: uuidv4(),
              action: Types.FILTER_REJECT,
            }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
          title="Blacklist HIT ID"
        />
        <ProtectButton
          disabled={() => {
            let isDisabled = false;
            const list = this.state.filter;
            for (let i = 0; i < list.length; i++) {
              if ((this.state.hits[cellInfo.index].hit_set_id === list[i].keyword) && (Types.FILTER_TYPE_HIT_ID === list[i].type)) {
                if (Types.FILTER_ACCEPT === list[i].action) {
                  isDisabled = true;
                }
              }
            }
            return isDisabled;
          }}
          onClick={() => {
            const name = this.state.hits[cellInfo.index].hit_set_id;
            this.si.addFilter([{
              keyword: name,
              type: Types.FILTER_TYPE_HIT_ID,
              name: this.state.hits[cellInfo.index].title,
              id: uuidv4(),
              action: Types.FILTER_ACCEPT,
            }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
          title="Whitelist HIT ID"
        />
        <a
          href={"https://worker.mturk.com" + this.state.hits[cellInfo.index].accept_project_task_url.replace(".json", "")}
          target="_blank"
          rel="noreferrer noopener"
          className="link"
          title={this.state.hits[cellInfo.index].description}
        >
          {cellInfo.value}
        </a>
      </FlexView>
    );
  }

  renderTimeCell(cellInfo) {
    return (
      <FlexView hAlignContent="right" vAlignContent="center" marginTop="0.25em" marginRight="4px" marginBottom="0.25em" className="normal-text">

        <div>{(new Date(cellInfo.value)).toLocaleTimeString()}</div>
      </FlexView>
    );
  }

  renderPayCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginRight="4px" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderAvailableCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginRight="4px" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  render() {
    if (this.state.hits === null || this.state.filter === null) {
      return (<div />);
    }

    const columns = [
      {
        Header: "Requester (" + this.state.hits.filter((obj) => { return obj.validated; }).length + ")",
        accessor: "requester_name",
        Cell: this.renderRequesterCell,
        maxWidth: this.getColumnWidth(this.state.hits, "requester_name", "Requester (" + this.state.hits.length + ") XX XX"),
        filterMethod: (filter, row) => { return (row[filter.id].toLowerCase().includes(filter.value.toLowerCase())); },
      },
      {
        Header: "Title",
        accessor: "title",
        Cell: this.renderTitleCell,
        filterMethod: (filter, row) => { return (row[filter.id].toLowerCase().includes(filter.value.toLowerCase())); },
      },
      {
        Header: "Pay",
        accessor: "pay",
        Cell: this.renderPayCell,
        width: this.getColumnWidth(this.state.hits, "pay", "Pay $$$"),
        filterMethod: (filter, row) => { return (parseFloat(row[filter.id]) >= parseFloat(filter.value)); },
      },
      {
        Header: "Available",
        accessor: "assignable_hits_count",
        Cell: this.renderAvailableCell,
        width: this.getColumnWidth(this.state.hits, "assignable_hits_count", "Available"),
        filterMethod: (filter, row) => { return (parseFloat(row[filter.id]) >= parseFloat(filter.value)); },
      },
      {
        Header: "Time",
        accessor: "timestamp",
        Cell: this.renderTimeCell,
        width: this.getColumnWidth(this.state.hits, "timestamp", "HHH:MM:SS"),
        filterMethod: (filter, row) => { return ((new Date(row[filter.id])).toLocaleTimeString().toLowerCase().includes(filter.value.toLowerCase())); },
      },
    ];

    return (
      <div className="block">
        <ReactTable
          data={this.state.hits}
          resolveData={(data) => {
            const newData = [];
            for (let i = 0; i < data.length; i++) {
              if (data[i].validated) {
                newData.push(data[i]);
              }
            }
            return newData;
          }}
          columns={columns}
          showPaginationBottom={false}
          resizable
          filterable

          sorted={this.state.sorted}
          page={this.state.page}
          pageSize={this.state.pageSize}
          expanded={this.state.expanded}
          resized={this.state.resized}
          filtered={this.state.filtered}

          onSortedChange={(sorted) => { return this.setState({ sorted }); }}
          onPageChange={(page) => { return this.setState({ page }); }}
          onPageSizeChange={(pageSize, page) => {
            this.setState({ page, pageSize });
          }}
          onExpandedChange={(expanded) => { return this.setState({ expanded }); }}
          onResizedChange={(resized) => { return this.setState({ resized }); }}
          onFilteredChange={(filtered) => {
            this.setState({ page: 0, filtered });
          }}
        />
        <FlexView basis="100%">
          <FlexView grow={1} hAlignContent="center" className="rt-pagination-parent">
            <FlexView marginRight="2em">
              <button
                className="raised-button"
                onClick={() => {
                  if (this.state.page > 0) {
                    this.setState((prevState) => {
                      return { page: prevState.page - 1 };
                    });
                  }
                }}
                type="button"
              >
                Previous
              </button>
            </FlexView>
            <FlexView hAlignContent="center" vAlignContent="center">
              <div className="normal-text">Page</div>
              <input
                type="text"
                spellCheck="false"
                value={this.state.page}
                className="normal-text tiny-input input rt-pagination-input"
                onChange={(e) => {
                  let num = parseInt(e.target.value, 10);
                  if (e.target.value.length === 0) {
                    num = 0;
                  }
                  if (num >= 0 && num <= this.state.hits.length / this.state.pageSize) {
                    this.setState({ page: num });
                  }
                }}
              />
              <div className="normal-text">
                of &nbsp;
                {this.getNumPages()}
              </div>
            </FlexView>

            <FlexView marginLeft="2em">
              <button
                className="raised-button"
                onClick={() => {
                  if (this.state.page < this.state.hits.length / this.state.pageSize - 1) {
                    this.setState((prevState) => {
                      return { page: prevState.page + 1 };
                    });
                  }
                }}
                type="button"
              >
                Next
              </button>
            </FlexView>
          </FlexView>
        </FlexView>
      </div>
    );
  }
}
