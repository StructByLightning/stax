/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import { NavLink } from "react-router-dom";
import FlexView from "react-flexview";
import { withRouter } from "react-router";

class NavMenu extends React.PureComponent {
  render() {
    return (
      <FlexView grow={0} hAlignContent="center" className="nav-menu">
        <FlexView grow={1} shrink={1} hAlignContent="center" vAlignContent="center">
          <NavLink to="/queue" className="nav-menu-item" exact activeClassName="active-menu-item">QUEUE</NavLink>
        </FlexView>
        <FlexView grow={1} shrink={1} hAlignContent="center" vAlignContent="center">
          <NavLink to="/hits" className="nav-menu-item" exact activeClassName="active-menu-item">HITS</NavLink>
        </FlexView>
        <FlexView grow={1} shrink={1} hAlignContent="center" vAlignContent="center">
          <NavLink to="/filter" className="nav-menu-item" exact activeClassName="active-menu-item">FILTER</NavLink>
        </FlexView>
        <FlexView grow={1} shrink={1} hAlignContent="center" vAlignContent="center">
          <NavLink to="/activity" className="nav-menu-item" exact activeClassName="active-menu-item">ACTIVITY</NavLink>
        </FlexView>
        <FlexView grow={1} shrink={1} hAlignContent="center" vAlignContent="center">
          <NavLink to="/config" className="nav-menu-item" exact activeClassName="active-menu-item">CONFIG</NavLink>
        </FlexView>
        <FlexView grow={1} shrink={1} hAlignContent="center" vAlignContent="center">
          <NavLink to="/changelog" className="nav-menu-item" exact activeClassName="active-menu-item">ABOUT</NavLink>
        </FlexView>
      </FlexView>
    );
  }
}
export default withRouter(NavMenu);
