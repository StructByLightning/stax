/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import FlexView from "react-flexview";

class ChangelogView extends React.PureComponent {
  render() {
    return (
      <FlexView>
        <FlexView column shrink={1} hAlignContent="left" className="block normal-text">
          <div className="content header">CHANGELOG</div>
          <div className="content">
            <div>Want a feature not on this list, no matter how small? Send me a DM on Reddit: /u/StructByLightning. Or open a GitLab issue on the<a className="link" href="https://gitlab.com/StructByLightning/stax/issues">Issues page</a>.</div>
          </div>

          <div className="content">
            <div className="header">V1.2.1</div>
            <div>▪ Fix for a bug with the advanced interval settings. If you encountered this bug and can&apos;t type anything into the boxes, click the Reset button and that should fix it. (#53)</div>
          </div>

          <div className="content">
            <div className="header">V1.2.0</div>
            <div>▪ Fixed the missing checkboxes on the Queue tab.</div>
            <div>▪ Stax now tracks how much time you spend with a HIT tab open and uses the information to calculate each completed HIT&apos;s hourly wage. (#38)</div>
            <div>▪ Added four reports (1 day, 7 days, 14 days, and 30 days) to the Activity tab. Click their titles to copy a Markdown version to the clipboard. (#37)</div>
            <div>▪ Added an explicit dropzone to the Filter tab for exported Stax and MTS filter files. (#45)</div>
            <div>▪ Added an explicit dropzone to the Config tab for exported theme files.</div>
            <div>▪ Fixed the default sort in the Activity tab to show the most recently completed HITs. (#48)</div>
            <div>▪ Added an Export to CVS button to the Activity tab.</div>
            <div>▪ The Activity tab&apos;s exported CSV will now be sorted by date, with the most recent at the top. (#49)</div>
            <div>▪ Renamed the Stax state switch in the Config tab to Running.</div>
            <div>▪ Added interval fields for the three background processes to the Config tab. These are advanced settings -- the defaults are optimized for Amazon&apos;s rate limiting. Choosing poor intervals may result in pages loading slowly or not at all. Modify them at your own risk. (#44)</div>
            <div>▪ Added a donation panel to the About tab.</div>
          </div>

          <div className="content">
            <div className="header">V1.1.3</div>
            <div>▪ Fixed the missing checkboxes on the Queue tab.</div>
            <div>▪ The Blacklist tab has been renamed to Filter.</div>
            <div>▪ Entries in the Filter tab can now be set to Reject or Accept. Accept entries will override all Reject entries, so use them with care. (#34)</div>
            <div>▪ Added whitelist (create an Accept filter) buttons to the Queue and HITs tab.</div>
            <div>▪ Added experimental support for filter export/import from both Stax and MTS. You can find the related controls in the Filter tab, at the bottom inside a purple box marked as Experimental. To import, simply drag the relevant file into the filter table. Use this feature at your own risk. (#12, #17)</div>
            <div>▪ Added a penny HIT link to the captcha popup. (#35)</div>
            <div>▪ Fixed a display bug with HIT timers in the queue. (#36)</div>
            <div>▪ Fixed Stax accepting some HITs with pay below the minimum if it was changed recently.</div>
            <div>▪ Stax will go straight to the Queue tab instead of a blank screen when opened.</div>
            <div>▪ If Stax has been updated recently, it will show the About tab instead of the Queue tab.</div>
            <div>▪ Added an Activity tab. It does not automatically sync with Amazon; you must click the Sync button at the bottom each time you want updated information, and it will take around half a minute to collect all of the data (depending on how many HITs you&apos;ve done in the last month). More features will be added to the Activity tab in future updates. (#9)</div>
            <div>▪ Added a Minimum and Maximum pay for notification alarm sounds when a HIT has been successfully accepted. Find them under Config/Behavior. (#39)</div>
            <div>▪ Fixed a couple of minor interface inconsistencies.</div>
          </div>

          <div className="content">
            <div className="header">V1.0.2</div>
            <div>▪ Fixed the missing checkboxes on the Queue tab.</div>
          </div>

          <div className="content">
            <div className="header">V1.0.1</div>
            <div>▪ Fixed Stax beeping whenever your queue was changed even if Stax wasn&apos;t running.</div>
            <div>▪ The Config tab now has an Appearance section for creating custom themes.</div>
            <div>▪ Themes can be exported to a Stax theme file and imported by dragging them onto the Appearance section.</div>
            <div>▪ Minor visual changes to clean up Stax&apos;s visuals.</div>
          </div>

          <div className="content">
            <div className="header">V1.0.0</div>
            <div>▪ Major architectural changes to improve efficiency, particularly when Stax is a background tab.</div>
            <div>▪ Several minor display fixes.</div>
          </div>

          <div className="content">
            <div className="header">V0.0.4</div>
            <div>▪ Stax can now play a notification sound when a new HIT is added to the queue. This is disabled by default and can be turned on with the Notification Sound and Notification Volume settings in the Config tab. (#28)</div>
            <div>▪ An alert similar to the captcha popup will now appear in the Queue tab when Amazon has triggered a logout. (#23)</div>
            <div>▪ Stax accepts HITs whose requesters do not have TO ratings. This is enabled by default but can now be turned off with the Accept Unrated setting in the Config tab. (#30)</div>
          </div>

          <div className="content">
            <div className="header">V0.0.3</div>
            <div>▪ Added an About tab that lists changes made to Stax.</div>
            <div>▪ Stax will now detect when a captcha has occurred and embed it in the Queue tab for manual completion.</div>
            <div>▪ Stax now auto-blacklists the IDs of HITs that have been accepted. This is enabled by default and can be turned off with the Single Accept setting in the Config tab. (#16)</div>
            <div>▪ The blacklist by HIT ID buttons will not appear in the Queue tab if Single Accept is enabled to reduce visual clutter.</div>
            <div>▪ The blacklist buttons in the Queue tab now blacklists by requester ID and HIT ID, instead of by requester name and HIT title. (#13)</div>
            <div>▪ You can blacklist HITs and requesters by their IDs from the Hits tab now. (#15)</div>
            <div>▪ You can edit entries in the Blacklist tab now. (#18)</div>
            <div>▪ Possible fix for intermittent blacklist failure bug. (#19)</div>
            <div>▪ You can manually add a new entry to the blacklist now. (#25)</div>
            <div>▪ Added a new type of blacklist entry: Text. Text entries will filter out HITs if the HIT title or description contains (or equals) the entry&apos;s keyword.</div>
            <div>▪ Fixed Stax not detecting input in the Hits tab. (#20)</div>
            <div>▪ Hovering over a HIT title in the Queue and Hits tabs will show its description. (#21)</div>
            <div>▪ Better tables in the Queue, Hits, and Blacklist tabs.</div>
            <div>▪ Tweaked the default config values to provide a better first-time experience.</div>
            <div>▪ More tooltips on miscellaneous buttons.</div>
            <div>▪ Many minor visual fixes.</div>
          </div>
          <div className="content">
            <div className="header">V0.0.2</div>
            <div>▪ Removed Tesseract-based captcha completion.</div>

          </div>
          <div className="content">
            <div className="header">V0.0.1</div>
            <div>▪ First release.</div>
          </div>
        </FlexView>
        <FlexView column shrink={1} className="block normal-text">
          <FlexView column className="content">
            <div className="header">SUPPORT STAX</div>
            <div>Stax is and always will be free and open source. With that said, I would greatly appreciate any <a className="link" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=R78ZDX5ZMZJYL">donations</a> you might want to contribute.</div>
          </FlexView>
        </FlexView>
      </FlexView>
    );
  }
}

export default ChangelogView;
