/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* eslint no-restricted-globals: off */

import React, { Component } from "react";
import ReactTable from "react-table";
import FlexView from "react-flexview";
import { saveAs } from "file-saver/FileSaver";
import LoadingSpinner from "./widgets/loadingSpinner";
import StoreInterface from "../../store/storeInterface";
import * as Types from "../../constants/types";

export default class ActivityView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sorted: [
        {
          id: "date",
          desc: true,
        },
      ],
      page: 0,
      pageSize: 17,
      expanded: {},
      resized: [],
      filtered: [],
      activity: null,
      tabs: null,
      syncing: false,
      showCopied: {
        1: false,
        7: false,
        14: false,
        30: false,
      },
    };

    this.mounted = false;

    this.si = new StoreInterface();
    this.si.getStore((error, payload) => {
      this.setState({ activity: payload.activity, tabs: payload.tabs });
    });

    this.intervalHandle = setInterval(() => {
      this.si.getStore((error, payload) => {
        this.setState({ activity: payload.activity, tabs: payload.tabs });
      });
    }, 1000);


    this.renderRequesterCell = this.renderRequesterCell.bind(this);
    this.renderTitleCell = this.renderTitleCell.bind(this);
    this.renderHourlyCell = this.renderHourlyCell.bind(this);
    this.renderReport = this.renderReport.bind(this);
  }

  componentWillUnmount() {
    clearInterval(this.intervalHandle);
  }

  getColumnWidth(rows, accessor, headerText) {
    const maxWidth = 400;
    const magicSpacing = 12;

    let cellLength = headerText.length;
    for (let i = 0; i < rows.length; i++) {
      const len = ("" + rows[i][accessor]).length;

      if (len > cellLength) {
        cellLength = len;
      }
    }
    const num = Math.min(maxWidth, cellLength * magicSpacing);

    return num;
  }

  getNumPages() {
    let numPages = 0;
    if (this.state.activity.length > this.state.pageSize) {
      numPages = Math.ceil(this.state.activity.length / this.state.pageSize) - 1;
    }

    return numPages;
  }

  getAssociatedTabData(assignmentId) {
    let flatPay = -1;
    let hourlyPay = "N/A";
    let time = 0;
    for (let i = 0; i < this.state.activity.length; i++) {
      if (this.state.activity[i].assignmentId === assignmentId) {
        flatPay = this.state.activity[i].pay;
      }
    }

    const relevantTabs = [];
    for (let i = 0; i < this.state.tabs.length; i++) {
      if (this.state.tabs[i].assignmentId === assignmentId) {
        relevantTabs.push(this.state.tabs[i]);
      }
    }

    if (relevantTabs.length > 0 && flatPay > -1) {
      for (let i = 0; i < relevantTabs.length; i++) {
        time += relevantTabs[i].closed - relevantTabs[i].opened;
      }

      time /= 1000; // to seconds
      hourlyPay = (flatPay / (time / 3600));
    }
    return { hourly: hourlyPay, time };
  }

  generateReport(day) {
    const report = {
      numHits: 0,
      totalHourlyPaid: 0,
      numHitsApproved: 0,
      numHitsPending: 0,
      numHitsRejected: 0,
      totalPaid: 0,
      totalProjectedPaid: 0,
      totalTime: 0,
      avgHourlyPay: 0,
    };

    const minimum = new Date(new Date().setDate(new Date().getDate() - day)).getTime();
    for (let i = 0; i < this.state.activity.length; i++) {
      if (new Date(this.state.activity[i].date).getTime() > minimum) {
        // increment counters and sums
        if (this.state.activity[i].state === Types.HIT_STATUS_PAID || this.state.activity[i].state === Types.HIT_STATUS_APPROVED) {
          report.numHitsApproved++;
          report.totalPaid += this.state.activity[i].pay;
          report.totalProjectedPaid += this.state.activity[i].pay; // projected should be a total to match other apps
        } else if (this.state.activity[i].state === Types.HIT_STATUS_PENDING || this.state.activity[i].state === Types.HIT_STATUS_SUBMITTED) { // submitted only appears in the mturk json api for some reason
          report.numHitsPending++;
          report.totalProjectedPaid += this.state.activity[i].pay;
        } else if (this.state.activity[i].state === Types.HIT_STATUS_REJECTED) {
          report.numHitsRejected++;
        }

        report.numHits++;

        // time is more complicated since it involves a sum of all relevant tabs
        const data = this.getAssociatedTabData(this.state.activity[i].assignmentId);
        if (data.hourly !== "N/A") {
          report.totalHourlyPaid += this.state.activity[i].pay;
          report.totalTime += data.time;
          report.numHitsWithHourlyData++;
        }
      }
    }
    report.avgHourlyPay = (report.totalHourlyPaid / (report.totalTime / 3600)).toFixed(2);
    report.totalTime = Math.round(report.totalTime / 60); // convert to int minutes
    report.totalPaid = report.totalPaid.toFixed(2);
    report.totalProjectedPaid = report.totalProjectedPaid.toFixed(2);

    if (isNaN(report.avgHourlyPay)) {
      report.avgHourlyPay = 0;
    }

    return report;
  }

  activityToCsv() {
    let string = "Requester Name,Requester ID,Assignment ID,Title,State,Pay($),Date,Time Spent(s),Hourly Wage($)\n";

    const activity = JSON.parse(JSON.stringify(this.state.activity)); // make a copy so it doesn't change midway

    activity.sort((a, b) => {
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    });

    console.log(activity);


    for (let i = 0; i < activity.length; i++) {
      string += "\"" + ("" + activity[i].requesterName).replace(/"/g, "\"\"") + "\",";
      string += "\"" + ("" + activity[i].requesterId).replace(/"/g, "\"\"") + "\",";
      string += "\"" + ("" + activity[i].assignmentId).replace(/"/g, "\"\"") + "\",";
      string += "\"" + ("" + activity[i].state).replace(/"/g, "\"\"") + "\",";
      string += "\"" + ("" + activity[i].title).replace(/"/g, "\"\"") + "\",";
      string += "\"" + ("" + activity[i].pay).replace(/"/g, "\"\"") + "\",";
      string += "\"" + ("" + activity[i].date).replace(/"/g, "\"\"") + "\",";

      const data = this.getAssociatedTabData(activity[i].assignmentId);
      string += "\"" + data.time + "\",";
      string += "\"" + data.hourly + "\"\n";
    }
    return string;
  }

  renderReport(day) {
    const report = this.generateReport(day);

    return (
      <FlexView column grow={1} className="block">
        <FlexView
          hAlignContent="center"
          onClick={() => {
            const text = "**Stax " + day + (day > 1 ? " Days" : " Day") + " Report**\n" +
            "---------\n" +
            "*HITs*\n" +
            "- **Submitted:** " + report.numHits + "\n" +
            "- **Approved:** " + report.numHitsApproved + "\n" +
            "- **Pending:** " + report.numHitsPending + "\n" +
            "- **Rejected:** " + report.numHitsRejected + "\n\n" +

            "*Earnings*\n" +
            "- **Received:** $" + report.totalPaid + "  \n" +
            "- **Projected Total:** $" + report.totalProjectedPaid + "\n\n" +

            "*Misc*\n" +
            "- **Average Wage:** $" + report.avgHourlyPay + "/hr\n" +
            "- **Minutes Spent:** " + report.totalTime;


            navigator.clipboard.writeText(text);

            // start a COPIED animation
            this.setState((prevState) => {
              const oldShowCopied = prevState.showCopied;
              const newShowCopied = { ...oldShowCopied, [day]: true };
              return { showCopied: newShowCopied };
            });
            setTimeout(() => {
              this.setState((prevState) => {
                const oldShowCopied = prevState.showCopied;
                const newShowCopied = { ...oldShowCopied, [day]: false };
                return { showCopied: newShowCopied };
              });
            }, 3000);
          }}
        >
          <div className="copy-text-parent">
            <div className={this.state.showCopied[day] ? "header fadeout" : "header"}>{day} {day > 1 ? "days" : "day"}</div>
            <div className={this.state.showCopied[day] ? "header fadein" : "header faded-out"}>COPIED</div>
            <div className="hidden header">{"" + day + (day > 1 ? "days" : "day")}</div>
          </div>
        </FlexView>
        <FlexView column className="content">
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Submitted:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">{report.numHits}</div>
            </FlexView>
          </FlexView>
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Approved:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">{report.numHitsApproved}</div>
            </FlexView>
          </FlexView>
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Pending:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">{report.numHitsPending}</div>
            </FlexView>
          </FlexView>
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Rejected:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">{report.numHitsRejected}</div>
            </FlexView>
          </FlexView>
        </FlexView>
        <FlexView column className="content">
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Earnings:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">${report.totalPaid}</div>
            </FlexView>
          </FlexView>
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Projected Earnings:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">${report.totalProjectedPaid}</div>
            </FlexView>
          </FlexView>
        </FlexView>
        <FlexView column className="content">
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Average Wage:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">${report.avgHourlyPay}/hr</div>
            </FlexView>
          </FlexView>
          <FlexView>
            <FlexView grow={1}>
              <div className="normal-text bold-text">Minutes Spent:&nbsp;</div>
            </FlexView>
            <FlexView grow={1} hAlignContent="right">
              <div className="normal-text">{report.totalTime}</div>
            </FlexView>
          </FlexView>
        </FlexView>
      </FlexView>
    );
  }

  renderDateCell(cellInfo) {
    return (
      <FlexView vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderRequesterCell(cellInfo) {
    return (
      <FlexView vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderTitleCell(cellInfo) {
    return (
      <FlexView vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderStateCell(cellInfo) {
    return (
      <FlexView vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderPayCell(cellInfo) {
    return (
      <FlexView vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderHourlyCell(cellInfo) {
    const hourlyPay = this.getAssociatedTabData(cellInfo.value).hourly;
    return (
      <FlexView vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginBottom="0.25em" className="normal-text">
        <div>{parseFloat(hourlyPay).toFixed(2)}</div>
      </FlexView>
    );
  }


  render() {
    if (this.state.activity === null || this.state.tabs === null) {
      return (<div />);
    }

    const columns = [
      {
        Header: "Date",
        accessor: "date",
        Cell: this.renderDateCell,
        width: this.getColumnWidth(this.state.activity, "date", "Date"),
        filterMethod: (filter, row) => { return (row[filter.id].toLowerCase().includes(filter.value.toLowerCase())); },
      },
      {
        Header: "Title",
        accessor: "title",
        Cell: this.renderTitleCell,
        filterMethod: (filter, row) => { return (row[filter.id].toLowerCase().includes(filter.value.toLowerCase())); },
      },
      {
        Header: "Requester",
        accessor: "requesterName",
        Cell: this.renderRequesterCell,
        width: this.getColumnWidth(this.state.activity, "requesterName", "Requester"),
        filterMethod: (filter, row) => { return (parseFloat(row[filter.id]) >= parseFloat(filter.value)); },
      },
      {
        Header: "State",
        accessor: "state",
        Cell: this.renderStateCell,
        width: this.getColumnWidth(this.state.activity, "state", "State"),
        filterMethod: (filter, row) => { return (parseFloat(row[filter.id]) >= parseFloat(filter.value)); },
      },
      {
        Header: "Pay",
        accessor: "pay",
        Cell: this.renderPayCell,
        width: this.getColumnWidth(this.state.activity, "pay", "Pay $.$$"),
        filterMethod: (filter, row) => { return (parseFloat(row[filter.id]) >= parseFloat(filter.value)); },
      },
      {
        Header: "Hourly Wage",
        accessor: "assignmentId",
        Cell: this.renderHourlyCell,
        width: this.getColumnWidth(this.state.activity, "Hourly", "Hourly Wage"),
        filterMethod: (filter, row) => { return (parseFloat(row[filter.id]) >= parseFloat(filter.value)); },
      },

    ];

    return (
      <FlexView column>
        <FlexView>
          {this.renderReport(1)}
          {this.renderReport(7)}
          {this.renderReport(14)}
          {this.renderReport(30)}
        </FlexView>
        <FlexView className="block">
          <FlexView grow={1} className="content" hAlignContent="center">

            {!this.state.syncing && (
              <button
                className={this.state.syncing ? "raised-button disabled-button" : "raised-button"}
                onClick={() => {
                  this.setState({ syncing: true });
                  this.si.syncActivity(() => {
                    this.setState({ syncing: false });
                  });
                }}
                type="button"
              >
                Sync
              </button>
            )}
            {this.state.syncing && (
              <LoadingSpinner label="Syncing" />
            )}
          </FlexView>
        </FlexView>
        <FlexView column className="block">
          <ReactTable
            data={this.state.activity}
            columns={columns}
            showPaginationBottom={false}
            resizable
            filterable

            sorted={this.state.sorted}
            page={this.state.page}
            pageSize={this.state.pageSize}
            expanded={this.state.expanded}
            resized={this.state.resized}
            filtered={this.state.filtered}

            onSortedChange={(sorted) => { return this.setState({ sorted }); }}
            onPageChange={(page) => { return this.setState({ page }); }}
            onPageSizeChange={(pageSize, page) => {
              this.setState({ page, pageSize });
            }}
            onExpandedChange={(expanded) => { return this.setState({ expanded }); }}
            onResizedChange={(resized) => { return this.setState({ resized }); }}
            onFilteredChange={(filtered) => {
              this.setState({ page: 0, filtered });
            }}
          />
          <FlexView basis="100%">
            <FlexView grow={1} hAlignContent="left">
              <FlexView vAlignContent="center">
                <button
                  type="button"
                  className="raised-button"
                  onClick={() => {
                    const blob = new Blob([this.activityToCsv()], { type: "text/plain;charset=utf-8" });
                    saveAs(blob, "stax-activity.csv");
                  }}
                  title="Export this data to a CSV file."
                >
                  <FlexView vAlignContent="center" hAlignContent="center">Export (CSV)</FlexView>
                </button>
              </FlexView>
            </FlexView>
            <FlexView grow={1} hAlignContent="center" className="rt-pagination-parent">
              <FlexView marginRight="2em">
                <button
                  className="raised-button"
                  onClick={() => {
                    if (this.state.page > 0) {
                      this.setState((prevState) => {
                        return { page: prevState.page - 1 };
                      });
                    }
                  }}
                  type="button"
                >
                  Previous
                </button>
              </FlexView>
              <FlexView hAlignContent="center" vAlignContent="center">
                <div className="normal-text">Page</div>
                <input
                  type="text"
                  spellCheck="false"
                  value={this.state.page}
                  className="normal-text tiny-input input rt-pagination-input"
                  onChange={(e) => {
                    let num = parseInt(e.target.value, 10);
                    if (e.target.value.length === 0) {
                      num = 0;
                    }
                    if (num >= 0 && num <= this.state.activity.length / this.state.pageSize) {
                      this.setState({ page: num });
                    }
                  }}
                />
                <div className="normal-text">
                  of &nbsp;
                  {this.getNumPages()}
                </div>
              </FlexView>
              <FlexView marginLeft="2em">
                <button
                  className="raised-button"
                  onClick={() => {
                    if (this.state.page < this.state.activity.length / this.state.pageSize - 1) {
                      this.setState((prevState) => {
                        return { page: prevState.page + 1 };
                      });
                    }
                  }}
                  type="button"
                >
                  Next
                </button>
              </FlexView>
            </FlexView>
            <FlexView grow={2} hAlignContent="right">
              <FlexView vAlignContent="center" />
            </FlexView>
          </FlexView>
        </FlexView>
      </FlexView>
    );
  }
}
