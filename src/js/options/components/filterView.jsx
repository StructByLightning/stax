/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import { saveAs } from "file-saver/FileSaver";
import FlexView from "react-flexview";
import ReactTable from "react-table";
import ExitButton from "./widgets/exitButton";
import SegmentedControl from "./widgets/segmentedControl";
import * as Types from "../../constants/types";
import StoreInterface from "../../store/storeInterface";
import DropBlock from "./widgets/dropBlock";

const uuidv4 = require("uuid/v4");

export default class FilterView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sorted: [],
      page: 0,
      pageSize: 17,
      expanded: {},
      resized: [],
      filtered: [],
      filter: null,
    };

    this.si = new StoreInterface();
    this.si.getStore((error, payload) => {
      this.setState({ filter: payload.filter });
    });

    this.renderKeywordCell = this.renderKeywordCell.bind(this);
    this.renderNameCell = this.renderNameCell.bind(this);
    this.renderTypeCell = this.renderTypeCell.bind(this);
    this.renderActionCell = this.renderActionCell.bind(this);
  }


  getNumPages() {
    let numPages = 0;
    if (this.state.filter.length > this.state.pageSize) {
      numPages = Math.ceil(this.state.filter.length / this.state.pageSize) - 1;
    }

    return numPages;
  }

  parseFilterFile(data) {
    const keys = Object.keys(data);

    // stax filter file
    if (keys.length > 0 && keys[0] === "filter") {
      this.applyFilter(data.filter);
    }

    // mts blocklist file
    if (keys.length > 0) {
      if (data[keys[0]].name !== undefined && data[keys[0]].match !== undefined && data[keys[0]].strict !== undefined && data[keys[0]].term !== undefined) {
        const filter = [];

        for (let i = 0; i < keys.length; i++) {
          const entry = {};
          entry.id = uuidv4(); // mts doesn't have a guid
          entry.keyword = data[keys[i]].match; // mts's match and term are the same as stax's keyword
          entry.action = Types.FILTER_REJECT; // inferred from the shape of the data (includes are different)
          entry.name = data[keys[i]].name; // same as mts
          entry.type = Types.FILTER_TYPE_DESCRIPTION; // good default fallback

          // if strict is true then it PROBABLY applies to a hit or requester
          if (data[keys[i]].strict) {
            // requester ids are between 10-20 alphanumeric digits long and start with an A
            if (keys[i].match(/^A[A-z0-9]{10,20}$/)) {
              entry.type = Types.FILTER_TYPE_REQUESTER_ID;
            } else if (keys[i].match(/^[A-z0-9]{25,}$/)) { // hit ids are 30 digits
              entry.type = Types.FILTER_TYPE_HIT_ID;
            }
          }
          filter.push(entry);
        }
        this.applyFilter(filter);
      }
    }
  }

  applyFilter(newFilter) {
    const si = new StoreInterface();
    si.addFilter(newFilter, (error, payload) => {
      this.setState({ filter: payload.filter });
    });
  }

  renderKeywordCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" className="normal-text">
        <ExitButton
          onClick={() => {
            this.si.removeFilter([this.state.filter[cellInfo.index].id], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
          title="Remove this entry from the filter."
        />
        <div
          spellCheck="false"
          className="normal-text input input-parent rt-input"
          contentEditable
          suppressContentEditableWarning
          onBlur={(e) => {
            const entry = this.state.filter[cellInfo.index];
            this.si.setFilter([{ id: entry.id, keyword: e.target.innerHTML }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
          title="This is the text that Stax will search for."
          dangerouslySetInnerHTML={{
            __html: this.state.filter[cellInfo.index][cellInfo.column.id],
          }}
        />
      </FlexView>
    );
  }

  renderNameCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" className="normal-text input-parent">
        <div
          className="normal-text input rt-input"
          spellCheck="false"
          contentEditable
          suppressContentEditableWarning
          onBlur={(e) => {
            const entry = this.state.filter[cellInfo.index];
            this.si.setFilter([{ id: entry.id, name: e.target.innerHTML }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
          title="This is a human-readable description of the entry (useful when filtering by ID)."
          dangerouslySetInnerHTML={{
            __html: this.state.filter[cellInfo.index][cellInfo.column.id],
          }}
        />
      </FlexView>
    );
  }

  renderTypeCell(cellInfo) {
    return (
      <SegmentedControl
        label=""
        activeSegment={"" + this.state.filter[cellInfo.index].type}
        segments={[
          { name: "HIT", value: "HIT ID", title: "Do not accept HITs if their ID equals the keyword." },
          { name: "Requester", value: "Requester ID", title: "Do not accept HITs if their requester ID equals the keyword." },
          { name: "Text", value: "Description", title: "Do not accept HITs if the title or description contains the keyword. Not case sensitive." },
        ]}
        onChange={(value) => {
          this.si.setFilter([{ id: this.state.filter[cellInfo.index].id, type: value }], (error, payload) => {
            this.setState({ filter: payload.filter });
          });
        }}
      />
    );
  }

  renderActionCell(cellInfo) {
    return (
      <SegmentedControl
        label=""
        activeSegment={"" + this.state.filter[cellInfo.index].action}
        segments={[
          { name: "Accept", value: Types.FILTER_ACCEPT, title: "Accept HITs matching this entry. This overrides all other Reject entries." },
          { name: "Reject", value: Types.FILTER_REJECT, title: "Reject HITs matching this entry." },
        ]}
        onChange={(value) => {
          this.si.setFilter([{ id: this.state.filter[cellInfo.index].id, action: value }], (error, payload) => {
            this.setState({ filter: payload.filter });
          });
        }}
      />
    );
  }

  render() {
    if (this.state.filter === null) {
      return (<div />);
    }

    const columns = [
      {
        Header: "Keyword (" + this.state.filter.length + ")",
        accessor: "keyword",
        Cell: this.renderKeywordCell,
        filterMethod: (filter, row) => {
          return row[filter.id].toLowerCase().includes(filter.value.toLowerCase());
        },
      },
      {
        Header: "Name",
        accessor: "name",
        Cell: this.renderNameCell,
        filterMethod: (filter, row) => {
          return row[filter.id].toLowerCase().includes(filter.value.toLowerCase());
        },
      },
      {
        Header: "Type",
        accessor: "type",
        Cell: this.renderTypeCell,
        filterMethod: (filter, row) => {
          return row[filter.id].toLowerCase().includes(filter.value.toLowerCase());
        },
      },
      {
        Header: "Action",
        accessor: "action",
        Cell: this.renderActionCell,
        filterMethod: (filter, row) => {
          return row[filter.id].toLowerCase().includes(filter.action.toLowerCase());
        },
      },
    ];

    return (
      <DropBlock
        label="Drop a Stax filter file here to load it."
        onDrop={(event) => {
          event.preventDefault();
          const fr = new FileReader();
          fr.onloadend = (obj) => {
            this.parseFilterFile(JSON.parse(obj.target.result));
          };
          fr.readAsText(event.dataTransfer.files[0]);
        }}
      >
        <ReactTable
          data={this.state.filter}
          columns={columns}
          showPaginationBottom={false}
          filterable

          sorted={this.state.sorted}
          page={this.state.page}
          pageSize={this.state.pageSize}
          expanded={this.state.expanded}
          resized={this.state.resized}
          filtered={this.state.filtered}

          onSortedChange={(sorted) => { return this.setState({ sorted }); }}
          onPageChange={(page) => { return this.setState({ page }); }}
          onPageSizeChange={(pageSize, page) => {
            this.setState({ page, pageSize });
          }}
          onExpandedChange={(expanded) => { return this.setState({ expanded }); }}
          onResizedChange={(resized) => { return this.setState({ resized }); }}
          onFilteredChange={(filtered) => { this.setState({ page: 0, filtered }); }}
        />
        <FlexView basis="100%">
          <FlexView grow={1} hAlignContent="left">
            <FlexView vAlignContent="center">
              <button
                type="button"
                className="raised-button"
                onClick={() => {
                  let targetPage = Math.ceil((this.state.filter.length + 1) / this.state.pageSize) - 1;
                  if (targetPage < 0) {
                    targetPage = 0;
                  }

                  this.si.addFilter([
                    {
                      id: uuidv4(),
                      name: "",
                      keyword: "",
                      type: Types.FILTER_TYPE_HIT_ID,
                      action: Types.FILTER_REJECT,
                    }
                    ], (error, payload) => {
                      this.setState({ page: targetPage, filter: payload.filter });
                    });
                }}
                title="Create a new filter entry."
              >
                New Entry
              </button>
            </FlexView>
          </FlexView>
          <FlexView grow={1} hAlignContent="center" className="rt-pagination-parent">
            <FlexView marginRight="2em">
              <button
                type="button"
                className="raised-button"
                onClick={() => {
                  if (this.state.page > 0) {
                    this.setState((prevState) => {
                      return { page: prevState.page - 1 };
                    });
                  }
                }}
              >
                Previous
              </button>
            </FlexView>
            <FlexView hAlignContent="center" vAlignContent="center">
              <div className="normal-text">Page</div>
              <input
                type="text"
                spellCheck="false"
                value={this.state.page}
                className="normal-text tiny-input input rt-pagination-input"
                onChange={(e) => {
                  let num = parseInt(e.target.value, 10);
                  if (e.target.value.length === 0) {
                      num = 0;
                  }
                  if (num >= 0 && num <= this.state.filter.length / this.state.pageSize) {
                    this.setState({ page: num });
                  }
                }}
              />
              <div className="normal-text">
                of &nbsp;
                {this.getNumPages()}
              </div>
            </FlexView>
            <FlexView marginLeft="2em">
              <button
                type="button"
                className="raised-button"
                onClick={() => {
                  if (this.state.page < this.state.filter.length / this.state.pageSize - 1) {
                    this.setState((prevState) => {
                      return { page: prevState.page + 1 };
                    });
                  }
                }}
              >
                Next
              </button>
            </FlexView>
          </FlexView>
          <FlexView grow={1} hAlignContent="right">
            <FlexView vAlignContent="center">
              <button
                type="button"
                className="danger-button"
                onClick={() => {
                  this.si.clearFilter((error, payload) => {
                    this.setState({ filter: payload.filter });
                  });
                }}
                title="This action cannot be undone."
              >
                Clear All
              </button>
            </FlexView>
          </FlexView>
        </FlexView>
        <FlexView column className="experimental-content">
          <div className="header">EXPERIMENTAL</div>
          <FlexView>
            <FlexView column>
              <button
                type="button"
                className="raised-button"
                onClick={() => {
                  const blob = new Blob([JSON.stringify({ filter: this.state.filter })], { type: "text/plain;charset=utf-8" });
                  saveAs(blob, "stax-filter.json");
                }}
                title="Export the current filter settings to a local file to back it up or transfer it between computers. Drag the file onto Stax to import it."
              >
                <FlexView vAlignContent="center" hAlignContent="center">Export (Stax)</FlexView>
              </button>
              <button
                type="button"
                className="raised-button"
                onClick={() => {
                  const mtsBlocklist = {};
                  const mtsIncludelist = {};
                  for (let i = 0; i < this.state.filter.length; i++) {
                    const entry = {};
                    entry.name = this.state.filter[i].name;
                    entry.match = this.state.filter[i].keyword;


                    if (this.state.filter[i].type === Types.FILTER_TYPE_DESCRIPTION) {
                      entry.strict = false;
                    } else {
                      entry.strict = true;
                    }

                    if (this.state.filter[i].action === Types.FILTER_REJECT) {
                      entry.term = this.state.filter[i].keyword;
                      mtsBlocklist[this.state.filter[i].keyword] = entry;
                    } else if (this.state.filter[i].action === Types.FILTER_ACCEPT) {
                      entry.term = this.state.filter[i].keyword;
                      mtsIncludelist[this.state.filter[i].keyword] = entry;
                    }
                  }

                  const blob1 = new Blob([JSON.stringify(mtsBlocklist)], { type: "text/plain;charset=utf-8" });
                  saveAs(blob1, "mts-block-list.json");

                  const blob2 = new Blob([JSON.stringify(mtsIncludelist)], { type: "text/plain;charset=utf-8" });
                  saveAs(blob2, "mts-include-list.json");
                }}
                title="Export the current filter settings to a local MTS-formatted block and include list."
              >
                <FlexView vAlignContent="center" hAlignContent="center">Export (MTS)</FlexView>
              </button>
            </FlexView>
            <FlexView hAlignContent="center" vAlignContent="center" className="fake-drop-zone">
              <div className="normal-text">Drop your import file here</div>
            </FlexView>
          </FlexView>
        </FlexView>
      </DropBlock>
    );
  }
}
