/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import FlexView from "react-flexview";
import { saveAs } from "file-saver/FileSaver";
import TextInput from "./textInput";
import SegmentedControl from "./widgets/segmentedControl";
import Slider from "./widgets/slider";
import StoreInterface from "../../store/storeInterface";
import ColorSelector from "./widgets/colorSelector";
import * as Functions from "../functions";

export default class ConfigView extends Component {
  constructor(props) {
    super(props);

    this.si = new StoreInterface();
    this.state = {
      config: null,
      theme: null,
      dragging: false,
    };

    this.si.getStore((error, payload) => {
      this.setState({ config: payload.config, theme: payload.theme });
    });
  }


  render() {
    if (this.state.config === null || this.state.theme === null) {
      return (<div />);
    }

    return (
      <div>
        <FlexView column className="block normal-text">
          <FlexView column wrap className="content">
            <div className="header">BEHAVIOR</div>
            <FlexView wrap>
              <FlexView column className="">
                <SegmentedControl
                  label="Sort method"
                  activeSegment={this.state.config.hitSort}
                  segments={[
                    { name: "Newest", value: "updated_desc", title: "Sort with most recently posted HITs first." },
                    { name: "Oldest", value: "updated_asc", title: "Sort with the oldest HITs first." },
                    { name: "Best pay", value: "reward_desc", title: "Sort with the highest flat paying HITs first." },
                    { name: "Worst pay", value: "reward_asc", title: "Sort with the worst flat paying HITs first." },
                    { name: "Most HITs", value: "num_hits_desc", title: "Sort with the biggest batches first." },
                    { name: "Least HITs", value: "num_hits_asc", title: "Sort with the smallest batches first." },
                  ]}
                  onChange={(value) => {
                    this.si.setConfig({ hitSort: value }, (error, payload) => {
                      this.setState({ config: payload.config });
                    });
                  }}
                />
                <SegmentedControl
                  label="Notification sound"
                  activeSegment={this.state.config.notificationAudioFilepath}
                  segments={[
                    { name: "Exquisite", value: "sounds/exquisite.mp3" },
                    { name: "Good", value: "sounds/good.mp3" },
                    { name: "Plucky", value: "sounds/plucky.mp3" },
                    { name: "Withdraw", value: "sounds/withdraw.mp3" },
                    { name: "None", value: "" },
                  ]}
                  onChange={(value) => {
                    this.si.setConfig({ notificationAudioFilepath: value }, (error, payload) => {
                      this.setState({ config: payload.config });
                    });
                  }}
                />
                <FlexView>
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.notificationMinPay;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size >= 0) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Don't play a notification sound if the new HIT's pay is less than this."
                    width="small"
                    defaultValue={this.state.config.notificationMinPay}
                    placeholder=""
                    label="Min Notification Pay"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.notificationMinPay = data;
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ notificationMinPay: parseFloat(this.state.config.notificationMinPay) }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.notificationMaxPay;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size >= 0) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Don't play a notification sound if the new HIT's pay is greater than this."
                    width="small"
                    defaultValue={this.state.config.notificationMaxPay}
                    placeholder=""
                    label="Max Notification Pay"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.notificationMaxPay = data;
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ notificationMaxPay: parseFloat(this.state.config.notificationMaxPay) }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                </FlexView>
                <Slider
                  label="Notification volume"
                  min="0"
                  max="1"
                  step="0.01"
                  value={"" + this.state.config.notificationAudioVolume}
                  onChange={(value) => {
                    this.si.setConfig({ notificationAudioVolume: value }, (error, payload) => {
                      this.setState({ config: payload.config });
                    });
                  }}
                />
                <FlexView wrap>
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.minReward;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size >= 0) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Don't scrape HITs that have a flat pay less than this."
                    width="small"
                    defaultValue={this.state.config.minReward}
                    placeholder=""
                    label="Pay ($)"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.minReward = data;
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ minReward: parseFloat(this.state.config.minReward) }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.pageSize;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size > 0 && size <= 100) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Number of HITs to scrape at once. Maximum of 100."
                    width="small"
                    defaultValue={this.state.config.pageSize}
                    placeholder=""
                    label="Page size"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 1;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.pageSize = data;
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ pageSize: this.state.config.pageSize }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />

                  <SegmentedControl
                    label="Require masters"
                    activeSegment={"" + this.state.config.masters}
                    segments={[
                      { name: "YES", value: "true", title: "Only scrape HITs that require a Master&apos;s qualification." },
                      { name: "NO", value: "false", title: "Only scrape HITs that require a Master&apos;s qualification." },
                    ]}
                    onChange={() => {
                      this.si.toggleConfig(["masters"], (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />

                  <SegmentedControl
                    label="Only qualified"
                    activeSegment={"" + this.state.config.qualified}
                    segments={[
                      { name: "YES", value: "true", title: "Only scrape HITs that you have MTurk&apos;s qualifications for. This will not screen out HITs where the requester does not include an Amazon MTurk qualification." },
                      { name: "NO", value: "false", title: "Only scrape HITs that you have MTurk&apos;s qualifications for. This will not screen out HITs where the requester does not include an Amazon MTurk qualification." },
                    ]}
                    onChange={() => {
                      this.si.toggleConfig(["qualified"], (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                </FlexView>
                <FlexView wrap>
                  <SegmentedControl
                    label="Single Accept"
                    activeSegment={"" + this.state.config.singleAccept}
                    segments={[
                      { name: "YES", value: "true", title: "Automatically blocklist a HIT&apos;s ID after it has been accepted. Note that requesters often post copies of HITs under multiple IDs." },
                      { name: "NO", value: "false", title: "Automatically blocklist a HIT&apos;s ID after it has been accepted. Note that requesters often post copies of HITs under multiple IDs." },
                    ]}
                    onChange={() => {
                      this.si.toggleConfig(["singleAccept"], (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                  <SegmentedControl
                    label="Accept Unrated"
                    activeSegment={"" + this.state.config.acceptUnratedRequesters}
                    segments={[
                      { name: "YES", value: "true", title: "Accept HITs whose requesters are unrated." },
                      { name: "NO", value: "false", title: "Accept HITs whose requesters are unrated." },
                    ]}
                    onChange={() => {
                      this.si.toggleConfig(["acceptUnratedRequesters"], (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                </FlexView>
              </FlexView>

              <FlexView column className="text normal-text settings-panel">
                <FlexView wrap>
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.to1MinComm;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size >= 0 && size <= 4) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Minimum Communication score on TO1."
                    width="small"
                    defaultValue={this.state.config.to1MinComm}
                    placeholder=""
                    label="TO1 comm"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.to1MinComm = parseFloat(data);
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ to1MinComm: this.state.config.to1MinComm }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.to1MinPay;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size >= 0 && size <= 4) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Minimum Pay score on TO1."
                    width="small"
                    defaultValue={this.state.config.to1MinPay}
                    placeholder=""
                    label="TO1 pay"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.to1MinPay = parseFloat(data);
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ to1MinPay: this.state.config.to1MinPay }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.to1MinFair;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size >= 0 && size <= 4) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Minimum Fair score on TO1."
                    width="small"
                    defaultValue={this.state.config.to1MinFair}
                    placeholder=""
                    label="TO1 fair"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.to1MinFair = parseFloat(data);
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ to1MinFair: this.state.config.to1MinFair }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.to1MinFast;
                      const size = parseInt(raw, 10);
                      if (!Number.isNaN(size) && size >= 0 && size <= 4) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Minimum Fast score on TO1."
                    width="small"
                    defaultValue={this.state.config.to1MinFast}
                    placeholder=""
                    label="TO1 fast"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.to1MinFast = parseFloat(data);
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ to1MinFast: this.state.config.to1MinFast }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                </FlexView>
                <FlexView>
                  <TextInput
                    isValid={() => {
                      const raw = this.state.config.lifespan;
                      const size = parseInt(raw, 10);

                      if (!Number.isNaN(size) && size >= 0) {
                        return true;
                      }
                      return false;
                    }}
                    errorMessage="Invalid"
                    tooltip="Time to continue trying to accept HITs that someone else has already taken."
                    width="small"
                    defaultValue={"" + this.state.config.lifespan}
                    placeholder=""
                    label="Lifespan (ms)"
                    type="number"
                    onChange={(data) => {
                      if (data.length === 0) {
                        data = 0;
                      }
                      this.setState((prevState) => {
                        const newConfig = JSON.parse(JSON.stringify(prevState.config));
                        newConfig.lifespan = data;
                        return { config: newConfig };
                      });
                    }}
                    onValidBlur={() => {
                      this.si.setConfig({ lifespan: this.state.config.lifespan }, (error, payload) => {
                        this.setState({ config: payload.config });
                      });
                    }}
                  />
                </FlexView>
              </FlexView>


            </FlexView>
            <FlexView>
              <FlexView grow={1} hAlignContent="left" vAlignContent="bottom">
                <SegmentedControl
                  label="Running"
                  activeSegment={this.state.config.active ? "true" : "false"}
                  segments={[
                    { name: "YES", value: "true" },
                    { name: "NO", value: "false" },
                  ]}
                  title="Whether to scrape, accept, and perform other active functions. Turning this off will disable all automatic Stax activity, including background syncing to Mturk."
                  onChange={() => {
                    this.si.toggleConfig(["active"], (error, payload) => {
                      this.setState({ config: payload.config });
                    });
                  }}
                />
              </FlexView>
            </FlexView>
            <FlexView grow={1} hAlignContent="right" vAlignContent="bottom">
              <button
                type="button"
                className="danger-button"
                onClick={() => {
                  console.log("Hello")
                  this.si.clearConfig((error, payload) => {
                    console.log(payload);
                    this.setState({ config: payload.config });
                  });
                }}
                title="Reset all settings to their default values."
              >
                <FlexView vAlignContent="center" hAlignContent="center">Reset</FlexView>
              </button>
            </FlexView>
            <FlexView column className="dangerous-content">
              <div className="header">ADVANCED</div>
              <TextInput
                isValid={() => {
                  // obviously needs to be a number
                  const raw = this.state.config.hitScraperInterval;
                  const size = parseInt(raw, 10);
                  if (Number.isNaN(size)) {
                    return false;
                  }

                  // valid means that the total requests per second is below 1.5
                  // this is set to 1.75 to give the user some wiggle room, 1.5 is recommended though
                  const hitScraperContrib = 1 / (this.state.config.hitScraperInterval / 1000);
                  const hitAccepterContrib = 1 / (this.state.config.hitAccepterInterval / 1000);
                  const queueScraperContrib = 1 / (this.state.config.queueScraperInterval / 1000);

                  if (hitScraperContrib + hitAccepterContrib + queueScraperContrib > 1.75) {
                    return false;
                  }

                  return true;
                }}
                errorMessage="Too low"
                tooltip="How frequently to check for new HITs."
                width="small"
                defaultValue={this.state.config.hitScraperInterval}
                placeholder=""
                label="HIT Scraper Interval"
                type="number"
                onChange={(data) => {
                  if (data.length === 0) {
                    data = 0;
                  }
                  this.setState((prevState) => {
                    const newConfig = JSON.parse(JSON.stringify(prevState.config));
                    newConfig.hitScraperInterval = parseFloat(data);
                    return { config: newConfig };
                  });
                }}
                onValidBlur={() => {
                  this.si.setConfig({ hitScraperInterval: this.state.config.hitScraperInterval }, (error, payload) => {
                    this.setState({ config: payload.config });
                  });
                }}
              />
              <TextInput
                isValid={() => {
                  // obviously needs to be a number
                  const raw = this.state.config.hitAccepterInterval;
                  const size = parseInt(raw, 10);
                  if (Number.isNaN(size)) {
                    return false;
                  }

                  // valid means that the total requests per second is below 1.5
                  // this is set to 1.75 to give the user some wiggle room, 1.5 is recommended though
                  const hitScraperContrib = 1 / (this.state.config.hitScraperInterval / 1000);
                  const hitAccepterContrib = 1 / (this.state.config.hitAccepterInterval / 1000);
                  const queueScraperContrib = 1 / (this.state.config.queueScraperInterval / 1000);

                  if (hitScraperContrib + hitAccepterContrib + queueScraperContrib > 1.75) {
                    return false;
                  }

                  return true;
                }}
                errorMessage="Too low"
                tooltip="How frequently to check HIT Panda links."
                width="small"
                defaultValue={this.state.config.hitAccepterInterval}
                placeholder=""
                label="HIT Accepter Interval"
                type="number"
                onChange={(data) => {
                  if (data.length === 0) {
                    data = 0;
                  }
                  this.setState((prevState) => {
                    const newConfig = JSON.parse(JSON.stringify(prevState.config));
                    newConfig.hitAccepterInterval = parseFloat(data);
                    return { config: newConfig };
                  });
                }}
                onValidBlur={() => {
                  this.si.setConfig({ hitAccepterInterval: this.state.config.hitAccepterInterval }, (error, payload) => {
                    this.setState({ config: payload.config });
                  });
                }}
              />
              <TextInput
                isValid={() => {
                  // obviously needs to be a number
                  const raw = this.state.config.queueScraperInterval;
                  const size = parseInt(raw, 10);
                  if (Number.isNaN(size)) {
                    return false;
                  }

                  // valid means that the total requests per second is below 1.5
                  // this is set to 1.75 to give the user some wiggle room, 1.5 is recommended though
                  const hitScraperContrib = 1 / (this.state.config.hitScraperInterval / 1000);
                  const hitAccepterContrib = 1 / (this.state.config.hitAccepterInterval / 1000);
                  const queueScraperContrib = 1 / (this.state.config.queueScraperInterval / 1000);

                  if (hitScraperContrib + hitAccepterContrib + queueScraperContrib > 1.75) {
                    return false;
                  }

                  return true;
                }}
                errorMessage="Too low"
                tooltip="How frequently to sync the queue."
                width="small"
                defaultValue={this.state.config.queueScraperInterval}
                placeholder=""
                label="Queue Scraper Interval"
                type="number"
                onChange={(data) => {
                  if (data.length === 0) {
                    data = 0;
                  }
                  this.setState((prevState) => {
                    const newConfig = JSON.parse(JSON.stringify(prevState.config));
                    newConfig.queueScraperInterval = parseFloat(data);
                    return { config: newConfig };
                  });
                }}
                onValidBlur={() => {
                  this.si.setConfig({ queueScraperInterval: this.state.config.queueScraperInterval }, (error, payload) => {
                    this.setState({ config: payload.config });
                  });
                }}
              />
            </FlexView>
          </FlexView>
        </FlexView>
        <FlexView
          className={this.state.dragging ? "normal-text drop-block" : "block normal-text"}
          onDragOver={(event) => {
            this.setState({ dragging: true });
            event.preventDefault();
            return false;
          }}
          onDragLeave={(event) => {
            this.setState({ dragging: false });
            event.preventDefault();
            return false;
          }}
          onDrop={(event) => {
            event.preventDefault();
            const fr = new FileReader();
            fr.onloadend = (obj) => {
              const data = JSON.parse(obj.target.result);
              const si = new StoreInterface();
              if (data.theme !== undefined) {
                si.setTheme(data.theme, (error, payload) => {
                  this.setState({ theme: payload.theme, dragging: false }); // this is nested so disordered/incomplete theme files won't break anything
                });

                for (let i = 0; i < data.theme.length; i++) {
                  Functions.applyThemeVariable(data.theme[i].variable, data.theme[i].color);
                }
              }
            };
            fr.readAsText(event.dataTransfer.files[0]);
          }}
        >
          {this.state.dragging && (
            <FlexView grow={1} hAlignContent="center" vAlignContent="center">
              <div>Drop a Stax theme file here to load it.</div>
            </FlexView>
          )}
          {!this.state.dragging && (
            <FlexView column className="content">
              <div className="header">APPEARANCE</div>
              <FlexView wrap>
                {this.state.theme.map((entry) => {
                  return (
                    <div key={entry.variable}>
                      <ColorSelector
                        label={entry.name}
                        color={entry.color}
                        onChange={(data) => {
                          this.si.setTheme([{ variable: entry.variable, color: data.color }], (error, payload) => {
                            this.setState({ theme: payload.theme });
                          });
                          Functions.applyThemeVariable(entry.variable, data.color);
                        }}
                      />
                    </div>
                  );
                })}
              </FlexView>
              <FlexView>
                <FlexView grow={1} hAlignContent="left">
                  <FlexView column>
                    <button
                      type="button"
                      className="raised-button"
                      onClick={() => {
                        const blob = new Blob([JSON.stringify({ theme: this.state.theme })], { type: "text/plain;charset=utf-8" });
                        saveAs(blob, "stax-theme.json");
                      }}
                      title="Export this theme to a local file to back it up or transfer it between computers. Drag the file onto Stax to import it."
                    >
                      <FlexView vAlignContent="center" hAlignContent="center">Export</FlexView>
                    </button>
                  </FlexView>
                  <FlexView hAlignContent="center" vAlignContent="center" className="fake-drop-zone">
                    <div className="normal-text">Drop your import file here</div>
                  </FlexView>
                </FlexView>
                <FlexView grow={1} hAlignContent="right" vAlignContent="bottom">
                  <button
                    type="button"
                    className="danger-button"
                    onClick={() => {
                      this.si.clearTheme((error, payload) => {
                        this.setState({ theme: payload.theme });
                        for (let i = 0; i < payload.theme.length; i++) {
                          Functions.applyThemeVariable(payload.theme[i].variable, payload.theme[i].color);
                        }
                      });
                    }}
                    title="Reset all theme options to their default values."
                  >
                    <FlexView vAlignContent="center" hAlignContent="center">Reset</FlexView>
                  </button>
                </FlexView>
              </FlexView>
            </FlexView>
          )}
        </FlexView>
      </div>
    );
  }
}
