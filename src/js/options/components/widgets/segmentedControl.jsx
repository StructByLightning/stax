/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import FlexView from "react-flexview";
import PropTypes from "prop-types";

export default class SegmentedControl extends Component {
  render() {
    console.log("sc render");
    return (
      <FlexView column className="sc-parent" title={this.props.title}>
        <div className="sc-label">{this.props.label}</div>
        <FlexView>
          <FlexView className="sc">
            {this.props.segments.map((segment) => {
              return (
                <FlexView
                  hAlignContent="center"
                  vAlignContent="center"
                  onClick={(event) => {
                    this.props.onChange(event.target.getAttribute("name"));
                  }}
                  name={segment.value}
                  key={segment.value}
                  className={"sc-segment " + (this.props.activeSegment === segment.value ? "sc-segment-active" : "")}
                  title={segment.title}
                >
                  <div className="sc-segment-label">{segment.name}</div>
                </FlexView>
              );
            })}
          </FlexView>

        </FlexView>
      </FlexView>
    );
  }
}

SegmentedControl.propTypes = {
  segments: PropTypes.array.isRequired,
  activeSegment: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  title: PropTypes.string,
  onChange: PropTypes.func,
}
