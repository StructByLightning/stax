/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import PropTypes from "prop-types";
import ColorPicker from "rc-color-picker";

export default class ColorSelector extends Component {
  render() {
    return (
      <div className="color-picker">
        <div className="color-picker-label">{this.props.label}</div>
        <div className="color-picker-active">
          <div className="hue-picker">
            <ColorPicker
              color={"" + this.props.color}
              enableAlpha={false}
              onChange={this.props.onChange}
              placement="topLeft"
            />
          </div>
        </div>
      </div>
    );
  }
}


ColorSelector.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
