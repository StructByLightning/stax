/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import PropTypes from "prop-types";
import FlexView from "react-flexview";


export default class DropBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dragging: false,
    };

    this.dragCounter = 0;
  }

  render() {
    return (
      <FlexView
        column
        grow={1}
        className={this.state.dragging ? "drop-block" : "block normal-text"}
        onDragOver={(event) => {
          event.preventDefault();
        }}
        onDragEnter={(event) => {
          if (this.state.dragging) {
            this.dragCounter += 1;
          } else {
            this.setState({ dragging: true });
          }
          event.preventDefault();
          return false;
        }}
        onDragLeave={(event) => {
          this.dragCounter -= 1;
          if (this.dragCounter > 1) {
            this.setState({ dragging: true });
          } else if (this.dragCounter === 0) {
            this.setState({ dragging: false });
          }
          event.preventDefault();
          return false;
        }}
        onDrop={(event) => {
          this.setState({ dragging: false });
          this.props.onDrop(event);
        }}
      >
        {this.state.dragging && (
          <FlexView grow={1} hAlignContent="center" vAlignContent="center" className="normal-text">
            <div>{this.props.label}</div>
          </FlexView>
        )}
        {!this.state.dragging && this.props.children}
      </FlexView>
    );
  }
}

DropBlock.propTypes = {
  onDrop: PropTypes.func.isRequired,
  children: PropTypes.any.isRequired,
  label: PropTypes.string.isRequired,
};
