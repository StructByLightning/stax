/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import PropTypes from "prop-types";

export default class CancelButton extends Component {


  render() {
    return (
      <div
        className={"protect-button " + (this.props.disabled() ? "disabled-button" : "")}
        onClick={() => {
          this.props.onClick();
        }}
        title={this.props.title}
      >
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
          <path d="M45.063 17.406L45.063 117.344L108.719 177.656L111.655 180.436L111.655 365.686C145.615 409.076 194.227 449.446 260.561 488.313C328.384 448.529 377.354 406.37 411.249 362.219L411.249 180.436L414.155 177.656L477.811 117.374L477.811 17.404L418.843 17.404L418.843 85.157L352.843 85.157L352.843 17.407L293.873 17.407L293.873 85.157L228.47 85.157L228.47 17.407L169.47 17.407L169.47 85.157L104.06 85.157L104.06 17.407L45.06 17.407Z" stroke="#000000" strokeOpacity="1" strokeWidth="42" />
          <path d="M101.719 117.876L261.749 117.876L261.749 270.626L148.437 270.626L148.437 161.656L101.719 117.876Z" />
          <path d="M262.312 271.81L374.468 271.81L374.468 345.623C349.516 378.723 312.598 410.337 262.31 439.653L262.31 271.814Z" />
        </svg>
      </div>
    );
  }
}

CancelButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.func.isRequired,
  title: PropTypes.string
}
