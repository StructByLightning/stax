/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import PropTypes from "prop-types";
import FlexView from "react-flexview";

// width can be one of:
// tiny, small, normal, large, huge, giant

export default class TextInput extends Component {
  render() {
    return (
      <FlexView column className="input-parent">
        <div className="input-label">{this.props.label}</div>
        <input
          className={
            "input normal-text "
            + this.props.width + "-input" +
            (this.props.isValid() ? " valid-input" : " invalid-input")
          }
          title={this.props.tooltip}
          type={this.props.type}
          placeholder={this.props.placeholder}
          onChange={(event) => {
            this.props.onChange(event.target.value);
          }}
          spellCheck="false"
          value={this.props.defaultValue}
          onBlur={(event) => {
            if (this.props.isValid()) {
              if (this.props.onValidBlur !== undefined) {
                this.props.onValidBlur(event.target.value);
              }
            }
          }}
        />
        {!this.props.isValid() &&
          <div className="tiny-text input-error-message">{this.props.errorMessage}</div>
        }
      </FlexView>
    );
  }
}

TextInput.propTypes = {
  placeholder: PropTypes.string.isRequired,
  tooltip: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  defaultValue: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.number.isRequired]),
  type: PropTypes.string.isRequired,
  isValid: PropTypes.func.isRequired,
  errorMessage: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired, // corresponds to the classes in inputs.scss (tiny/small/normal/large/huge/giant)
  onChange: PropTypes.func.isRequired, // will be called with one argument, data, that is the new value of the input
  onValidBlur: PropTypes.func,
};

TextInput.defaultProps = {
  defaultValue: "",
  onValidBlur: () => {},
};
