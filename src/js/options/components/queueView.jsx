/*
Stax, a second-generation MTurk automation tool
Copyright (C) 2018  Resheet Schultz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import ReactTable from "react-table";
import FlexView from "react-flexview";
import Checkbox from "./widgets/checkbox";
import CancelButton from "./widgets/cancelButton";
import ProtectButton from "./widgets/protectButton";
import TextInput from "./textInput";
import StoreInterface from "../../store/storeInterface";
import * as Types from "../../constants/types";
import * as Errors from "../../constants/errors";

const uuidv4 = require("uuid/v4");

export default class QueueView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markedHits: [],
      working: 0,
      captchaInput: "",
      submittingCaptcha: false,
      sorted: [],
      page: 0,
      pageSize: 25,
      expanded: {},
      resized: [],
      filtered: [],
      queue: null,
      filter: null,
      config: null,
      captcha: null,
      status: null,
    };


    this.si = new StoreInterface();
    this.si.getStore((error, payload) => {
      this.setState({
        queue: payload.queue,
        filter: payload.filter,
        config: payload.config,
        captcha: payload.captcha,
        status: payload.status,
      });
    });

    this.intervalHandle = setInterval(() => {
      this.si.getStore((error, payload) => {
        this.setState({
          queue: payload.queue,
          filter: payload.filter,
          config: payload.config,
          captcha: payload.captcha,
          status: payload.status,
        });
      });
    }, 1000);

    this.returnHits = this.returnHits.bind(this);
    this.renderRequesterCell = this.renderRequesterCell.bind(this);
    this.renderTitleCell = this.renderTitleCell.bind(this);
    this.renderTimeCell = this.renderTimeCell.bind(this);
    this.renderPayCell = this.renderPayCell.bind(this);
  }

  componentWillUnmount() {
    clearInterval(this.intervalHandle);
  }

  getColumnWidth(rows, accessor, headerText) {
    const maxWidth = 400;
    const magicSpacing = 12;

    let cellLength = headerText.length;
    for (let i = 0; i < rows.length; i++) {
      const len = ("" + rows[i][accessor]).length;
      if (len > cellLength) {
        cellLength = len;
      }
    }

    let num = Math.min(maxWidth, cellLength * magicSpacing);

    // add spacing for the buttons
    if (accessor === "requester_name") {
      num += 3 * magicSpacing;
    } else if (accessor === "time_to_deadline_in_seconds") {
      num += 5 * magicSpacing;
    }

    return num;
  }

  returnHits() {
    if (this.state.markedHits.length > 0) {
      this.setState((prevState) => {
        return { working: prevState.markedHits.length };
      });
      for (let i = 0; i < this.state.markedHits.length; i++) {
        this.returnHit(this.state.markedHits[i]);
      }
    }
    this.setState({ markedHits: [] });
  }

  returnHit(hit) {
    let url = "https://worker.mturk.com" + hit.task_url;
    url = url.replace(".json", ""); // for some reason it won't work if this stays

    this.si.returnHit(url, (error) => {
      if (error === Errors.RATE_LIMIT) { // hit wasn't returned so try again
        this.returnHit(hit);
      } else if (error === Errors.NO_ERROR) {
        this.si.removeQueue([hit.task_id], (e, payload) => {
          this.setState({ filter: payload.filter });
        });
        this.setState((prevState) => {
          return { working: prevState.working - 1 };
        });
      }
    });
  }

  secondsToHms(time) {
    time = Number(time);

    // get leftover seconds and convert time to minutes
    const seconds = time % 60;
    time = Math.trunc(time / 60);

    // get leftover minutes and convert time to hours
    const minutes = time % 24;
    time = Math.trunc(time / 24);

    const hours = time;

    let hDisplay = hours;
    let mDisplay = minutes;
    let sDisplay = seconds;
    if (hours < 10) {
      hDisplay = "0" + hours;
    }
    if (minutes < 10) {
      mDisplay = "0" + minutes;
    }
    if (seconds < 10) {
      sDisplay = "0" + seconds;
    }

    return hDisplay + ":" + mDisplay + ":" + sDisplay;
  }


  renderRequesterCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" marginTop="0.25em" marginBottom="0.25em" className="normal-text">
        <CancelButton
          disabled={() => {
            let isDisabled = false;
            const list = this.state.filter;
            for (let i = 0; i < list.length; i++) {
              if ((this.state.queue[cellInfo.index].requester_id === list[i].keyword) && (Types.FILTER_TYPE_REQUESTER_ID === list[i].type)) {
                if (Types.FILTER_REJECT === list[i].action) {
                  isDisabled = true;
                }
              }
            }
            return isDisabled;
          }}
          title="Blacklist requester ID"
          onClick={() => {
            const name = this.state.queue[cellInfo.index].requester_id;
            this.si.addFilter([{
              keyword: name,
              type: Types.FILTER_TYPE_REQUESTER_ID,
              name: this.state.queue[cellInfo.index].requester_name,
              id: uuidv4(),
              action: Types.FILTER_REJECT,
            }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
        />
        <ProtectButton
          disabled={() => {
            let isDisabled = false;
            const list = this.state.filter;
            for (let i = 0; i < list.length; i++) {
              if ((this.state.queue[cellInfo.index].requester_id === list[i].keyword) && (Types.FILTER_TYPE_REQUESTER_ID === list[i].type)) {
                if (Types.FILTER_ACCEPT === list[i].action) {
                  isDisabled = true;
                }
              }
            }
            return isDisabled;
          }}
          title="Whitelist requester ID"
          onClick={() => {
            const name = this.state.queue[cellInfo.index].requester_id;
            this.si.addFilter([{
              keyword: name,
              type: Types.FILTER_TYPE_REQUESTER_ID,
              name: this.state.queue[cellInfo.index].requester_name,
              id: uuidv4(),
              action: Types.FILTER_ACCEPT,
            }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
        />
        <div className="normal-text hits-table-cell">{cellInfo.value}</div>
      </FlexView>
    );
  }

  renderTitleCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" marginTop="0.25em" marginLeft="4px" marginRight="4px" marginBottom="0.25em" className="normal-text">
        {!this.state.config.singleAccept && (
          <CancelButton
            disabled={() => {
              let isDisabled = false;
              const list = this.state.filter;
              for (let i = 0; i < list.length; i++) {
                if ((this.state.queue[cellInfo.index].hit_set_id === list[i].keyword) && (Types.FILTER_TYPE_HIT_ID === list[i].type)) {
                  if (Types.FILTER_REJECT === list[i].action) {
                    isDisabled = true;
                  }
                }
              }
              return isDisabled;
            }}
            onClick={() => {
              const name = this.state.queue[cellInfo.index].hit_set_id;
              this.si.addFilter([{
                keyword: name,
                type: Types.FILTER_TYPE_HIT_ID,
                name: this.state.queue[cellInfo.index].title,
                id: uuidv4(),
                action: Types.FILTER_REJECT,
              }], (error, payload) => {
                this.setState({ filter: payload.filter });
              });
            }}
            title="Blacklist HIT ID"
          />
        )}
        <ProtectButton
          disabled={() => {
            let isDisabled = false;
            const list = this.state.filter;
            for (let i = 0; i < list.length; i++) {
              if ((this.state.queue[cellInfo.index].hit_set_id === list[i].keyword) && (Types.FILTER_TYPE_HIT_ID === list[i].type)) {
                if (Types.FILTER_ACCEPT === list[i].action) {
                  isDisabled = true;
                }
              }
            }
            return isDisabled;
          }}
          onClick={() => {
            const name = this.state.queue[cellInfo.index].hit_set_id;
            this.si.addFilter([{
              keyword: name,
              type: Types.FILTER_TYPE_HIT_ID,
              name: this.state.queue[cellInfo.index].title,
              id: uuidv4(),
              action: Types.FILTER_ACCEPT,
            }], (error, payload) => {
              this.setState({ filter: payload.filter });
            });
          }}
          title="Whitelist HIT ID"
        />
        <a
          href={"https://worker.mturk.com" + this.state.queue[cellInfo.index].task_url.replace(".json", "")}
          target="_blank"
          rel="noopener noreferrer"
          className="link"
          title={this.state.queue[cellInfo.index].description}
        >
          {cellInfo.value}
        </a>
      </FlexView>
    );
  }

  renderTimeCell(cellInfo) {
    return (
      <FlexView hAlignContent="right" vAlignContent="center" marginTop="0.25em" marginBottom="0.25em" className="normal-text">
        <div>{this.secondsToHms(parseInt(cellInfo.value, 10))}</div>
        <Checkbox
          className={(this.state.working ? "disabled-button" : "")}
          task_id={this.state.queue[cellInfo.index].task_id}
          task_url={this.state.queue[cellInfo.index].task_url}

          isChecked={() => {
            let index = -1;
            for (let i = 0; i < this.state.markedHits.length; i++) {
              if (this.state.queue[cellInfo.index].task_id === this.state.markedHits[i].task_id) {
                index = i;
              }
            }
            return index !== -1;
          }}
          onChange={(change, props) => {
            if (change) { // add hit id to list if it is not already there
              let index = -1;
              for (let i = 0; i < this.state.markedHits.length; i++) {
                if (props.task_id === this.state.markedHits[i].task_id) {
                  index = i;
                }
              }
              if (index === -1) {
                this.setState((prevState) => {
                  return { markedHits: [...prevState.markedHits, { task_id: props.task_id, task_url: props.task_url }] };
                });
              }
            } else { // remove hit id from list if it is already there
              let index = -1;
              for (let i = 0; i < this.state.markedHits.length; i++) {
                if (props.task_id === this.state.markedHits[i].task_id) {
                  index = i;
                }
              }
              if (index !== -1) {
                this.setState((prevState) => {
                  return {
                    markedHits: [
                      ...prevState.markedHits.slice(0, index),
                      ...prevState.markedHits.slice(index + 1),
                    ],
                  };
                });
              }
            }
          }}
        />
      </FlexView>
    );
  }

  renderPayCell(cellInfo) {
    return (
      <FlexView hAlignContent="left" vAlignContent="center" marginLeft="4px" marginTop="0.25em" marginBottom="0.25em" className="normal-text">
        <div>{cellInfo.value}</div>
      </FlexView>
    );
  }

  render() {
    if (this.state.queue === null || this.state.filter === null || this.state.config === null || this.state.captcha === null || this.state.status === null) {
      return (<div />);
    }
    const columns = [
      {
        Header: "Requester (" + this.state.queue.length + ")",
        accessor: "requester_name",
        Cell: this.renderRequesterCell,
        maxWidth: this.getColumnWidth(this.state.queue, "requester_name", "Requester (" + this.state.queue.length + ") XX XX"),
        filterMethod: (filter, row) => {
          return row[filter.id].toLowerCase().includes(filter.value.toLowerCase());
        },
      },
      {
        Header: "Title",
        accessor: "title",
        Cell: this.renderTitleCell,
        filterMethod: (filter, row) => {
          return row[filter.id].toLowerCase().includes(filter.value.toLowerCase());
        },
      },
      {
        Header: "Pay",
        accessor: "pay",
        Cell: this.renderPayCell,
        maxWidth: this.getColumnWidth(this.state.queue, "pay", "Pay $$$"),
        filterMethod: (filter, row) => {
          return parseFloat(row[filter.id]) >= parseFloat(filter.value);
        },
      },
      {
        Header: "Time",
        accessor: "time_to_deadline_in_seconds",
        Cell: this.renderTimeCell,
        maxWidth: this.getColumnWidth(this.state.queue, "time_to_deadline_in_seconds", "HHH:MM:SS"),
        filterMethod: (filter, row) => {
          return (this.secondsToHms(row[filter.id])).includes(filter.value.toLowerCase());
        },
      },
    ];

    return (
      <div>
        {this.state.status.loggedOut && (
          <FlexView column className="block">
            <FlexView grow={1} column className="content normal-text" hAlignContent="center" vAlignContent="center">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" className="alert-icon">
                <path d="M254.97 34.75C224.49 34.583 195.95 56.87 175.438 96.906C175.363 97.052 175.262 97.166 175.188 97.312L43.063 326.783L42.843 327.126C18.5 365.413 13.377 401.515 28.47 428.03C43.55 454.528 77.097 468.156 121.97 465.938L387.063 465.938C431.95 468.165 465.508 454.534 480.593 428.031C495.683 401.521 490.549 365.436 466.218 327.157L465.998 326.782L335.28 98.064C335.22 97.944 335.156 97.839 335.094 97.72C314.146 57.457 285.468 34.917 254.969 34.75Z" fill="#e11212" fillOpacity="1" />
                <path d="M255.03 53.594C268.606 53.724 281.483 60.524 293.156 71.937C304.762 83.284 315.71 99.39 326.562 120.281C326.625 120.403 326.687 120.505 326.75 120.626L441.97 322.189C442.003 322.242 442.028 322.291 442.062 322.345L442.187 322.565C455.107 342.839 463.582 360.625 467.469 376.532C471.379 392.542 470.532 407.18 463.624 418.94C456.716 430.7 444.402 438.473 428.844 442.846C413.4 447.186 394.336 448.502 371.436 447.346L137.625 447.346C112.78 448.604 92.895 447.026 77.22 442.221C61.44 437.381 49.54 428.771 43.5 416.531C37.46 404.294 37.638 389.734 42 374.095C46.333 358.56 54.815 341.487 66.875 322.565L67.095 322.188L183.562 120C183.642 119.843 183.732 119.72 183.812 119.562C194.51 98.644 205.32 82.6 216.875 71.376C228.517 60.069 241.455 53.463 255.031 53.596Z" fill="#ffffff" fillOpacity="1" />
                <path d="M302.687 115.687L274.157 339.719L232.313 339.719L204.438 120.5C203.034 123.056 201.628 125.705 200.218 128.47L200.125 128.688L200 128.906L83.062 331.876L82.969 332.063L82.843 332.25C71.28 350.346 63.598 366.226 60 379.125C56.402 392.025 56.892 401.447 60.25 408.25C63.608 415.053 70.175 420.53 82.72 424.375C95.262 428.22 113.39 429.922 137.125 428.688L137.375 428.656L371.688 428.656L371.938 428.686C393.788 429.824 411.246 428.406 423.813 424.876C436.379 421.343 443.635 416.049 447.5 409.469C451.365 402.889 452.478 393.924 449.313 380.969C446.147 368.011 438.581 351.595 426.219 332.249L426.093 332.061L425.968 331.843L310.31 129.563L310.217 129.405L310.153 129.218C307.653 124.39 305.163 119.892 302.683 115.686Z" className="selected" fill="#e11212" fillOpacity="1" />
                <path d="M231.28 361.875L275.187 361.875L275.187 405.781L231.28 405.781L231.28 361.876Z" fill="#fff" fillOpacity="1" />
              </svg>

              <div>You are currently logged out of Mturk.</div>
              <div>Please log back in on the official Amazon <a className="link" href="https://worker.mturk.com" target="_blank" rel="noopener noreferrer">website</a>.</div>
              <div>Stax does not store your username or password.</div>
            </FlexView>
          </FlexView>
        )}
        {this.state.captcha.imageUrl.length > 0 && !this.state.submittingCaptcha && !this.state.captcha.waitingForResync && (
          <FlexView column className="block">
            <FlexView hAlignContent="center">
              <img className="captcha-image" src={this.state.captcha.imageUrl} alt="" />
            </FlexView>
            <FlexView hAlignContent="center">
              <TextInput
                isValid={() => {
                  return true;
                }}
                errorMessage="Invalid"
                tooltip="Enter the symbols you see in the image to continue scraping HITs (required by AMT)."
                width="small"
                defaultValue={this.state.captchaInput}
                placeholder=""
                type="text"
                label=""
                onChange={(data) => {
                  this.setState({ captchaInput: data });
                }}
                onValidBlur={() => {}}
              />
              <button
                type="button"
                className={"raised-button " + (this.state.submittingCaptcha ? "disabled-button" : "")}
                onClick={() => {
                  this.setState({ submittingCaptcha: true });

                  this.si.submitCaptcha(this.state.captchaInput, (error) => {
                    if (error === Errors.NO_ERROR) {
                      this.si.clearCaptcha((e, payload) => {
                        this.setState({ captchaInput: "", submittingCaptcha: false, captcha: payload.captcha });
                      });
                    } else if (error === Errors.RATE_LIMIT || error === Errors.UNKNOWN || error === Errors.INCORRECT_CAPTCHA_SUBMISSION) {
                      this.si.clearCaptcha(() => {
                        this.si.setCaptcha({ imageUrl: "", waitingForResync: true }, (e, payload) => {
                          this.setState({ captcha: payload.captcha, captchaInput: "", submittingCaptcha: false });
                        });
                      });
                    }
                  });
                }}
              >
                Submit
              </button>
            </FlexView>
            <FlexView hAlignContent="center">
              <a href={this.state.captcha.hitUrl.replace("?stax=true", "")} className="normal-text link">Complete on Mturk</a>
            </FlexView>
          </FlexView>
        )}
        {this.state.submittingCaptcha && !this.state.captcha.waitingForResync && (
          <FlexView column className="block">
            <FlexView column hAlignContent="center">
              <div className="normal-text">Submitting answer</div>
              <div className="spinner tiny-text" />
            </FlexView>
          </FlexView>
        )}
        {this.state.captcha.waitingForResync && (
          <FlexView column className="block">
            <FlexView column hAlignContent="center">
              <div className="normal-text">Incorrect answer</div>
              <div className="normal-text">Retrieving new captcha</div>
              <div className="spinner tiny-text" />
            </FlexView>
          </FlexView>
        )}

        <div className="block">
          <ReactTable
            data={this.state.queue}
            columns={columns}
            showPaginationBottom={false}
            resizable
            filterable
            minRows={0}

            sorted={this.state.sorted}
            page={this.state.page}
            pageSize={this.state.pageSize}
            expanded={this.state.expanded}
            resized={this.state.resized}
            filtered={this.state.filtered}

            onSortedChange={(sorted) => { return this.setState({ sorted }); }}
            onPageChange={(page) => { return this.setState({ page }); }}
            onPageSizeChange={(pageSize, page) => { return this.setState({ page, pageSize }); }}
            onExpandedChange={(expanded) => { this.setState({ expanded }); }}
            onResizedChange={(resized) => { this.setState({ resized }); }}
            onFilteredChange={(filtered) => { this.setState({ page: 0, filtered }); }}
          />
          <FlexView hAlignContent="right">
            <FlexView>
              <button
                type="button"
                className={"danger-button " + (this.state.working > 0 ? "disabled-button" : "")}
                onClick={() => {
                  const elements = [];
                  const queue = this.state.queue;
                  for (let i = 0; i < queue.length; i++) {
                    elements.push({ task_id: queue[i].task_id, task_url: queue[i].task_url });
                  }
                  this.setState({ markedHits: [...elements] }, () => {
                    this.returnHits();
                  });
                }}
                title="MTurk limits the number of hits that may be returned at once, so this may take several seconds."
              >
                Return All HITs
              </button>
            </FlexView>
            <FlexView>
              <button
                type="button"
                className={"danger-button " + (this.state.working > 0 ? "disabled-button" : "")}
                onClick={() => {
                  this.returnHits();
                }}
                title="MTurk limits the number of hits that may be returned at once, so this may take several seconds."
              >
                Return Selected HITs
              </button>
            </FlexView>
          </FlexView>
        </div>
      </div>
    );
  }
}
