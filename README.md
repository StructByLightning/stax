# **Stax**
Stax is a second-generation HIT automation tool for Amazon MTurk designed from the ground up to create a streamlined workflow experience. A ridiculous amount of time is wasted *looking* for work. Stax aims to reduce or eliminate that, allowing workers to focus on the parts that require an actual person at the wheel.

Install the extension [here](https://chrome.google.com/webstore/detail/stax/ffdlknlhakgbhhkigcemlcndcjjllled/related?hl=en-US&gl=US&authuser=1) or read on for more information.

### **Features**
- Accept HITs whose requesters meet minimum custom standards
- Built-in load balancer to reduce rate limit errors and maximize HITs watched
- Manage HITs from an automatically synced queue
- Filter HITs with one click by ID or requester, or by text description
- Return multiple (or all) HITs at once
- Activity tracker and hourly wage calculator
- Custom themes

### **Is It Legit?**
Yes. Stax does not violate Amazon MTurk's Terms of Service because it does not complete HITs for you. Stax can be thought of as the logical next step from the current script ecosystem. HIT scrapers and HIT grabbers have existed for a long time now; Stax only combines the two into an integrated easy-to-use package. Other features of Stax, such as the enhanced queue management, are also allowed by Amazon MTurk.

### **Installation**
Stax is only available for Chrome at this time. Install it [here](https://chrome.google.com/webstore/detail/stax/ffdlknlhakgbhhkigcemlcndcjjllled/related?hl=en-US&gl=US&authuser=1).

### **Permissions**
Stax's permissions are required for it to function. Feel free to check the source code if you are concerned about abuse. At this time, Stax does not "phone home" or transmit your data to a server.

### **Contributing**
Stax is hosted on GitLab, a free git platform. Feature requests and bug reports are welcome on the [Issues](https://gitlab.com/StructByLightning/stax/issues) page. Click the green New Issue button, pick an appropriate title, and describe your question. The more detail you provide, the more likely it is to be fixed or added. If the feature already exists in another script and you want it to be integrated into Stax, provide a link to the script as well as a brief description of what it does.

Don't want to create a GitLab account? No problem. DM me on Reddit: /u/StructByLightning or send me an email at resheetschultz@gmail.com.

### **Contributors**
Resheet Schultz (primary developer)

### **License**
Stax is licensed under GPL3. See LICENSE.md for more information.
